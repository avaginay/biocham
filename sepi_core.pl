:- module(
  sepi_core,
  [
    % Commands
    search_sepi/5,
    write_header/6
  ]
).


:- use_module(sepi_util).
:- use_module(sepi_neigh_merge).
:- use_module(sepi_old_merge).


%% Main part of SEPI search

%! search_sepi(+G1, +G2, +Stream, +Merge, +Extremal_sepi)
%
% call all function that write clauses to the CNF file
% according to the options
%
% @arg G1             graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg G2             graph 2 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg Stream         Stream for the CNF file
% @arg Merge          no/neighbours/old cf. sepi:merge_restriction/1
% @arg Extremal_sepi  no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
search_sepi(G1, G2, Stream, Merge, Extremal_sepi) :-
  once(write_header(G1, G2, Stream, Extremal_sepi, Merge, 0)),
  G1 = [N1| _],
  G2 = [N2| _],
  get_option(stats,Stats),
  statistics(walltime, [_ | [_]]),
  partial_surj(N1, N2, Stream, Extremal_sepi),
  subgraph_epi(G1, G2, Stream, Extremal_sepi),
  statistics(walltime, [_ | [Execution_time_core]]),
  (
    Stats = yes
  ->
    string_concat('Core clauses time: ', Execution_time_core, String1_core),
    string_concat(String1_core, ' ms\n', String2_core),
    write(String2_core)
  ;
    true
  ),
  (
    Merge = neighbours
  ->
    statistics(walltime, [_ | [_]]),
    once(merge_neigh(G1, G2, Stream)),
    statistics(walltime, [_ | [Execution_time_hard_merge]]),
    (
      Stats = yes
    ->
      string_concat('Hard merge clauses time: ', Execution_time_hard_merge, String1_hard_merge),
      string_concat(String1_hard_merge, ' ms\n', String2_hard_merge),
      write(String2_hard_merge)
    ;
      true
    )
  ;
    Merge = old
  ->
    statistics(walltime, [_ | [_]]),
    once(search_sepi_merge(G1, G2, Stream)),
    statistics(walltime, [_ | [Execution_time_old_merge]]),
    (
      Stats = yes
    ->
      string_concat('Old merge clauses time: ', Execution_time_old_merge, String1_old_merge),
      string_concat(String1_old_merge, ' ms\n', String2_old_merge),
      write(String2_old_merge)
    ;
      true
    )
  ;
    true
  ),
  (
    Extremal_sepi \= no
  ->
    W is 1,
    format(Stream, 'c bottom constraints \n', []),
    statistics(walltime, [_ | [_]]),
    (
      Extremal_sepi = minimal_deletion
    ->
      bottom_min_constraint(N1, N2, W, Stream)
    ;
      bottom_max_constraint(N1, N2, W, Stream)
    ),
    statistics(walltime, [_ | [Execution_time_bottom]]),
    (
      Stats = yes
    ->
      string_concat('Bottom clauses time: ', Execution_time_bottom, String1_bottom),
      string_concat(String1_bottom, ' ms\n', String2_bottom),
      write(String2_bottom)
    ;
      true
    )
  ;
    true
  ).


%! write_header(+G1, +G2, +Stream, +Bottom, +Merge, +Solutions)
%
% write the input file's header for the SAT solver 
% in a simplified version of the DIMACS format
% p (w)cnf number_of_variables number_of_clauses (weight_of_hard_clauses)
%
% @arg G1         graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg G2         graph 2 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg Stream     Stream for the CNF file
% @arg Bottom     min_bottom/max_bottom/normal cf. sepi:reduction/1
% @arg Merge      no/neigh/old cf. sepi:merge_restriction/1
% @arg Solutions  number of solutions already found when enumerating all of them
write_header(G1, G2, Stream, Bottom, Merge, Solutions):-
  G1 = [N1, Ns1, E1, _],
  G2 = [N2, Ns2, E2, _],
  length(E1, Na1),
  length(E2, Na2),
  Nr1 is N1 - Ns1,
  Nr2 is N2 - Ns2,
  Nb_core_clauses is (N1 + N2 + (N1 * (3 * N2 + 1)) + Na1 + Na2 + (3 * Na1 * Na2) + (3 * Na1) + (Na1 * (N2 * N2 - Na2)) + (Ns1 * Nr2 + Nr1 * Ns2)) + Solutions,
  get_option(mapping_restriction, Mapping),
  length(Mapping, Added_clauses_mapping),
  Added_clauses_neigh is (N1 * N1 * N2),
  Added_clauses_bottom is N1,
  is_dummy(Na1, N1, N2, Na1, Na2, Nb_core_variables),
  (
    Bottom \= no
  ->
    Clauses_bottom is 1
  ;
    Clauses_bottom is 0
  ),
  (
    Merge = neighbours
  ->
    Clauses_neigh is 1
  ;
    Clauses_neigh is 0
  ),
  Total_clauses is (Nb_core_clauses + Added_clauses_mapping + Clauses_neigh * Added_clauses_neigh + Clauses_bottom * Added_clauses_bottom),
  Total_variables is Nb_core_variables,
  (
    Bottom \= no
  ->
    Top is (Added_clauses_bottom + 1),
    format(Stream, "p wcnf ~d ~d ~d~n", [Total_variables, Total_clauses, Top]),
    debug(sepi, "Header: p wcnf ~d ~d ~d~n", [Total_variables, Total_clauses, Top])
  ;
    format(Stream, "p cnf ~d ~d~n", [Total_variables, Total_clauses]),
    debug(sepi, "header: p cnf ~d ~d~n", [Total_variables, Total_clauses])
  ).


%% Basic sepi implementation

%! partial_surj(+N1, +N2, +Stream, +Bottom)
%
% Partial surjective function encoding
%
% @arg N1     Number of vertices in graph 1
% @arg N2     Number of vertices in graph 2
% @arg Stream Stream for the CNF file
% @arg Bottom min_bottom/max_bottom/normal cf. sepi:reduction/1
partial_surj(N1, N2, Stream, Bottom) :-
  N1 >= N2,
  format(Stream, 'c left totality \n', []),
  left_totality(N1, N2, Stream, Bottom),
  format(Stream, 'c right totality \n', []),
  right_totality(N1, N2, Stream, Bottom),
  format(Stream, 'c functionality \n', []),
  functionality(N1, N2, Stream, Bottom).


left_totality(N1, N2, Stream, Bottom) :-
  N1_1 is N1 - 1,
  (
    Bottom \= no
  ->
    Top is N1 + 1,
    format(Stream, "~d ", [Top])
  ;
    true
  ),
  left_totality(N1_1, N2, 0, 0, Stream, Bottom).

left_totality(N1, N2, N1, N, Stream, _) :-
  N is N2 + 1,
  write(Stream, "0\n"), !.

left_totality(N1, N2, I, N, Stream, Bottom) :-
  N is N2 + 1,
  I1 is I + 1,
  (
    Bottom \= no
  ->
    Top is N1 + 2,
    format(Stream, "0\n~d ", [Top])
  ;
    write(Stream, "0\n")
  ),
  left_totality(N1, N2, I1, 0, Stream, Bottom).

left_totality(N1, N2, I, J, Stream, Bottom) :-
  m_ij(I, J, N2, X),
  format(Stream, "~d ", [X]),
  J1 is J + 1,
  left_totality(N1, N2, I, J1, Stream, Bottom).


right_totality(N1, N2, Stream, Bottom) :-
  N2_1 is N2 - 1,
  (
    Bottom \= no
  ->
    Top is N1 + 1,
    format(Stream, "~d ", [Top])
  ;
    true
  ),
  right_totality(N1, N2_1, 0, 0, Stream, Bottom).

right_totality(N1, N2, N1, N2, Stream, _) :-
  write(Stream, "0\n"), !.

right_totality(N1, N2, N1, J, Stream, Bottom) :-
  J1 is J + 1,
  (
    Bottom \= no
  ->
    Top is N1 + 1,
    format(Stream, "0\n~d ", [Top])
  ;
    write(Stream, "0\n")
  ),
  right_totality(N1, N2, 0, J1, Stream, Bottom).

right_totality(N1, N2, I, J, Stream, Bottom) :-
  N is N2 + 1,
  m_ij(I, J, N, X),
  write(Stream, X), write(Stream, " "),
  I1 is I + 1,
  right_totality(N1, N2, I1, J, Stream, Bottom).


functionality(N1, N2, Stream, Bottom) :-
  functionality(N1, N2, 0, 0, Stream, Bottom).

functionality(N1, N2, N, N2, Stream, Bottom) :-
  N is N1 - 1,
  inf_imp_not_equ(N, N2, N1, N2, Stream, Bottom).

functionality(N1, N2, I, N2, Stream, Bottom) :-
  I1 is I + 1,
  inf_imp_not_equ(I, N2, N1, N2, Stream, Bottom),
  functionality(N1, N2, I1, 0, Stream, Bottom).

functionality(N1, N2, I, J, Stream, Bottom) :-
  J1 is J + 1,
  inf_imp_not_equ(I, J, N1, N2, Stream, Bottom),
  inf_impl_inf(I, J, N1, N2, Stream, Bottom),
  equ_impl_inf(I, J, N1, N2, Stream, Bottom),
  functionality(N1, N2, I, J1, Stream, Bottom).


%! subgraph_epi(+G1, +G2, +Stream, +Bottom)
%
% subgraph epimorphism coding
%
% @arg G1     graph 1 (G1 = [N1, Ns1, E1, Id1])
% @arg G2     graph 2 (G2 = [N2, Ns2, E2, Id2])
% @arg Stream Stream for the CNF file
% @arg Bottom min_bottom/max_bottom/normal cf. sepi:reduction/1
subgraph_epi(G1, G2, Stream, Bottom) :-
  G1 = [N1, _, Arcs1, Id1],
  G2 = [N2, _, Arcs2, Id2],
  length(Arcs1, Na1),
  length(Arcs2, Na2),
  format(Stream, 'c left totality on arcs \n', []),
  left_totality_arcs(N1, N2, Na1, Na2, Stream, Bottom),
  format(Stream, 'c right totality on arcs \n', []),
  right_totality_arcs(N1, N2, Na1, Na2, Stream, Bottom),
  format(Stream, 'c graph morphism \n', []),
  graph_morph(N1, N2, Arcs1, Arcs2, Stream, Bottom),
  format(Stream, 'c subgraph morphism \n', []),
  subgraph_morph(N1, N2, Arcs1, Arcs2, Stream, Bottom),
  format(Stream, 'c redundant morphism \n', []),
  redundant_morph_prop(N1, N2, Arcs1, Arcs2, Stream, Bottom),
  format(Stream, 'c bigraph constraint \n', []),
  bigraph_constraint(N1, N2, Id1, Id2, Stream, Bottom).


left_totality_arcs(N1, N2, Na1, Na2, Stream, Bottom)  :-
  Na1_1 is Na1 - 1,
  (
    Bottom \= no
  ->
    Top is N1 + 1,
    format(Stream, "~d ", [Top])
  ;
    true
  ),
  left_totality_arcs(N1, N2, Na1_1, Na2, 0, 0, Stream, Bottom).

left_totality_arcs(N1, N2, Na1, Na2, Na1, Na2, Stream, _) :-
  is_dummy(Na1, N1, N2, Na1, Na2, X),
  format(Stream, '~d 0\n', X).

left_totality_arcs(N1, N2, Na1, Na2, I, Na2, Stream, Bottom) :-
  I1 is I + 1,
  is_dummy(I, N1, N2, Na1, Na2, X),
  (
    Bottom \= no
  ->
    Top is N1 + 1,
    format(Stream, "~d 0\n~d ", [X, Top])
  ;
    format(Stream, "~d 0\n", [X])
  ),
  left_totality_arcs(N1, N2, Na1, Na2, I1, 0, Stream, Bottom).

left_totality_arcs(N1, N2, Na1, Na2, I, J, Stream, Bottom) :-
  m_arcs(I, J, N1, N2, Na2, X),
  format(Stream, '~d ', X),
  J1 is J + 1,
  left_totality_arcs(N1, N2, Na1, Na2, I, J1, Stream, Bottom).


right_totality_arcs(N1, N2, Na1, Na2, Stream, Bottom) :-
  Na2_1 is Na2 - 1,
  (
    Bottom \= no
  ->
    Top is N1 + 1,
    format(Stream, "~d ", [Top])
  ;
    true
  ),
  right_totality_arcs(N1, N2, Na1, Na2_1, 0, 0, Stream, Bottom).

right_totality_arcs(_, _, Na1, Na2, Na1, Na2, Stream, _) :-
  write(Stream, "0\n"), !.

right_totality_arcs(N1, N2, Na1, Na2, Na1, J, Stream, Bottom) :-
  J1 is J + 1,
  (
    Bottom \= no
  ->
    Top is N1 + 1,
    format(Stream, "0\n~d ", [Top])
  ;
    write(Stream, "0\n")
  ),
  right_totality_arcs(N1, N2, Na1, Na2, 0, J1, Stream, Bottom).

right_totality_arcs(N1, N2, Na1, Na2, I, J, Stream, Bottom) :-
  N is Na2 + 1,
  m_arcs(I, J, N1, N2, N, X),
  format(Stream, '~d ', X),
  I1 is I + 1,
  right_totality_arcs(N1, N2, Na1, Na2, I1, J, Stream, Bottom).


graph_morph(N1, N2, Arcs1, Arcs2, Stream, Bottom) :-
  graph_morph(N1, N2, Arcs1, Arcs2, 0, 0, Stream, Bottom).

graph_morph(N1, N2, Arcs1, Arcs2, I, Na2, Stream, Bottom) :-
  length(Arcs2, Na2),
  I1 is I + 1,
  graph_morph(N1, N2, Arcs1, Arcs2, I1, 0, Stream, Bottom).

graph_morph(N1, N2, Arcs1, Arcs2, Na1, Na2, Stream, Bottom) :-
  length(Arcs1, Na1_1),
  length(Arcs2, Na2_1),
  Na1 is  Na1_1 - 1,
  Na2 is Na2_1 - 1,
  write_graph_morph(N1, N2, Arcs1, Arcs2, Na1, Na2, Stream, Bottom), !.

graph_morph(N1, N2, Arcs1, Arcs2, I, J, Stream, Bottom) :-
  J1 is J + 1,
  write_graph_morph(N1, N2, Arcs1, Arcs2, I, J, Stream, Bottom),
  graph_morph(N1, N2, Arcs1, Arcs2, I, J1, Stream, Bottom).


write_graph_morph(N1, N2, Arcs1, Arcs2, I, J, Stream, Bottom) :-
  J1 is J + 1,
  I1 is I + 1,
  length(Arcs2, Na2),
  get_edge_ij(Arcs1, I1, 0, A1i0),
  get_edge_ij(Arcs1, I1, 1, A1i1),
  get_edge_ij(Arcs2, J1, 0, A2j0),
  get_edge_ij(Arcs2, J1, 1, A2j1),
  m_arcs(I, J, N1, N2, Na2, X1),
  m_ij(A1i0, A2j0, N2, X2),
  m_ij(A1i1, A2j1, N2, X3),
  Top is N1 + 1,
  implies(X1, X2, Stream, Bottom, Top),
  implies(X1, X3, Stream, Bottom, Top),
  a_and_b_implies(X2, X3, X1, Stream, Bottom, Top).


subgraph_morph(N1, N2, Arcs1, Arcs2, Stream, Bottom) :-
  subgraph_morph(N1, N2, Arcs1, Arcs2, 0, Stream, Bottom).

subgraph_morph(N1, N2, Arcs1, Arcs2, Na1, Stream, Bottom) :-
  length(Arcs1, Na1_1),
  Na1 is Na1_1 - 1,
  write_subgraph_morph(N1, N2, Arcs1, Arcs2, Na1, Stream, Bottom).

subgraph_morph(N1, N2, Arcs1, Arcs2, I, Stream, Bottom) :-
  I1 is I + 1,
  write_subgraph_morph(N1, N2, Arcs1, Arcs2, I, Stream, Bottom),
  subgraph_morph(N1, N2, Arcs1, Arcs2, I1, Stream, Bottom).


write_subgraph_morph(N1, N2, Arcs1, Arcs2, I, Stream, Bottom) :-
  I1 is I + 1,
  length(Arcs2, Na2),
  length(Arcs1, Na1),
  Na1_1 is Na1 - 1,
  is_dummy(I, N1, N2, Na1_1, Na2, X1),
  get_edge_ij(Arcs1, I1, 0, A1i0),
  get_edge_ij(Arcs1, I1, 1, A1i1),
  m_ij(A1i0, N2, N2, X2),
  m_ij(A1i1, N2, N2, X3),
  X_2 is -X2,
  Top is N1 + 1,
  a_and_b_implies(X1, X_2, X3, Stream, Bottom, Top),
  implies(X2, X1, Stream, Bottom, Top),
  implies(X3, X1, Stream, Bottom, Top).


redundant_morph_prop(N1, N2, Arcs1, Arcs2, Stream, Bottom) :-
  redundant_morph_prop(N1, N2, Arcs1, Arcs2, 0, 0, Stream, Bottom).

redundant_morph_prop(_, _, Arcs1, _, Na1, _, _, _) :-
  length(Arcs1, Na1).

redundant_morph_prop(N1, N2, Arcs1, Arcs2, I, N2_1, Stream, Bottom) :-
  I1 is I + 1,
  N2_1 is N2 - 1,
  write_redundant_morph(N1, N2, Arcs1, Arcs2, I, N2_1, Stream, Bottom),
  redundant_morph_prop(N1, N2, Arcs1, Arcs2, I1, 0, Stream, Bottom).

redundant_morph_prop(N1, N2, Arcs1, Arcs2, I, J, Stream, Bottom) :-
  J1 is J + 1,
  write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, Stream, Bottom),
  redundant_morph_prop(N1, N2, Arcs1, Arcs2, I, J1, Stream, Bottom).


write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, Stream, Bottom) :-
  write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, 0, Stream, Bottom).

write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, N2_1, Stream, Bottom) :-
  N2_1 is N2 - 1,
  I1 is I + 1,
  (
    not(is_in_arcs(J, N2_1, Arcs2))
  ->
    (
      get_edge_ij(Arcs1, I1, 0, E1),
      get_edge_ij(Arcs1, I1, 1, E2),
      m_ij(E1, J, N2, X1),
      m_ij(E2, N2_1, N2, X2),
      X_2 is -X2,
      Top is N1 + 1,
      implies(X1, X_2, Stream, Bottom, Top)
    )
  ;
    true
  ).

write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, K, Stream, Bottom) :-
  K1 is K + 1,
  I1 is I + 1,
  (
    not(is_in_arcs(J, K, Arcs2))
  ->
    (
      get_edge_ij(Arcs1, I1, 0, E1),
      get_edge_ij(Arcs1, I1, 1, E2),
      m_ij(E1, J, N2, X1),
      m_ij(E2, K, N2, X2),
      X_2 is -X2,
      Top is N1 + 1,
      implies(X1, X_2, Stream, Bottom, Top)
    )
  ;
    true
  ),
  write_redundant_morph(N1, N2, Arcs1, Arcs2, I, J, K1, Stream, Bottom).


%! bigraph_constraint(+N1, +N2, +Id1, +Id2, +Stream, +Extremal_sepi)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% A can't have the image B through mu if they don't have the same type
%
% @arg N1     Number of vertices in graph 1
% @arg N2     Number of vertices in graph 2
% @arg Id1    id of graph 1
% @arg Id2    id of graph 2
% @arg Extremal_sepi no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
% @arg Stream Stream for the CNF file
bigraph_constraint(N1, N2, Id1, Id2, Stream, Extremal_sepi):-
  N1_1 is N1 - 1,
  N2_1 is N2 - 1,
  numlist(0,N1_1,Vertex_G1),
  numlist(0,N2_1,Vertex_G2),
  forall(
    (
      member(A, Vertex_G1),
      member(B, Vertex_G2)
    ),
    (
      (
        get_type(A, Id1, TypeA),
        get_type(B, Id2, TypeB),
        not(TypeA is TypeB)
      )
    ->
      write_bigraph_constraint(A, B, N1, N2, Stream, Extremal_sepi)
    ;
      true
    )
  ).


%! write_bigraph_constraint(+A, +B, +N1, +N2, +Stream, +Extremal_sepi)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% A and B don't have the same type, thus A can't have the image B
%
% @arg A      Vertex A in graph 1
% @arg B      Vertex B in graph 2
% @arg N1     Number of vertices in graph 1
% @arg N2     Number of vertices in graph 2
% @arg Extremal_sepi no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
% @arg Stream Stream for the CNF file
write_bigraph_constraint(A, B, N1, N2, Stream, Extremal_sepi):-
  m_ij(A, B, N2, X),
  X1 is -X,
  (
    Extremal_sepi \= no
  ->
    Top is N1 + 1,
    format(Stream, "~d ~d 0~n", [Top, X1])
  ;
    format(Stream, "~d 0~n", X1)
  ).


%! bottom_min_constraint(+N1, +N2, +W, +Stream)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% write '-mu(I)=bottom' for all vertex I of graph 1
%
% @arg N1     Number of vertices in graph 1
% @arg N2     Number of vertices in graph 2
% @arg W      Weight of the soft SAT clause
% @arg Stream Stream for the CNF file
bottom_min_constraint(N1, N2, W, Stream) :-
  N1_1 is N1 - 1,
  numlist(0,N1_1,Vertex_G1),
  forall(
    member(I, Vertex_G1),
    (
      m_ij(I, N2, N2, X),
      format(Stream, "~d -~d 0\n", [W, X])
    )
  ).


%! bottom_max_constraint(+N1, +N2, +W, +Stream)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% write 'mu(I)=bottom' for all vertex I of graph 1
%
% @arg N1     Number of vertices in graph 1
% @arg N2     Number of vertices in graph 2
% @arg W      Weight of the soft SAT clause
% @arg Stream Stream for the CNF file
bottom_max_constraint(N1, N2, W, Stream) :-
  N1_1 is N1 - 1,
  numlist(0,N1_1,Vertex_G1),
  forall(
    member(I, Vertex_G1),
    (
      m_ij(I, N2, N2, X),
      format(Stream, "~d ~d 0\n", [W, X])
    )
  ).
