:- module(
  sepi,
  [
    % Public API
    search_reduction/2,
    extremal_sepi/1,
    merge_restriction/1
  ]
).


% Only for separate compilation/linting
:- use_module(doc).
:- use_module(biocham).
:- use_module(sepi_infos).
:- use_module(sepi_util).
:- use_module(sepi_core).
:- use_module(sepi_sat).
:- use_module(sepi_old_merge).
:- use_module(sepi_mapping).


:- doc('This section describes commands to detect model reduction
  relationships between reaction models based solely on the
  structure of their reaction graph \\cite{GSF10bi}.
  The commands below check the existence of a subgraph
  epimorphism (SEPI) \\cite{GFS14dam}, i.e. a graph reduction
  from one graph to a second graph, obtained by deleting and/or
  merging species and/or reactions. A SAT solver is used to
  solve this NP-complete problem.').


:- grammar(extremal_sepi).

extremal_sepi(no).
:- grammar_doc('\\emphright{Search standard reduction.}').

extremal_sepi(minimal_deletion).
:- grammar_doc('\\emphright{Search reduction that minimises bottom (i.e., the number of deletions).}').

extremal_sepi(maximal_deletion).
:- grammar_doc('\\emphright{Search reduction that maximises bottom (i.e., the number of deletions).}').

:- grammar(merge_restriction).

merge_restriction(no).
:- grammar_doc('\\emphright{Search standard reduction.}').

merge_restriction(neighbours).
:- grammar_doc('\\emphright{Search reduction with local two-neighbour restriction (cf. report chapter 5).}').

merge_restriction(old).
:- grammar_doc('\\emphright{Search reduction with old two-neighbour restriction (prior to 2019).}').


:- initial(option(mapping_restriction: [])).
:- initial(option(merge_restriction: no)).
:- initial(option(timeout: 180)).
:- initial(option(all_reductions: no)).
:- initial(option(distinct_species: no)).
:- initial(option(max_nb_reductions: 200)).
:- initial(option(extremal_sepi: no)).
:- initial(option(stats: no)).


search_reduction(FileName1, FileName2) :-
  biocham_command,
  type(FileName1, input_file),
  type(FileName2, input_file),
  doc('checks whether there exists one model reduction from a
    first Biocham model to a second model, and returns the
    first model reduction found, as a description of a graph
    morphism (SEPI) from the reaction graph of the first model
    to the second. Optionally, a partial mapping of the form
    [\'label1\' -> \'label2\', \'label3\' -> \'deleted\'] can
    be given to restrict the search to reductions satisfying
    this mapping of molecular names.
    Another option restricts the merge operation to nodes 
    sharing at least one neigbour in the reaction graph.
    A timeout option can be given in seconds (default is 180s).
    Note that the files FileName1 and FileName2 will be loaded,
    therefore any previous model will be overwritten and some
    biocham commands might be executed.
  '),
  option(mapping_restriction, [basic_influence], Mapping,
    'enforce the given mappings in the SEPI'),
  option(merge_restriction, merge_restriction, Merge,
    'restrict merges according to some criterion'),
  option(timeout, number, Timeout, 'timeout for the (Max)SAT solver'),
  option(all_reductions, yesno, Reductions,
    'specifies if solver is looking for all SEPI reductions or not'),
  option(distinct_species, yesno, Distinct_species,
    'specifies if solver is listing only SEPIs with distinct species (cf. report section 6.6)'),
  option(max_nb_reductions, number, MaxReduc,
    'limits the number of SEPI reductions the solver is looking for'),
  option(extremal_sepi, extremal_sepi, Bottom, 'defines the type of reduction searched (cf. report chapter 3)'),
  option(stats, yesno, _Stats, 'display computation time'),
  debug(sepi, "Debugging sepi", []),
  (
    get_graphs_infos(FileName1, FileName2, G1, G2),
    debug(sepi, "G1: ~w", [G1]),
    debug(sepi, "G2: ~w", [G2]),
    tmp_file_stream(text, FileRoot, StreamFileInInit),
    close(StreamFileInInit),
    % .wcnf extension needed for MAX SAT solver and doesn't disturb Glucose
    string_concat(FileRoot, ".wcnf", FileIn),
    debug(sepi, "Will be writing clauses to: ~w", [FileIn]),
    with_option([
        mapping_restriction: Mapping, 
        merge_restriction: Merge,
        timeout: Timeout,
        all_reductions: Reductions,
        distinct_species: Distinct_species,
        max_nb_reductions: MaxReduc,
        extremal_sepi: Bottom
      ], 
      main_search_reduction(G1, G2, FileIn)
    ),
    delete_file(FileRoot),
    delete_file(FileIn)
  ;
    write('no sepi found\n')
  ),
  !.


%! main_search_reduction(+G1, +G2, +FileIn)
%
% need to call the function with with_option([], ...)
% because load_model in get_graphs_infos reset all options
%
% writes all clauses to the CNF file then call a SAT solver
%
% each graph is represented by a list
% each vertex is represented by an integer
% Specie vertices: [[0, Nb_species - 1]] 
% Reaction vertices: [[Nb_species, Nb_vertices - 1]]
% Edges_list example: [(0,3), (1,3), (3,2)]
%
% @arg G1     graph 1 [Nb_vertices, Nb_species, Edges_list, Id]
% @arg G2     graph 2 [Nb_vertices, Nb_species, Edges_list, Id]
% @arg FileIn file for the CNF formula
main_search_reduction(G1, G2, FileIn):-
  get_option(mapping_restriction, Mapping),
  get_option(merge_restriction, Merge),
  get_option(timeout, Timeout),
  get_option(all_reductions, Reductions),
  get_option(max_nb_reductions, MaxReduc),
  get_option(extremal_sepi, Bottom),
  debug(sepi, "Options: Mapping: ~w, Merge: ~w, Timeout, ~w, All Reductions: ~w, Max Reductions: ~w, Reduction: ~w", [Mapping, Merge, Timeout, Reductions, MaxReduc, Bottom]),
  open(FileIn, write, Stream),
  once(search_sepi(G1, G2, Stream, Merge, Bottom)),
  once(constraints(G1, G2, Mapping, Stream)),
  close(Stream),
  NbDeleteInit = "-1",
  solve_sat(G1, G2, FileIn, Reductions, Bottom, Merge, MaxReduc, NbDeleteInit, NbReductions), 
  format("Number of reductions: ~w~n", [NbReductions]).


:- doc('\\begin{example}Detecting Michaelis-Menten reductions:').
:- biocham_silent(clear_model).
:- biocham('load(library:examples/sepi/MM1.bc)').
:- biocham('list_model').
:- biocham('load(library:examples/sepi/MM2.bc)').
:- biocham('list_model').
:- biocham('search_reduction(library:examples/sepi/MM2.bc, library:examples/sepi/MM1.bc)').
:- biocham('search_reduction(library:examples/sepi/MM1.bc, library:examples/sepi/MM2.bc, mapping_restriction: [E->A])').
:- doc('\\end{example}').

