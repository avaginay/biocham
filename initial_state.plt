:- use_module(library(plunit)).

:- begin_tests(initial_state, [setup((clear_model, reset_options))]).

test(
  'present',
  [true(InitialState == [initial_state(a=1), initial_state(b=1)])]
) :-
  clear_model,
  command(present({ a, b })),
  all_items([kind: initial_state], InitialState).

test(
  'absent',
  [true(InitialState == [absent(a), absent(b)])]
) :-
  clear_model,
  command(absent({ a, b })),
  all_items([kind: initial_state], InitialState).

test(
  'undefined',
  [true(InitialState == [initial_state(a=1), absent(b)])]
) :-
  clear_model,
  command(present({ a, b, c })),
  command(absent({ b, c, e })),
  command(undefined({ c, d, e })),
  all_items([kind: initial_state], InitialState).

test(
  'make_absent_not_present',
  [true(InitialState == [initial_state(a=1), absent(b)])]
) :-
  clear_model,
  add_reaction(a => b),
  command(present(a)),
  make_absent_not_present,
  all_items([kind: initial_state], InitialState).

test(
  'make_present_not_absent',
  [true(InitialState == [absent(a), initial_state(b=1)])]
) :-
  clear_model,
  add_reaction(a => b),
  command(absent(a)),
  make_present_not_absent,
  all_items([kind: initial_state], InitialState).

test(
  'present/2',
  [true((InitialState, Concentration) == ([initial_state(a = 1), initial_state(b = k)], 3))]
) :-
  clear_model,
  command(present(a, 1)),
  command(present(b, k)),
  command(parameter(k=3)),
  all_items([kind: initial_state], InitialState),
  get_initial_concentration(b, Concentration).

test(
  'initial_state/1',
  [true((Conc_a, Conc_b) == (1, 2))]
) :-
  clear_model,
  command(initial_state(a=1, b=2)),
  get_initial_concentration(a, Conc_a),
  get_initial_concentration(b, Conc_b).

:- end_tests(initial_state).
