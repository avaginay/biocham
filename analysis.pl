:- module(
  analysis,
  [
    analyze/0
  ]
).


:- doc('This section describes a standard model analysis method which uses all the concepts of Biocham.').

analyze :-
    biocham_command,
    doc('peforms the following commands on the current model: generate CTL specification, reduce model,... to be completed with static analyses, generation of FO-LTL specification, sensitivity, robustness etc..'),
    list_reactions,
    % dimension analysis
    % invariants
    % sepis in BioModels
    generate_ctl,
    list_ctl,
    reduce_model_ctl,
    list_reactions,
    % generate_foltl
    % sensitivty
    % robustness
    % reduce_model_foltl
    % bifurcations on stable
    true.


