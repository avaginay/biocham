:- use_module(library(plunit)).
% not necessary, except for separate compilation
:- use_module(reaction_rules).

:- begin_tests(reduce, [setup((clear_model, reset_options))]).

test(
  'reduce_3_inhibitions',
  [
    setup(clear_model)
  ]
) :-
  command('_' => a),
  command('_' => b),
  command('_' => c),
  command(a => '_'),
  command(b => '_'),
  command(c => '_'),
  once(
    reduce:reduce_model(
      'oscil'('a'),
      Removed
    )
  ),
  assertion(Removed = ['MA'(1) for _=>b,'MA'(1) for _=>c,'MA'(1) for b=>_,'MA'(1) for c=>_]).


:- end_tests(reduce).
