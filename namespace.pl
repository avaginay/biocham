:- module(
  namespace,
  [
    % Public API
    identifier_kind/3,
    check_identifier_kind/2,
    check_no_free_identifiers/0,
    update_identifier_kinds/1
  ]
).


:- devdoc('This file manages the different identifiers of a model (in the sense of the grammatical token identifier, not name).
The identifier kinds are
\\begin{itemize}
\\item parameter
\\item function
\\item object (molecule)
\\item free
\\end{itemize}
The presence of a free identifier blocks some commands such as numerical simulation for instance.
The tokens name (e.g. for reaction names) are not checked.

The data structure is a triple kind(ModelId, Ident, Kind).
').



:- devdoc('\\section{Public API}').


:- dynamic(identifier_kind/3).


check_identifier_kind(Ident, NeededKind) :-
  single_model(ModelId),
  (
    identifier_kind(ModelId, Ident, Kind),
    Kind \= free,
    NeededKind \= Kind
  ->
    throw(
      error(
        already_defined_with_another_kind(Ident, Kind, NeededKind),
        check_identifier_kind
      )
    )
  ;
    true
  ).


prolog:message(
  error(already_defined_with_another_kind(Ident, Kind, NeededKind))
) -->
  ['~a cannot be ~a because it is already used as ~a.'-[Ident, NeededKind, Kind]].



check_no_free_identifiers :-
  single_model(ModelId),
  findall(
    Ident,
    identifier_kind(ModelId, Ident, free),
    FreeIdents
  ),
  (
    FreeIdents = []
  ->
    true
  ;
    throw(error(not_defined(FreeIdents)))
  ).


prolog:message(error(not_defined(FreeIdents))) -->
  {
    findall('~a', member(_, FreeIdents), As),
    atomic_list_concat(As, ', ', Format),
    (
      FreeIdents = [_]
    ->
      Verb = 'is'
    ;
      Verb = 'are'
    ),
    atomic_list_concat([Format, Verb, 'not defined'], ' ', FullFormat)
  },
  [FullFormat-FreeIdents].


update_identifier_kinds(ModelId) :-
  retractall(identifier_kind(ModelId, _, _)),
  \+ (
    item([parent: ModelId, kind: parameter, item: parameter(Object = _Value)]),
    \+ (
      assertz(identifier_kind(ModelId, Object, parameter))
    )
  ),
  \+ (
    item([parent: ModelId, kind: function, item: function(Head = _Body)]),
    Head =.. [Object | _],
    \+ (
      assertz(identifier_kind(ModelId, Object, function))
    )
  ),
  enumerate_objects_and_free(ModelId, reaction),
  enumerate_objects_and_free(ModelId, influence),
  \+ (
    directly_inherits_from(SubModelId, ModelId),
    \+ (
      update_identifier_kinds(SubModelId)
    )
  ).


:- devdoc('\\section{Private predicates}').


enumerate_objects_and_free(ModelId, Kind) :-
  \+ (
    item([parent: ModelId, kind: Kind, item: Item]),
    \+ (
      grammar_iter(
        Kind,
        [object: namespace:set_kind(ModelId, object)],
        Item
      )
    )
  ),
  \+ (
    item([parent: ModelId, kind: initial_state, item: Item]),
    (
      Item = initial_state(Object = _)
    ;
      Item = absent(Object)
    ),
    \+ (
      set_kind(ModelId, object, Object)
    )
  ),
  \+ (
    item([parent: ModelId, kind: Kind, item: Item]),
    \+ (
      grammar_iter(
        Kind,
        [identifier: namespace:set_kind_if_not_set(ModelId, free)],
        Item
      )
    )
  ).


set_kind(ModelId, Kind, Identifier) :-
  assertz(identifier_kind(ModelId, Identifier, Kind)).


set_kind_if_not_set(ModelId, Kind, Identifier) :-
  (
    identifier_kind(ModelId, Identifier, _Kind)
  ->
    true
  ;
    set_kind(ModelId, Kind, Identifier)
  ).
