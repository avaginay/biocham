:- module(
  markov,
  [
    % API
    stochastic_simulation/4
  ]
).


:- dynamic(is_boolean/0).
:- dynamic(last_time/1).  % last written state's time

stochastic_simulation(Until, Propensities, Time, Boolean) :-
  (
    Boolean == true
  ->
    assertz(is_boolean)
  ;
    retractall(is_boolean)
  ),
  store_parameters,
  get_initial_numbers(InitialNumbers),
  get_conditions_and_actions(Conditions, Actions),
  list_to_assoc(['Time'-0 | InitialNumbers], InitState),
  init_state(InitState),
  retractall(last_time(_)),
  assertz(last_time(0.0)),
  get_option(stochastic_thresholding, Threshold),
  %statistics(runtime,_),
  markov_run(InitState, Conditions, Actions, Until, Propensities, Time,
    Threshold),
  %statistics(runtime,[_,T]),
  %TT is T/1000,
  %format("Simulation time: ~ps~n",[TT]),
  make_state_table,
  restore_parameters.


% hack to force initial state from outside without items
:- dynamic(initial_state/1).


get_initial_numbers(InitialNumbers) :-
  initial_state(InitialNumbers),
  !.

get_initial_numbers(InitialNumbers) :-
  get_option(stochastic_conversion, Rate),
  enumerate_molecules(Molecules),
  findall(
    Molecule-Initial,
    (
      member(Molecule, Molecules),
      get_initial_concentration(Molecule, InitialC),
      (
        is_boolean
      ->
        Initial is round(sign(InitialC))
      ;
        Initial is round(InitialC*Rate)
      )
    ),
    InitialNumbers
  ).


:- dynamic(cache_conditions/2).


get_conditions_and_actions(Conditions, Actions) :-
  nomake_state_table,
  cache_conditions(Conditions, Actions),
  !.


get_conditions_and_actions(Conditions, Actions) :-
  findall(
    (Expr, Reactants, Products),
    (
        item([kind: reaction, item: Item]),
        reaction(
          Item,
          [
            kinetics: Kinetics,
            reactants: Reactants,
            inhibitors: Inhibitors,
            products: Products
          ]
        ),
        get_option(boolean_semantics, Semantics),
        (
            Semantics == positive
        ->
            kinetics(Reactants, Inhibitors, Kinetics, Expr)
        ;
            kinetics(Reactants, Inhibitors, Kinetics, KExpr),
            inhibitors_to_kinetics(Inhibitors, Test),
            Expr = Test * KExpr
        )
      ;
        item([kind: influence, item: Item]),
        influence(
          Item,
          [
            force: Force,
            positive_inputs: Inputs,
            negative_inputs: Inhibitors,
            sign: Sign,
            output: Output
          ]
        ),
        (
          Sign = '+'
        ->
          Products = [1*Output],
          Reactants = [],
          (
            is_boolean
          ->
            AllInputs = [1 - Output | Inputs]
          ;
            AllInputs = Inputs
          )
        ;
          Products = [],
          Reactants = [1*Output],
          % for inhibitions add output to inputs for default force MA(1)
          AllInputs = [Output | Inputs]
        ),
        maplist(influence_editor:add_coefficient, AllInputs, InputsWithCoeff),
        get_option(boolean_semantics, Semantics),
        (
            Semantics == positive
        ->
            kinetics(InputsWithCoeff, Inhibitors, Force, Expr)
        ;
            kinetics(InputsWithCoeff, Inhibitors, Force, KExpr),
            inhibitors_to_kinetics(Inhibitors, Test),
            Expr = Test * KExpr
        )
    ),
    AllStuff
  ),
  enumerate_molecules(Molecules),
  triplets_to_conditions_and_actions(AllStuff, Conditions, Actions, Molecules),
  prepare_events(Molecules),
  (
    nomake_state_table
  ->
    assertz(cache_conditions(Conditions, Actions))
  ;
    true
  ).


inhibitors_to_kinetics([], 1) :-
  !.

inhibitors_to_kinetics(Inhibitors, Expr) :-
  foldl(is_plus, Inhibitors, 0, Test),
  % FIXME can be wrong if some number of molecules is negative
  Expr = 1 - sign(Test).


is_plus(X, Y, X + Y).


triplets_to_conditions_and_actions([], [], [], _).

triplets_to_conditions_and_actions(
  [(K, Reactants, Products) | Stuff],
  [KK | Conditions],
  [Action | Actions],
  Molecules
) :-
  normalize_condition(K, KK, Molecules),
  reaction_editor:simplify_catalyst(
    Reactants, Products,
    SReactants, _, SProducts
  ),
  stoichiometry_to_action(SReactants, '-', [], A1),
  stoichiometry_to_action(SProducts, '+', A1, Action),
  triplets_to_conditions_and_actions(Stuff, Conditions, Actions, Molecules).


normalize_condition(Cond, SCond, _Molecules) :-
  is_boolean,
  !,
  if_molecule_divide(1, [], Cond, NCond),
  simplify(NCond, SCond).

normalize_condition(Cond, SCond, Molecules) :-
  get_option(stochastic_conversion, Rate),
  if_molecule_divide(Rate, Molecules, Cond, NCond),
  simplify(Rate*NCond, SCond).


if_molecule_divide(Rate, _Molecules, [M], [M]/Rate) :-
  !.

if_molecule_divide(Rate, Molecules, M, MM) :-
  atom(M),
  !,
  (
    member(M, Molecules)
  ->
    MM = M/Rate
  ;
    MM = M
  ).

if_molecule_divide(Rate, Molecules, M, MM) :-
  term_morphism(markov:if_molecule_divide(Rate, Molecules), M, MM).


stoichiometry_to_action([], _, A, A).

stoichiometry_to_action([_*M | L], '+', A, B) :-
  is_boolean,
  !,
  stoichiometry_to_action(L, '-', [M-1 | A], B).

stoichiometry_to_action([_*M | L], '-', A, B) :-
  is_boolean,
  !,
  stoichiometry_to_action(L, '-', [M-0 | A], B).

stoichiometry_to_action([N*M | L], Op, A, B) :-
  Next =.. [Op, M, N],
  stoichiometry_to_action(L, Op, [M-Next | A], B).


markov_run(State, Conditions, Actions, Until, PropensitiesType, TimeType,
  Threshold) :-
  maplist(eval_in_state(State), Conditions, Propensities),
  (
   PropensitiesType = 'boolean'
  ->
   maplist(sign, Propensities, TypedPropensities)
  ;
   TypedPropensities = Propensities
  ),
  sum_list(TypedPropensities, Alpha),
  (
   Alpha =:= 0
  ->
   NewTime = Until,
   NewState = State
  ;
   (
    TimeType = 'discrete'
   ->
    DeltaT = 1
   ;
    DeltaT is log(1/random_float)/Alpha
   ),
   eval_in_state(State, 'Time', Time),
   NewTime is Time + DeltaT,
   Bound is Alpha*random_float,
				% write(select_action(Bound, Actions, Propensities)),nl,
   select_action(Bound, Actions, TypedPropensities, Action),
   catch(
	 set_state(Action, State, NewState),
	 stochastic_bound(Molecule),
	 true)
  ),
  (nonvar(Molecule)
  ->
   format("Stochastic bound overflow on molecular species: ~a~n",[Molecule])
  ;
   set_state(['Time'-NewTime], NewState, NextState),
   handle_events(NextState),
   retract(last_time(LastTime)),
   (
				% FIXME this is a very rough sampling
    (
     (NewTime >= Until)
    ;
     (NewTime - LastTime >= min(1, Until/float(Threshold)))
    )
   ->
    write_state(NextState),
    assertz(last_time(NewTime))
   ;
    assertz(last_time(LastTime))
   ),
   (
    NewTime >= Until
   ->
    true
   ;
    markov_run(NextState, Conditions, Actions, Until, PropensitiesType, TimeType, Threshold)
   )
  ).


:- dynamic(original_parameters/2).
:- dynamic(stochastic_events/2).


store_parameters :-
  retractall(original_parameters(_, _)),
  forall(
    item([kind: parameter, key: Parameter, item: parameter(Parameter = Value)]),
    assertz(original_parameters(Parameter, Value))
  ).


restore_parameters :-
  forall(
    retract(original_parameters(Parameter, Value)),
    set_parameter(Parameter, Value)
  ).


prepare_events(Molecules) :-
  retractall(stochastic_events(_, _)),
  all_items([kind: event], Events),
  all_items([kind: time_event], TimeEvents),
  append(Events, TimeEvents, AllEvents),
  get_option(stochastic_conversion, Rate),
  forall(
    member(event(Condition, AssignmentList), AllEvents),
    (
      normalize_condition(Condition, SCond, Molecules),
      (
        % normalize does multiply kinetic rates by conversion rate
        SCond = Rate * NCond
      ->
        true
      ;
        NCond = SCond
      ),
      assertz(stochastic_events(NCond, AssignmentList))
    )
  ).


handle_events(State) :-
  forall(
    stochastic_events(Condition, AssignmentList),
    (
      eval_in_state(State, Condition, V),
      V > 0.5
    ->
      maplist(apply_assignment(State), AssignmentList)
    ;
      true
    )
  ).


apply_assignment(State, Parameter = Expression) :-
  eval_in_state(State, Expression, Value),
  set_parameter(Parameter, Value).


eval_in_state(State, [Object], Value) :-
  !,
  get_assoc(Object, State, Value).

eval_in_state(_State, Object, Value) :-
  atom(Object),
  parameter_value(Object, Value),
  !.

eval_in_state(_State, true, 1) :-
  !.

eval_in_state(_State, false, 0) :-
  !.

eval_in_state(State, or(A, B), Value) :-
  !,
  eval_in_state(State, A, VA),
  eval_in_state(State, B, VB),
  Value is min(1, VA + VB).

eval_in_state(State, and(A, B), Value) :-
  !,
  eval_in_state(State, A, VA),
  eval_in_state(State, B, VB),
  Value is VA * VB.

eval_in_state(State, not(A), Value) :-
  !,
  eval_in_state(State, A, VA),
  Value is 1 - VA.

eval_in_state(State, Condition, Value) :-
  Condition =.. [Op, A, B],
  memberchk(Op, ['=', '<', '<=', '>', '>=']),
  !,
  eval_in_state(State, A, VA),
  eval_in_state(State, B, VB),
  VCondition =.. [Op, VA, VB],
  (
    VCondition
  ->
    Value = 1
  ;
    Value = 0
  ).

eval_in_state(State, if(then(Condition, else(A, B))), Value) :-
  eval_in_state(State, Condition, VCondition),
  (
    VCondition > 0.5
  ->
    eval_in_state(State, A, Value)
  ;
    eval_in_state(State, B, Value)
  ).

eval_in_state(State, Object, Value) :-
  atom(Object),
  !,
  get_assoc(Object, State, Value).

eval_in_state(State, Expr, Value) :-
  term_morphism(markov:eval_in_state(State), Expr, Term),
  % FIXME should hande functions
  Value is Term.


select_action(Bound, [Action | _], [Propensity | _], Action) :-
  Bound < Propensity,
  !.

select_action(Bound, [_ | Actions], [P | Propensities], Action) :-
  NextBound is Bound - P,
  select_action(NextBound, Actions, Propensities, Action).


set_state([], State, State).

set_state([Key-Value | Actions], State, NewState) :-
  eval_in_state(State, Value, Eval),
  get_option(stochastic_bound,B),
  (Eval>B % that test should be limited to numbers of molecules that are reactants only (not functions nor non-reactants)
  ->
   throw(stochastic_bound(Key))
  ;
   put_assoc(Key, State, Eval, NextState),
   set_state(Actions, NextState, NewState)
  ).


:- dynamic(state/1).


init_state(State) :-
  retractall(state(_)),
  del_assoc('Time', State, Time, Rest),
  assoc_to_keys(Rest, Keys),
  assertz(state(['#Time' | Keys])),
  assoc_to_values(Rest, Values),
  assertz(state([Time | Values])).


write_state(State) :-
  del_assoc('Time', State, Time, Rest),
  assoc_to_values(Rest, Values),
  assertz(state([Time | Values])).


% allow avoiding tables alltogether
:- dynamic(nomake_state_table/0).


make_state_table :-
  nomake_state_table,
  !.

make_state_table :-
  findall(
    Row,
    (
      state(State),
      Row =.. [row | State]
    ),
    Table
  ),
  add_table(stochastic_simulation, Table).


export_state(Filename) :-
  automatic_suffix(Filename, '.csv', write, FilenameCsv),
  working_directory(Dir, Dir),
  absolute_file_name(FilenameCsv, AbsoluteFilenameCsv, [relative_to(Dir)]),
  retract(state(Header)),
  !,
  HeaderRow =.. [row | Header],
  setup_call_cleanup(
    open(AbsoluteFilenameCsv, write, Out),
    (
      % properly quote header line
      csv_write_stream(Out, [HeaderRow], []),
      forall(
        state(State),
        (
          atomic_list_concat(State, ', ', Atom),
          write(Out, Atom),
          nl(Out)
        )
      )
    ),
    (
      asserta(state(Header)),
      close(Out)
    )
  ).
