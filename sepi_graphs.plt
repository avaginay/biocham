:- use_module(library(plunit)).

:- begin_tests(sepi, [setup((clear_model, reset_options))]).


has_start(Start, String) :-
  atom_concat(Start, _, String).


has_end(End, String) :-
  atom_concat(_, End, String).


count_start(Atom, List, Count) :-
  include(has_start(Atom), List, L),
  length(L, Count).


count_end(Atom, List, Count) :-
  include(has_end(Atom), List, L),
  length(L, Count).


test('sepi_fail_nosat') :-
  with_output_to(
    atom(Output),
    command('search_reduction("library:examples/sepi/MM2.bc", "library:examples/sepi/MM1.bc").')
  ),
  Output = 'no sepi found\n'.


test('sepi_fail_sat') :-
  with_output_to(
    atom(Output),
    command('search_reduction("library:examples/sepi/MM4.bc", "library:examples/sepi/MM3.bc").')
  ),
  Output = 'no sepi found\nNumber of reductions: 0\n'.


test('sepi_success') :-
  with_output_to(
    atom(Result),
    command('search_reduction("library:examples/sepi/MM1.bc", "library:examples/sepi/MM2.bc").')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('sepi', Split, 1).


test('all_reductions_sucess') :-
  with_output_to(
    atom(Result),
    command('search_reduction("library:examples/sepi/MM1.bc", "library:examples/sepi/MM2.bc", all_reductions: yes).')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('sepi', Split, 3).


test('bottom_min_success') :-
  with_output_to(
    atom(Result),
    command('search_reduction("library:examples/sepi/MM1.bc", "library:examples/sepi/MM2.bc", extremal_sepi: minimal_deletion).')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_end('-> deleted', Split, 1).


test('bottom_min_fail') :-
  with_output_to(
    atom(Result),
    command('search_reduction("library:examples/sepi/MM1.bc", "library:examples/sepi/MM3.bc", extremal_sepi: minimal_deletion).')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('no sepi found', Split, 1).


test('all_bottom_min_success') :-
  with_output_to(
    atom(Result),
    command('search_reduction("library:examples/sepi/MM4.bc", "library:examples/sepi/MM2.bc", all_reductions: yes, extremal_sepi: minimal_deletion).')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('sepi', Split, 3).


test('bottom_max_success') :-
  with_output_to(
    atom(Result),
    command('search_reduction("library:examples/sepi/MM1.bc", "library:examples/sepi/MM2.bc", extremal_sepi: maximal_deletion).')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_end('-> deleted', Split, 2).


test('all_bottom_max_success') :-
  with_output_to(
    atom(Result),
    command('search_reduction("library:examples/sepi/MM1.bc", "library:examples/sepi/MM2.bc", extremal_sepi: maximal_deletion, all_reductions: yes).')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('sepi', Split, 2).


:- end_tests(sepi).
