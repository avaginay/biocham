:- module(
  parameters,
  [
    % Commands
    parameter/1,
    list_parameters/0,
    delete_parameter/1,
    % Public API
    parameter_value/2,
    list_model_parameters/0,
    set_parameter/2,
    set_parameters/1,
    copy_parameters/0,
    paste_parameters/0
  ]
).

% Only for separate compilation/linting
:- use_module(doc).

:- devdoc('\\section{Commands}').


parameter(ParameterList) :-
  biocham_command(*),
  type(ParameterList, '*'(parameter_name = number)),
  doc('sets the value of parameters.'),
  \+ (
    member(Parameter = Value, ParameterList),
    \+ (
      set_parameter(Parameter, Value)
    )
  ).


list_parameters :-
  biocham_command,
  doc('shows the values of all known parameters.'),
  list_items([kind: parameter]).


delete_parameter(ParameterSet) :-
  biocham_command(*),
  type(ParameterSet, '*'(parameter_name)),
  doc('deletes some parameters'),
  \+ (
    member(Parameter, ParameterSet),
    \+ (
      delete_item([kind: parameter, key: Parameter])
    )
  ).


:- devdoc('\\section{Public API}').


parameter_value(Parameter, Value) :-
  devdoc('
    succeeds for every \\argument{Parameter}=\\argument{Value} pair.
  '),
  item([kind: parameter, key: Parameter, item: parameter(Parameter = Value)]).


list_model_parameters :-
  devdoc('
    lists all the parameters in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  (
    item([no_inheritance, kind: parameter])
  ->
    write('parameter(\n'),
    assertz(first),
    write_successes(
      item([
        no_inheritance, kind: parameter, item: parameter(Parameter = Value)
      ]),
      write(',\n'),
      format('  ~w = ~w', [Parameter, Value])
    ),
    write('\n).\n')
  ;
    true
  ).


set_parameter(Parameter, Value) :-
  check_identifier_kind(Parameter, parameter),
  change_item([], parameter, Parameter, parameter(Parameter = Value)).


set_parameters([]).

set_parameters([Parameter = Value | Tail]) :-
  set_parameter(Parameter, Value),
  set_parameters(Tail).

:- dynamic(saved_parameter/1).

copy_parameters :-
  forall(
    item([kind: parameter, item:P]),
    assertz(saved_parameter(P))
  ).

paste_parameters :-
  forall(
    saved_parameter(parameter(P=V)),
    set_parameter(P, V)
  ),
  retractall(saved_parameter).
