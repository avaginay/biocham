'''generate output data for interactive plotting and other images'''
import base64
import os
import csv
import copy

from bokeh.plotting import figure
from bokeh.io.notebook import publish_display_data
from bokeh.io import output_notebook, show, push_notebook
from bokeh.models import HoverTool, ColumnDataSource, Legend, Range1d
from bokeh.models.widgets import Button
from bokeh.models.callbacks import CustomJS
from bokeh.layouts import column
from bokeh.palettes import Category10_10    # pylint:disable=no-name-in-module
from bokeh.resources import INLINE
from bokeh.embed import components

bokeh_loaded = False    # pylint:disable=invalid-name

def display_data_for_image(filename, handle=None, data_source=None, comm=None):
    '''return display_data record for dynamic plots of the given filename'''

    image_type = filename.split('.')[-1]

    if image_type == 'csv':
        plot_data = get_data_and_meta(filename)
        # plot_data is modified in create_bokeh_plot
        # this is just a hack to have correct plot in GUI for now
        gui_plot_data = copy.deepcopy(plot_data)
        fig, cds = create_bokeh_plot(plot_data)
        gui_fig, gui_cds = create_bokeh_plot(gui_plot_data, gui=True)
        against = plot_data['against']
        return handle_bokeh_plot(fig, gui_fig, against, cds=cds, gui_cds=gui_cds,
                                 old_handle=handle, old_data=data_source, comm=comm)

    with open(filename, 'rb') as file:
        image = file.read()
    # let us keep images (may be costly to produce as result of scan_parameters)
    # os.unlink(filename) 

    if image_type == 'svg':
        image_type = 'image/svg+xml'
        image_data = image.decode('utf-8')
    elif image_type == 'tex':
        image_type = 'text/latex'
        image_data = image.decode('utf-8')
    else:
        image_type = 'image/' + image_type
        image_data = base64.b64encode(image).decode('ascii')

    if comm:
        comm.send({
            'type': 'image',
            'image_type': image_type,
            'image_data': image_data,
        });

    publish_display_data(data={image_type: image_data})

    return None


def get_data_and_meta(filename):
    '''read data and metadata from filename and filenameX
    Rq: filenameX is written by ???'''
    with open(filename + 'X', 'rb') as file:
        metadata = file.read().decode('utf-8').splitlines()
    os.unlink(filename + 'X')
    logscale = metadata[0].split(' ')[1]
    to_show = metadata[1].split(' ')[1:]
    against = metadata[2].split(' ')[1]
    axes = metadata[3].split(' ')[1:5]
    axes_names = (' '.join(metadata[4].split(' ')[1:]), ' '.join(metadata[5].split(' ')[1:]))

    with open(filename, newline='') as csvfile:
        lines = list(csv.reader(csvfile))
    os.unlink(filename)

    return {'lines': lines,
            'logscale': logscale,
            'to_show': to_show,
            'against': against,
            'axes': axes,
            'axes_names': axes_names}


def create_bokeh_plot(data, gui=False):
    '''plot using bokeh from python'''
    lines, logscale, to_show, against = data['lines'], data['logscale'], \
    data['to_show'], data['against']
    xmin, xmax, ymin, ymax = ['*' if d == 'auto' else float(d) for d in data['axes']]

    headers = lines.pop(0)
    headers[0] = 'Time'

    cds = ColumnDataSource({name: [float(lines[i][index])
                                   for i in range(len(lines))]
                            for index, name in enumerate(headers)})

    global bokeh_loaded     # pylint:disable=global-statement,invalid-name
    if not bokeh_loaded:
        output_notebook(INLINE, hide_banner=True)
        bokeh_loaded = True

    fig = figure(
        x_axis_label=data['axes_names'][0],
        y_axis_label=data['axes_names'][1],
        x_axis_type='log' if 'x' in logscale else 'linear',
        y_axis_type='log' if 'y' in logscale else 'linear',
        tools='pan,wheel_zoom,box_zoom,reset,save,undo',
        active_drag='box_zoom',
        active_scroll='auto',
        toolbar_location='below',
        toolbar_sticky=False,
        plot_height=400,
        plot_width=540,     # will increase by 100 for each caption column,
        background_fill_alpha=0.3,
        background_fill_color='oldlace',
        sizing_mode=('fixed' if not gui else 'scale_width'),
    )

    fig.add_tools(HoverTool(tooltips=[
        ("x,y,name", "$x, $y, $name"),
    ]))

    # dashed grid
    fig.xgrid.grid_line_dash = [6, 4]
    fig.ygrid.grid_line_dash = [6, 4]

    minimum, maximum = None, None
    headers.remove(against)
    if against != 'Time':
        headers.remove('Time')

    for idx, val in enumerate(headers):
        color = Category10_10[idx % 10]
        fig.line(x=against, y=val,
                 line_width=2, color=color, source=cds, name=val)
        if against != 'Time':
            if (val not in ['Time', against]):
                fig.dash(x=cds.data[against], y=cds.data[val], angle=90.,
                         angle_units='deg', name=val, size=4, color=color)
        if to_show != ['{}']:
            if val not in to_show:
                fig.select(name=val).visible = False

            # find the min/max yvalue of the shown species
            else:
                if minimum is None:
                    minimum = min(cds.data[val])
                    maximum = max(cds.data[val])
                else:
                    minimum = min(minimum, min(cds.data[val]))
                    maximum = max(maximum, max(cds.data[val]))

    if to_show != ['{}']:
        if maximum == minimum:
            maximum, minimum = minimum + 1, minimum - 1
        # could look better if rounded to next tick
        pad = 0.1*(maximum - minimum)
        fig.y_range = Range1d(minimum - pad, maximum + pad)

    # handle the x/y_min/max option from plot.pl
    if xmin != '*': fig.x_range.start = xmin
    if xmax != '*': fig.x_range.end = xmax
    if ymin != '*': fig.y_range.start = ymin
    if ymax != '*': fig.y_range.end = ymax

    # items are a tuple of the form (label, renderer)
    full_items = [(header, fig.select(name=header)) for header in headers]
    nlegends = 1 + len(full_items) // 13
    for legend_index in range(nlegends):
        legend = Legend(
            items=full_items[legend_index*13:legend_index*13+13],
            location=(10, 0),
            click_policy='hide',
            border_line_color='gray',
        )
        fig.add_layout(legend, 'right')
        fig.plot_width += 100

    # small snippet to rerender axes when hiding species
    callback = CustomJS(args=dict(fig=fig, legend=fig.legend[0]), code="""
    var y_range = fig.y_range
    y_range.have_updated_interactively = false
    y_range.renderers = legend.items.map(item => item.renderers[0].visible && item.renderers[0])
    y_range.renderers.some(elem => elem) && Bokeh.index[fig.id].plot_canvas_view.update_dataranges()
    """
    )

    if against == 'Time':
        for item in fig.legend[0].items:
            item.renderers[0].js_on_change("visible", callback)

    return fig, cds


def handle_bokeh_plot(fig, gui_fig, against, cds=None, gui_cds=None, old_handle=None,
                      old_data=None, comm=None):
    if old_handle is None:
        layout = fig
        gui_layout = gui_fig

        if against != 'Time':
            button = Button(label="Animate")
            button.js_on_click(animation_callback(cds))
            gui_button = Button(label="Animate")
            gui_button.js_on_click(animation_callback(gui_cds))
            layout = column(fig, button)
            gui_layout = column(gui_fig, gui_button, sizing_mode="scale_width")

        if comm:
            send_plot_to_gui(gui_layout, comm)
            if comm.target_name == 'from_gui':
                return
        return show(layout, notebook_handle=True), cds

    # set the data of the old plot to what we just read
    old_data.data = dict(cds.data)
    # and display it
    push_notebook(handle=old_handle)
    return old_handle, old_data


def extract_image_filenames(output):
    '''return a pair with the list of image filenames found in the output,
    and the remainder'''
    output_lines = []
    image_filenames = []

    for line in output.splitlines():
        if line.startswith('<img src="'):
            filename = line.rstrip().split('"')[1]
            image_filenames.append(filename)
        else:
            output_lines.append(line)

    output = "\n".join(output_lines)
    return image_filenames, output

def send_plot_to_gui(fig, comm):
    script, div = components(fig)
    comm.send({
        'type': 'plot',
        'div': div,
        'script': script,
        'show': True
    })

def animation_callback(source):
    return CustomJS(args=dict(source=source), code="""
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    cb_obj.label = "Tracing"
    cb_obj.disabled = true
    var data = source.data

    async function update() {
        for (let i=0; i<data.Time.length; i++) {
            var ti = data.Time[i]
            var ti_ = data.Time[i-1] || 0
            var dt = ((ti - ti_) / data.Time[data.Time.length-1]) * 2000
            source.selected.indices = [...Array(i+1).keys()]
            await sleep(dt)
        }

        cb_obj.disabled = false
        cb_obj.label = "Animate"
    }
    update()
    """)
