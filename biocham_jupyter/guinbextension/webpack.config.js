const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: [
        './src/index.js'
        ],
    output: {
        filename: 'nbextension.js',
        path: path.resolve(__dirname, 'gui-build'),
        libraryTarget: 'amd'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            Kernel: path.resolve(__dirname, '../kernel/biocham_kernel'),
            Utils: path.resolve(__dirname, 'src/utils'),
            Config: path.resolve(__dirname, 'src/config'),
            Components: path.resolve(__dirname, 'src/components'),
            Constants: path.resolve(__dirname, 'src/constants'),
        }
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"'
        })
    ],
    devtool: 'cheap-module-source-map',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                use: [
                  { loader: "style-loader" },
                  { loader: "css-loader" }
                ]
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                use: {
                    loader: 'url-loader?limit=100000'
                }
            }
        ]
    }
};
