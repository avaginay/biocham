# The Biocham Jupyter GUI

## I- GUI architecture

### 1- Build process and libraries

All JavaScript libraries listed below and this project dependencies are installed using npm, the NodeJS package manager. They are all listed in the file [package.json](../package.json).

#### React and Redux
The Biocham GUI is developed primarily using the React and Redux JavaScript libraries. React is used for the view part of the app, whereas Redux is used for handling the app's state (see GUI events handling for detailed information). All components behaviour are described in their documentation, such as what they do when mounted .

#### Babel 
React code is written in [JSX](https://reactjs.org/docs/introducing-jsx.html), which is a syntax extension to Javascript. In order for the browser to correctly interpret it, there is a compilation step using [Babel](https://babeljs.io/), a Javascript compiler, useful for writing modern Javascript. Currently, the JavasSript language evolves faster than web browser's JavaScript engines and this kind of tool is necessary to palliate to this. 

This project Babel spec are written in a Babel file (`.babelrc`), which defines what babel plugins and presets (sets of babel plugins) are needed for compiling the JavaScript. The 2 presets used are [`present-env`](https://babeljs.io/docs/en/babel-preset-env) for writing JavaScript with its latest stable features and [`preset-react`](https://babeljs.io/docs/en/babel-preset-react) for writing JSX. The only other plugin used is for writing [JavaScript class properties](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties).

#### Webpack
[Webpack](https://survivejs.com/webpack/what-is-webpack/) is a module bundler that is used for building the GUI. It allows us to define an entry point (`src/index.js`, the main file for the GUI) and to use loaders for importing different types of files (e.g. `js/jsx`, `css`, ...), before bundling all the resources into a single file.


#### Production and development installation
The target `install_gui` in the `Makefile` is the one called for compiling and building the GUI for production. For development purposes, use the command `make install_gui_dev` in the biocham directory (essentially to have better logging in the JS console). The build commands are defined in the `package.json` npm file.

The Biocham GUI is shipped as a [Jupyter notebook extension](https://jupyter-notebook.readthedocs.io/en/stable/extending/frontend_extensions.html). It is specified that it needs to be defined as an [AMD module](https://en.wikipedia.org/wiki/Asynchronous_module_definition), which is specified in the Webpack config file (see `libraryTarget`).

### 2- Layout 

The GUI is composed of 3 different parts:

* The left part is the navigation menu containing the different biocham sections from the manual and the biocham model.
* The central part is the current section opened.
* The right part is used to show the different outputs (graphical outputs or textual outputs in a console).

### 3- Sections and how we structure the GUI

In this section, only the different steps and structure used to structure the GUI are described. The detailed description of how they work (i.e. kernel communication, state handling, ...) is described in another section.


#### a- The manual
The file `commands.js` contains all the Biocham manual structure (chapters, sections and commands) as an array. When building the GUI, this array is transformed in a tree in order for each chapter to contain its sections, and for each section to contain its commands. All the functions that uses this structure are stored in [sections.js](./module-sections.html). The config file that is cited on several occasions below is located at `config/config.js` and contains things like the model sections, chapters to ignore when building the GUI, or a mapping between biocham types and React widgets.

This tree is not used directly by the React app, there are several step that occurs before that:

* we extract the Biocham model sections from it (defined in the [config file](./config_config.js.html)).
* we merge and structure these model sections (cf. I.4)
* we remove some chapters that are useless in the GUI (cf. [config file](./config_config.js.html), `CHAPTERS_TO_IGNORE`)

So that in the end we get 2 structures: 

* The model sections (contained in the section `Model` of the GUI)
* The other sections, which are the remaining Biocham sections after the process described earlier

#### b- Section structure
Each Biocham chapter in the GUI can contain several Biocham sections, which can contain several Biocham commands. All the React components used to describe these are contained in the `components` directory, with file names pretty self-explanatory.

#### c- Biocham commands generation
Biocham commands have typed arguments/options (their structure is basically the same in React, more details below). In the config file, a few biocham types are mapped to specific input/widget types (see `BIOCHAM_TYPED_WIDGETS`); otherwise if not defined there, the input type will be a default input (i.e. textual). 

In the GUI, a command has the following structure:
* the command name is a button with a tooltip containing the command documentation
* the command arguments is an array which is used to generated inputs based on their type
* the command options works the same as the arguments, except that they are automatically filled with global option values (cf. section options)

To execute a command, simply click the button containing the commands name or hit `Ret` in one of the argument/option input.

### 4- Biocham model section
#### a - Structure
The GUI Biocham model spec is described in [config.js](./config_config.js.html) (see `CRUD_SECTIONS` there). It is composed of a few sections of the manual which regroups:
* Molecules and initial state
* Parameters
* Functions
* Reaction editor
* Influence editor
* Events
* Conservation law and invariants

Each of these sections is composed of the following:
* 1 CRUD table (which has at least a create/update and read functions, spec described below)
* 0 or more display tables (the remaining tables display content using the `list_` functions that are not defined in the CRUD tables spec, e.g. `list_locations` in the `Molecules and initial state` section)
* 0 or more biocham functions contained in the section and that are not used in above tables

The CRUD tables are composed of the following:
* a read function (e.g. `list_parameters`) which is used to populate the table's content
* a create/update function used to create new biocham objects, typically displayed in the last table's row (its display is the same as other biocham functions in the GUI, only change is that it is embedded in the table)
* a delete function* (not mandatory for a table), which is represented as a cross at the end of a row
* a command separator* (not mandatory either, this is only used for parsing purposes, i.e. to separate a biocham object's name from its value, e.g. parameters have '=' and initial state have ',')

#### b - Model updates

The whole model is updated everytime a biocham function is executed: every time a function is executed (cf. Kernel communication section), the tables receives the updated execution count, and if this one is different than the previous one (cf. React components lifecycle section for more information on this mechanism), the tables update by executing their 'read' command (cf. I.2 to see why the execution count isn't updated in this case).

The outputs are parsed upon reception in the GUI (methods located in [formats.js](./module-formats.html)) before populating the tables.

### 5- Outputs 

The way outputs are handled by the GUI is defined directly in the `App` component. When the app is mounted (see React component lifecycle), a comm handler is registered (cf. Kernel communication), and define how messages received from the kernel should be treated. We can define them in 2 separate categories: graphical and textual outputs.

#### a- Graphical outputs
The graphical outputs received are stored in the Redux store in an array, and can be of 3 different types:
* Bokeh plots ([`Plot`](./Plot.html) component)
* Biocham graphs (images)
* Latex ([`LatexDiv`](./LatexDiv.html) component)

The different outputs are shown in a Tab list at the top of the tab. The app only displays the current output selected (in the [`GraphicalTab`](./GraphicalTab.html) component), and based on its type it will render things differently:
* Bokeh plots are rendered using the [Range](https://developer.mozilla.org/fr/docs/Web/API/Range) API, due to the fact that the code received from the kernel is HTML and JS, and needs to be interpreted on the fly when rendered.
* Biocham graphs are rendered using `<img>` HTML tags.
* Latex code (only used to rendered ODEs) is rendered using MathJax.

#### b- Textual outputs 

Textual outputs (or string outputs) received are also stored in the Redux store in an array. They are rendered in the [`Console`](./Console.html) component.


### 6- Biocham workflows

#### a- Default workflows
There is also a mechanism implemented in the GUI called Biocham workflows. A workflow is a set of biocham commands that the user could preferably use, and is displayed as a section in the GUI. There are a few predetermined workflows (located in `config/workflows/workflows.js`) that are automatically generated from biocham files (in `config/workflows`). The file name will be the workflow name in the GUI.

#### b- Custom workflows

The user can also create his own workflows, which will be saved in the notebook's metadata upon saving. When clicking on `Create workflow`, a modal will open asking the user to name the workflow, then another modal will appear with all the biocham commands and their doc, so the user could append the ones he wants in the workflow.

The user can also modify or delete workflows if he only wants to keep some of them.

### 7- Settings

There is a settings section that currently contains:
* a checkbox to enable/disable automatic numerical simulations when changing parameters/initial state value (if checked, the commands `numerical_simulation. plot.` will be executed after the change of value).
* the command `option.`, with a list of all the different biocham global options.

## II- Kernel communication


The Jupyter notebook relies on a mechanism called 'comms' to communicate between the kernel and the front-end. Typically, when executing code in a notebook cell, a comm is created, which sends the code to execute to the kernel, which will return the output and some more data (e.g. execution count, ...).

In the Biocham GUI, we rely on this mechanism in 2 different ways

### 1- Using notebook cells

The one that is mostly used in the GUI is when executing a biocham function, a cell is created in the notebook and then executed. A mechanism defined directly in the kernel file (i.e. kernel.py) sends the result to the GUI before returning it to the notebook. 

### 2- Creating comm in the front-end

The second one is only used for updating the Biocham model in the GUI: instead of using the notebook's cells to execute code, comms are directly created in the front-end, and code is sent to the kernel directly through them with a 'silent' flag (set to true). If the kernel detects that the code received needs to be silent, the execution count won't update and the output won't be displayed in the GUI console. This is useful only to avoid displaying unwanted outputs in the GUI/notebook, such as the different model sections which are contained in tables and are displayed at all time for the user.

The file [`comms.js`](./module-comms.html) contains all the generic method to create cells, comms, and some methods that use biocham code (to update model and options). 


## III- GUI events handling

### 1- How Redux handles most of the kernel and GUI events 
Upon creation, the React app creates a Redux store which is used to store some parts of the app's state. 

The way Redux works in this app is the following: several reducer functions are defined in the `src/reducers` directory then combined and given to the store. A reducer is a function that receives the current state of the app and an action (a JS object used to update the state), and returns the updated app's state based on the content of the action. In several components, upon some user interaction (e.g. tab changes, graphical output deletions, ...), actions are dispatched and sent to the store, which will update the app's state. 

Before each component using the Redux store, there are 2 functions:
* `mapStateToProps` which maps the redux store state to the component's props.
* `mapDispatchToProps` which defines which type of action to send to the redux store, with according data (handled then in the reducers functions).

After being created, each part of the redux store state are populated with initial values (see reducers files).

### 2- What happens when saving the notebook in the GUI

When saving in the GUI (i.e. `File -> Save and Checkpoint`), several things are stored in the file's metadata (i.e. the ipynb file):
- the GUI current state (i.e. current graphical and textual outputs, custom workflows), so that the user will see the history of his last session
- the current Biocham model (see section Model of the documentation)
- the current Biocham global options 

## IV- Notebook modifications
### 1- Disabling the autosave 

The Jupyter notebook has a system of automatic saving, which can be confusing for the user.

As such, a small checkbox in the toolbar of the notebook offers the user to disable such mechanism. This choice is stored in the notebook's metadata, and the notebook needs to be saved in order to store it properly.

When saving the notebook, a message will appear on top of the notebook's page (e.g. "Last Checkpoint: a few seconds ago (autosaved)"). The 'autosaved' at the end of the message can be ignored if it has previously been disabled, as Jupyter automatically adds it to the timestamp saving string.

The disabling autosave mechanism is implemented in [`index.html`]('./module-GUI_main.html') (check `createDisableautosave`).




