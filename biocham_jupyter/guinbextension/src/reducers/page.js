import { biochamSections } from 'Utils/sections';
import { dashingLowercase } from 'Utils/utils';
import { TOGGLE_MENU_SECTION,
         CHANGE_TAB } from '../constants/actionTypes';
import { getMetadata } from 'Utils/utils';

const initialState = {
    navMenu: getMetadata('page', 'navMenu', {
        model: true,
    }),
    mainApp: getMetadata('page', 'mainApp', {
        model: true,
    }),
    mainTab: getMetadata('page', 'mainTab', 0),
    graphicalTab: getMetadata('page', 'graphicalTab', 0)
};

export default (state = initialState, action) => {
    switch (action.type) {
    case TOGGLE_MENU_SECTION:
        const isOpen = state.navMenu[action.name];
        return {
            ...state,
            // currently only opened section is stored, but
            // it should be a bit simpler (not urgent)
            navMenu: {
                [action.name]: !isOpen
            },
            mainApp: {
                [action.name]: true
            }
        };
    case CHANGE_TAB:
        return {
            ...state,
            [action.tabType]: action.index
        };
    default:
        return state;
    }
}
