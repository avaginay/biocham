/**
 * Main GUI file. 
 * @module main
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './store';
import App from './App';
import { getJupyter } from 'Utils/comms';
import { hideElement, showElement, isVisible } from 'Utils/utils';
import './styles/styles.css';

const Jupyter = getJupyter();

/**
 * Create the switch button (for the GUI) in the Jupyter notebook
 * header. 
 */
function createSwitchButton () {
    // just to init for the actual switch to detect the main toolbar toggle status
    const toolbar = document.getElementById('maintoolbar');
    if (!toolbar.style.display || toolbar.style.display === 'none') {
        toolbar.style.display = 'block';
    }

    const header = document.getElementById('header');
    // small divider on bottom of header
    const divider = document.createElement('div');
    divider.classList.add('bottom-header-bar');
    header.appendChild(divider);

    let toolbarButton = document.createElement('button');
    toolbarButton.id = 'switch_btn';
    toolbarButton.classList.add('btn', 'btn-xs', 'navbar-btn', 'notification_widget');
    toolbarButton.innerText = 'GUI';
    let helpMenu = document.getElementById('help_menu').parentElement;
    helpMenu.parentElement.insertBefore(toolbarButton, helpMenu);
    toolbarButton.onclick = switchView;
}

/**
 * Switch the view between the notebook and the GUI. We don't 
 * use `display=none` for the GUI because of the way bokeh plots
 * are handled (the sizing_mode options of the plot behave weirdly
 * when display is none). The Jupyter keyboard manager is also
 * disabled in the GUI as it forbid the user to use the keyboard
 * normally.
 */
export function switchView () {
    const notebookPanel = document.getElementById('ipython-main-app');
    const guiPanel = document.getElementById('gui_panel');
    const btn = document.getElementById('switch_btn');
    const header = document.getElementById('header');
    const toolbar = document.getElementById('maintoolbar');
    const toolbarHeight = toolbar.offsetHeight;
    const body = document.getElementsByTagName('body')[0];
    const nbpApp = document.getElementsByClassName('nbp-app');
        
    if (!isVisible(notebookPanel)) {
        Jupyter.notebook.metadata.biocham_gui = false;
        Jupyter.keyboard_manager.enable();

        // css handling, see styles.css for more info
        notebookPanel.classList.remove('hidden-notebook');
        notebookPanel.classList.add('visible-notebook');
        guiPanel.style.display = 'none';
        btn.innerText = 'GUI';
        toolbar.style.display = 'block';
        header.classList.remove('gui_header');
        body.classList.add('notebook_app');
    }
    else if (isVisible(notebookPanel)) {
        Jupyter.notebook.metadata.biocham_gui = true;
        // Need to disable the notebook shortcuts when in the GUI
        Jupyter.keyboard_manager.disable();

        // css handling 
        notebookPanel.classList.remove('visible-notebook');
        notebookPanel.classList.add('hidden-notebook');
        guiPanel.style.display = 'block';
        btn.innerText = 'Notebook';
        toolbar.style.display = 'none';
        header.classList.add('gui_header');
        body.classList.remove('notebook_app');
    }

    // adjust container height
    const $ = window.$;
    $('div#site').height($(window).height() - $('#header').height());
}

/**
 * Create autosave disabling checkbox.
 * @param {boolean} init - The boolean autosave in the notebook's metadata
 */
function createDisableAutosave(init) {
    const div = document.createElement('div');
    const checkbox = document.createElement('input');
    const label = document.createElement('label');
    
    checkbox.type = 'checkbox';
    checkbox.id = 'disable-autosave';
    checkbox.name = 'disable-autosave';
    checkbox.checked = init == undefined ? true: init? true: false;
    if (!init && init != undefined) {
        console.log('Autosave disabled');
        Jupyter.notebook.set_autosave_interval(0);
        Jupyter.notebook.events.trigger('autosave_disabled.Notebook');
    };
    label.for = 'disable-autosave';
    label.innerText = 'Autosave';

    div.classList.add('btn-group');
    const toolbar = document.getElementById('maintoolbar-container');
    div.appendChild(checkbox);
    div.appendChild(label);
    toolbar.appendChild(div);
    checkbox.onclick = switchAutosave;
}

/**
 * Handle autosave enabling/disabling. 
 */
function switchAutosave() {
    const checkbox = document.getElementById('disable-autosave');
    if (checkbox.checked) {
        console.log('Enabling autosave');
        Jupyter.notebook.metadata.autosave = true;
        Jupyter.notebook.set_autosave_interval(120000);
    }
    else {
        console.log('Disabling autosave');
        Jupyter.notebook.metadata.autosave = false;
        Jupyter.notebook.set_autosave_interval(0);
    }
}

/** 
 * Create container for the GUI, create switch button.
 */
function initGUI() {
    const guiPanel = document.createElement('div');
    guiPanel.id = 'gui_panel';
    document.getElementById('site').appendChild(guiPanel);
    createSwitchButton();
    createDisableAutosave(Jupyter.notebook.metadata.autosave);
}

/**
 * Init the GUI container then render the GUI inside it with ReactDOM.
 */
function loadGUI(kernelName) {
    if (kernelName === 'biocham') {
        if (!document.getElementById('gui_panel')) {
            initGUI();
            const root = document.getElementById('gui_panel');
            ReactDOM.render(
                <Provider store={store}>
                  <App Jupyter={Jupyter}/>
                </Provider>,
                root
            );
        }
    }
}

/**
 * Jupyter extension loading, only loads the GUI if Jupyter uses biocham 
 * kernel.
 */
export function load_ipython_extension() {
    // ensure that we are currently using a kernel biocham, otherwise
    // the GUI is not loaded.
    try {
        loadGUI(Jupyter.notebook.kernel.name);
        if (Jupyter.notebook.metadata.biocham_gui) {
            document.getElementById('switch_btn').click();
        }
    } catch(e) {
        Jupyter.notebook.events.on('kernel_ready.Kernel', function () {
            loadGUI(Jupyter.notebook.kernel.name);
            if (Jupyter.notebook.metadata.biocham_gui) {
                document.getElementById('switch_btn').click();
            }
        });
    }
}

