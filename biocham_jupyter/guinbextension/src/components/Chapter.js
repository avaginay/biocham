import React, { Component, Fragment } from 'react';
import Divider from '@material-ui/core/Divider';
import Section from './Section';
import Command from './Command';


/**
 * Container for biocham chapter sections.
 */
class Chapter extends Component {
    
    render() {
        const { classes, content, options,
                handleKernelResponse, appendResults } = this.props;
        
        return (
            <section>
              { content.map((section, index) => {
                  const commands = section.children;
                  return (
                      commands.length &&
                          <Section key={index}
                                   title={section.name}
                                   options={options}
                                   handleKernelResponse={handleKernelResponse}
                                   appendResults={appendResults}>
                            { commands.map((command, index) => {
                                return (
                                    <Fragment key={`${command.name}-${index}`}>
                                      <Command command={command}/>
                                      { commands.length !== index + 1 &&
                                        <Divider/>
                                      }
                                    </Fragment>
                                );
                            })
                            }
                          </Section>
                  );
              })
              }
            </section>
        );
    }
}

export default Chapter;
