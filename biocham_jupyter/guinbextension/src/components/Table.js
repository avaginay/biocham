import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Add from '@material-ui/icons/Add';
import Clear from '@material-ui/icons/Clear';
import ErrorBoundary from './ErrorBoundary';
import Command from './Command';
import { NumberInput, SliderInput, TextInput } from './inputs';
import { createComm, addToNotebook } from 'Utils/comms';
import { cleanEmpty } from 'Utils/sections';
import { placeHolder, step, precision, round } from 'Utils/utils';
import { format } from 'Utils/formats';
import { commTarget } from 'Constants/gui';


const headerStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    row: {
        borderBottom: '1px solid rgba(224, 224, 224, 1)'
    },
    headerCell: {
        fontSize: '13px'
    },
    tableName: {
        width: '38%'
    },
    tableValue: {
        width: '62%'
    }
});


/** Table header component. */
let TableHeader = props => {
    const { classes, split, title } = props;
    // format the title
    // e.g. : list_initial_state -> Initial State
    const cleanTitle = title.replace('list', '')
          .replace(/_/g, ' ')
          .split(' ')
          .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
          .join(' ');
    
    return (
        <TableHead className={classes.root}>
          <TableRow className={classes.row}>
            <TableCell className={split? classes.tableName: ''}>
              {cleanTitle}
            </TableCell>
            { split && 
              <TableCell className={classes.tableValue}>
                Value
              </TableCell>
            }
          </TableRow>
        </TableHead>
    );
};

TableHeader = withStyles(headerStyles)(TableHeader);


// ------------------------------------------------- //


const rowStyles = {
    root: {
        borderBottom: '1px solid rgba(224, 224, 224, 1)'
    },
    tableInputDiv: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    input: {
        width: '100%',
        marginLeft: '3px',
        marginRight: '3px'
    },
    delete: {
        height: '20px'
    },
    button: {
        backgroundColor: 'lightgrey',
        fontSize: '11px',
        marginLeft: '10px'
    },
    cell: {
        // don't know why there is a 1px shift between cells
        // so overwriting this here and putting it at row level
        borderBottom: 'none'
    },
    valueCell: {
        borderBottom: 'none',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
};

/** Table row component. If output is of the form {name, value}, the
 * row will contain a slider for value modifications.
 */
class BTableRow extends Component {

    /**
     * Handle object delete by executing biocham function.
     */
    handleDelete = () => {
        const { content, output } = this.props;
        const code = `${content.del.name}(${output.name}).`;
        addToNotebook(code);
    }

    render() {
        const { classes, output, content } = this.props;
        
        if (typeof output === 'object') {
            return (
                <TableRow className={classes.root}>
                  <TableCell className={classes.cell}>
                    {output.name}
                  </TableCell>
                  <TableCell className={classes.valueCell}>
                    <ErrorBoundary>
                    <SliderInput value={output.value}
                                 name={output.name}
                                 crud={content}
                                 handleChange={this.props.handleChange}
                                 min={output.min}
                                 max={output.max}
                                 step={output.step}
                                 precision={output.precision}/>
                    </ErrorBoundary>
                    { content.del &&
                      <IconButton onClick={this.handleDelete}>
                        <Clear className={classes.delete}/>
                      </IconButton>}
                  </TableCell>
                </TableRow>
            );
        } else {
            return (
                <TableRow key={`item-${output}`}>
                    <TableCell>
                      {output}
                    </TableCell>
                </TableRow>
            );
        }
    }
}

BTableRow = withStyles(rowStyles)(BTableRow);


// ------------------------------------------------- //


const styles = theme => ({
    root: {
        margin: '15px',
        overflowX: 'auto',
    },
    row: {
      height: '38px'
    },
    emptyTable: {
        paddingLeft: '24px',
        paddingBottom: '8px'
    },
});


const mapStateToProps = state => {
    return {
        executionCount: state.settings.executionCount
    };
};

/** Biocham model table component. */
class BTable extends Component {

    state = {
        output: []
    }
    
    /** On mount, updates table content. */
    componentDidMount() {
        this.update();
    }

    /** After every command execution that update the execution 
     * count, updates table content.
     */
    componentDidUpdate(prevProps, prevState) {
        const { executionCount } = this.props;

        if (prevProps.executionCount !== executionCount) {
            this.update();
        }
    }

    /**
     * Execute table read function and updates table state with result.
     * A boolean `silent` is sent to the kernel to tell it not to update
     * execution count.
     * Either table state will be an array of objects
     * (i.e. [{name: name, value: value}, ...]) or an array of values
     * (i.e. [value1, value2, ...]).
     */
    update = () => {
        const { content } = this.props;
        //if it's a crud table with add and delete the object structure is different 
        const cmd = content.name || content.read.name;
        const comm = createComm(commTarget);
        comm.send({code: `${cmd}.`, silent: true});
        comm.on_msg(msg => {
            let output = format({str: msg.content.data.output,
                                 type: cmd});
            const currentOutputs = this.state.output;
            
            if (output.length === 0) {
                this.setState({ output: '' });
            }
            else if (typeof output[0] === 'object'){
                if (currentOutputs === '') {
                    this.setOutputWithProps(output);
                }
                else {
                    this.setState({ output: output.map(newOutput => {
                        const curr = currentOutputs.find(out =>
                                                         out.name === newOutput.name);
                        return curr ?
                            this.updateProps(curr, newOutput) :
                            this.setProps(newOutput);
                    })});
                }
            }
            else {
                this.setState({ output });
            }
        });
    }

    /**
     * Apply setProps to each object in array, then set table state with it.
     * @param {Array} arr - array of outputs.
     */
    setOutputWithProps = (arr) => {
        const proppedOutputs = arr.map(out => {
            return this.setProps(out);
        });
        this.setState({output: proppedOutputs});
    }

    /** Set props of output object (used for sliders). Typically if an output 
     * has a value (i.e. initial state or parameters), it will:
     * * compute the sliders precision.
     * * compute the sliders step.
     * * computes the sliders min and max.
     * This is defined at the table level because otherwise the table's rows will
     * not always update in the GUI, and values would be inconsistent in the model
     * and in the table.
     */
    setProps = (obj) => {
        if (obj.value || obj.value === 0) {
            obj.precision = precision(obj.value);
            obj.step = step(obj.value);
            if (obj.value >= 0) {
                obj.min = 0;
                obj.max = round(obj.value*10, obj.precision) || 10;
            }else {
                obj.min = round(obj.value*10, obj.precision) || -10;
                obj.max = 0;
            }
        }
        return obj;
    }

    /**
     * Update output props (for sliders). Cases are:
     * * if former value was > 0 and new value is 0, divide max by 10
     * and update props.
     * * if new value is >= to max value, update props.
     * @param {Object} prevObj - the previous output object.
     * @param {Object} obj - the current output object.
     * @return {Object} the current output object with updated props.
     */
    updateProps = (prevObj, obj) => {
        const _precision = precision(obj.value);
        const shouldUpdate = _precision > prevObj.precision;

        if (prevObj.value > 0 && obj.value === 0) {
            const newPrecision = prevObj.precision + 1;
            const newStep = step(prevObj.value/10);
            return {...prevObj,
                    value: obj.value,
                    precision: newPrecision,
                    step: newStep,
                    max: prevObj.max/10};
        }

        return obj.value >= prevObj.max || obj.value <= prevObj.min || shouldUpdate ?
            this.setProps(obj) : {...prevObj, value: obj.value};
    }

    /** Generate table rows. */
    generateTableBody = (arr) => {
        const { content } = this.props;
        const { output } = this.state;

        return (
            output.map((item, index) => {
                return (
                    <BTableRow key={`table-row${index}`}
                               output={item}
                               content={content}
                               handleChange={this.update}/>
                );
            })
        );
    }

    render() {
        const { classes, content } = this.props;
        const { read, create, del, commandSep, placeholder } = content;
        const { output, rowsPerPage, page } = this.state;
        const len = output.length;
        const title = content.name || content.read.name;
        // check if the string should be split as name - value
        const split = !!commandSep;
        const sep = commandSep ? commandSep : '';
        
        return (
            <Paper className={classes.root}>
              <Table className={classes.table}>
                <TableHeader split={split}
                             title={title}/>
                <TableBody>
                  { output ?
                    this.generateTableBody(output) :
                    <TableRow>
                      <TableCell colSpan={split? 2: 1}>
                        No Entry
                      </TableCell>
                    </TableRow>
                  }
                </TableBody>
              </Table>
              { create &&
                <Command command={create} tableCmd={true}/>
              }
            </Paper>
        );
    }
}

export default connect(mapStateToProps)(
    withStyles(styles)(BTable)
);







