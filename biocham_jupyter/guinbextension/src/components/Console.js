import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import { CLEAR_CONSOLE } from 'Constants/actionTypes';
import { addToNotebook } from 'Utils/comms';
import { listCommands, getNames } from 'Utils/sections';
import { existingCompletions,
         longestCommonStartingSubstring } from 'Utils/completer';
// import Jupyter utils this way we don't get an import warning
// for using global Jupyter
const JupyterUtils = requirejs('base/js/utils');

const styles = theme => ({
    root: {
        display: 'flex',
        position: 'absolute',
        whiteSpace: 'pre-line',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        width: '100%',
        height: '100%',
        padding: '10px',
    },
    outputs: {
        top: '20px',
        width: '100%',
        padding: '5px',
        overflowY: 'auto',
    },
    input: {
        paddingTop: '10px'
    },
    output:{
        wordWrap: 'break-word'
    },
    clearButton: {
        top: '10px',
        bottom: '10px'
    }
});

const mapStateToProps = state => {
    return {
        consoleOutputs: state.outputs.consoleOutputs
    };
};

const mapDispatchToProps = dispatch => ({
    onClearConsole: () => dispatch({ type: CLEAR_CONSOLE })
});

/** Console for biocham code execution and outputs. */
class Console extends Component {

    state = {
        value: '',
        current: 0,
    }

    /** On mount, init current output to outputs (array) length, in order
     * to simulate a console behaviour for navigating forward/backward through 
     * commands history.
     */ 
    componentDidMount() {
        this.setState({ current: this.props.consoleOutputs.length });
    }

    /** Scroll to bottom of outputs container upon update. */
    componentDidUpdate(prevProps) {
        if (prevProps.consoleOutputs.length <
            this.props.consoleOutputs.length) {
            const elem = document.getElementById('bc-outputs');
            elem.scrollTop = elem.scrollHeight;
            this.setState(
                {
                    value: '',
                    current: this.props.consoleOutputs.length
                }
            );
        }
    }

    /** Handle input change. */
    handleChange = (e) => {
        this.setState({ value: e.target.value });
    }

    /** 
     * Handle keyboard events: 
     * * Execute code if `Ret`.
     * * Navigate through commands history if up/down arrows.
     * * Completion for last command in input with `Tab`.
     */
    handleKeyUp = (e) => {
        const { consoleOutputs } = this.props;
        const { value, current, storedValue } = this.state;

        // RET
        if (e.which === 13) {
            e.preventDefault();
            // if enter
            const code = value;
            addToNotebook(code);
            this.setState(
                {
                    value: '',
                    current: consoleOutputs.length + 1
                }
            );
        }
        // UP/DOWN arrows
        else if (e.which === 38 || e.which === 40) {
            e.preventDefault();

            // direction is -1/+1 for forward/backward through command history
            const direction = e.which - 39;
            let newIndex = current + direction;
            if (newIndex < 0) newIndex = 0;
            else if (newIndex >= consoleOutputs.length) {
                newIndex = consoleOutputs.length;
            }
            this.setState(
                {
                    value: consoleOutputs[newIndex] ?
                        consoleOutputs[newIndex].code: '',
                    current: newIndex
                }
            );
        }
        // TAB
        else if (e.which === 9) {
            e.preventDefault();
            const { value } = this.state;
            const inputCommands = value.match(/([a-z_]*?)\.|[a-z_]*?\((.*?)\)\./g);
            let lastCommand = value;
            if (inputCommands && inputCommands.length) {
                lastCommand = value.split(' ').pop();
            }
            const commandNames = getNames(listCommands);
            const completions = existingCompletions(lastCommand, commandNames);
            if (completions.length) {
                const complete = longestCommonStartingSubstring(completions);
                if (complete) {
                    let newVal = value.split(' ');
                    newVal[newVal.length - 1] = complete;
                    this.setState({ value: newVal.join(' ') });
                }
            }
        }
    }
    
    render() {
        const { classes, consoleOutputs } = this.props;
        const { value } = this.state;
        return (
            <div className={classes.root}>
              <button className={classes.clearButton}
                      onClick={this.props.onClearConsole}>
                Clear console
              </button>
              <section id="bc-outputs"
                   className={classes.outputs}>
                { consoleOutputs.map((o, index) => {
                    if (o.output && o.output !== '') {
                        // in exceptional cases, an img tag is returned,
                        // which we don't want to be in the console.
                        let output = o.output;
                        if (output.includes('<img')) {
                            output = output.replace(new RegExp(/\<(.*?)\>/g), '').trim();
                        }
                        return (
                            <p key={`output-${index}`}>
                              <p>{`> ${o.code}`}</p>
                              {/*we use dangerouslySetInnerHTML because in case of error rendering
                                Jupyter returns an HTML tag.*/}
                              <p className={classes.output}
                                 dangerouslySetInnerHTML={{
                                     __html: JupyterUtils._ansispan(output)
                                 }}/>
                            </p>
                        );
                    }
                    return <p key={`output-${index}`}>{`> ${o.code}`}</p>;
                })
                }
              </section>
              <InputBase value={value}
                         type="text"
                         onChange={this.handleChange}
                         onKeyDown={this.handleKeyUp}
                         fullWidth
                         autoFocus
                         startAdornment={
                             <InputAdornment position="start">
                               {'>'}
                             </InputAdornment>
                         }
              />
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(Console)
);
