import React, { Component, createRef } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { getMathJax } from 'Utils/comms';

const styles = ({
    root: {
        overflowY: 'auto',
        position: 'absolute',
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const MathJax = getMathJax();

/** Latex container (pretty printing of ODEs with MathJax). The HTML node created uses the 
 * same mechanism as plots (i.e. React refs).
 */
class LatexDiv extends Component {

    ref = createRef();

    /** On mount, calls handleTex. */
    componentDidMount() {
        this.handleTex();
    }

    /** Calls MathJax to correctly interpret latex code. */
    handleTex = () => {
        // see https://docs.mathjax.org/en/v1.0/typeset.html for typesetting
        MathJax.Hub.Queue(["Typeset",MathJax.Hub, this.ref.current]);
    }

    render() {
        const { classes, latex } = this.props;

        return (
            <div className={classes.root} ref={this.ref}>
              {latex.image_data}
            </div>
        );
    }
}

export default withStyles(styles)(LatexDiv);
