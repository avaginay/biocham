import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import { modelSectionsWithCruds } from 'Utils/sections';
import { createComm } from "Utils/comms";
import Section from './Section';
import Workflows from './Workflows';
import Command from './Command';
import BTable from './Table';
import { dashingLowercase } from 'Utils/utils';


/** Biocham model container. It contains a Workflows component and all the model
 * sections. 
 */
class Model extends Component {

    /** Generate section table outputs. */
    generateOutputs = (section) => {
        const { display } = section;
        let listCommands = [];

        display.crud.read && listCommands.push(display.crud);
        if (display.lists) {
            display.lists.map(cmd => listCommands.push(cmd));
        }
        
        return (
            listCommands.map((table, index) => {
                return (
                    <BTable key={`table-${index}`}
                            content={table}/>
                );
            })
        );
    }
    
    render () {
        const { classes } = this.props;
        const sectionsLen = modelSectionsWithCruds.length;

        return (
            <div>
              <Workflows/>
              { modelSectionsWithCruds.map((section, index) => {
                  const commands = section.children;
                  return (
                      <Section key={`model-section-${index}`}
                               title={section.name}>
                        { this.generateOutputs(section) }
                        { commands.map((command, index) => {
                            return (
                                <div key={`${command.name}-${index}`}>
                                  <Command command={command}/>
                                  { commands.length !== index + 1 &&
                                    <Divider/>
                                  }
                                </div>
                            );
                        })
                        }
                      </Section>
                  );
              })}
            </div>
        );
    }
}

export default Model;
