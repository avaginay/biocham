import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Tab, Tabs } from '@material-ui/core';
import { CHANGE_TAB } from 'Constants/actionTypes';
import GraphicalTab from './GraphicalTab';
import Console from './Console';

const styles = theme => ({
    root: {
        display: 'flex',
        alignContent: 'stretch',
        height: '100%',
        flexDirection: 'column',
    },
    tab: {
        flex: 1,
        position: 'relative',
    },
    tabHeader: {
        zIndex: 1,
        boxShadow: 'none',
        borderBottom: '1px solid #e7e7e7'
    },
    hidden: {
        display: 'none'
    }
});

/** Simple tab container. */
function TabContainer(props) {
    return (
        <div className={props.className}>
          {props.children}
        </div>
    );
}

const mapStateToProps = state => {
    return {
        mainTab: state.page.mainTab
    };
};

const mapDispatchToProps = dispatch => ({
    onChangeTab: (tabType, index) =>
        dispatch({ type: CHANGE_TAB, tabType, index })
});

/** Global container for all outputs. */
class OutputTabs extends Component {

    /** Handle main tab change (graphical outputs/console). */
    handleTabChange = (event, index) => {
        this.props.onChangeTab('mainTab', index);
    }

    render() {
        const { classes, handleTabChange, mainTab} = this.props;
        
        return (
            <div className={classes.root}>
              <AppBar position="static" color="default" className={classes.tabHeader}>
                <Tabs  value={mainTab}
                       onChange={this.handleTabChange}
                       indicatorColor="primary"
                       textcolor="primary"
                       variant="centered">
                  <Tab label="Graphical Outputs"/>
                  <Tab label="Console"/>
                </Tabs>
              </AppBar>
              <TabContainer className={mainTab !== 0 ?
                                       classes.hidden : classes.tab}>
                <GraphicalTab />
              </TabContainer>
              <TabContainer className={mainTab !== 1 ?
                                       classes.hidden : classes.tab}>
                <Console />
               </TabContainer>
            </div>
        );
    }
}

OutputTabs = connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(OutputTabs)
);

export { OutputTabs, TabContainer };
