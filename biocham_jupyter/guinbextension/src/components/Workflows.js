import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Modal from '@material-ui/core/Modal';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Command from './Command';
import WorkflowEditor from './WorkflowEditor';
import Section from './Section';
import {
    CHANGE_WORKFLOW,
    CREATE_WORKFLOW,
    DELETE_WORKFLOW,
    ADD_COMMAND_TO_WORKFLOW,
    REMOVE_COMMAND_FROM_WORKFLOW } from 'Constants/actionTypes';


const styles = theme => ({
    select: {
        margin: '15px',
    },
    button: {
        textTransform: 'none',
        margin: '2px'
    },
    createButton: {
        textTransform: 'none',
        margin: '15px'
    }
});

const mapStateToProps = state => {
    return {
        workflows: state.settings.workflows,
        workflowSelected: state.settings.workflowSelected,
    };
};

const mapDispatchToProps = dispatch => ({
    onChangeWorkflow: index =>
        dispatch({ type: CHANGE_WORKFLOW, index }),
    onCreateWorkflow: workflow =>
        dispatch({ type: CREATE_WORKFLOW, workflow }),
    onDeleteWorkflow: index =>
        dispatch({ type: DELETE_WORKFLOW, index }),
    onAddCommand: (wfname, command) =>
        dispatch({ type: ADD_COMMAND_TO_WORKFLOW, wfname, command }),
    onDeleteCommand: (wfname, index) =>
        dispatch({ type: REMOVE_COMMAND_FROM_WORKFLOW, wfname, index })
});

class Workflows extends Component {

    state = {
        modalOpen: false
    }

    /**
     * Handle workflow selection changes.
     */
    handleChange = (e) => {
        this.props.onChangeWorkflow(e.target.selectedIndex);
    }
    
    /** Handle workflow deletion. */
    handleDelete = () => {
        this.props.onDeleteWorkflow(this.props.workflowSelected);
    }

    /** Handle workflow editor modal opening. */
    handleOpen = () => {
        this.setState({ modalOpen: true });
    }

    /** Handle workflow editor modal closing. */
    handleClose = () => {
        this.setState({ modalOpen: false });
    }

    /** Prompt user for new workflow name, then creates empty workflow and 
     * open workflow editor modal. 
     */
    createWorkflow = () => {
        const name = prompt("Enter a workflow name");
        if (name != null && name !== '') {
            this.props.onCreateWorkflow({name, commands: []});
            this.props.onChangeWorkflow(this.props.workflows.length);
            this.handleOpen();
        }
    }
    
    render() {
        const { classes, options, workflows, workflowSelected } = this.props;

        const wf = workflows[workflowSelected];
        
        return(
            <Section id="workflows"
                     title="Workflows">
              { !workflows.length &&
                <Button className={classes.createButton}
                        onClick={this.createWorkflow}
                        variant="contained"
                        color="primary"
                        size="small">
                  Create workflow
                </Button>
              }
              { workflows.length ?
                <Fragment>
                  <div>
                    <select className={classes.select}
                            name="workflow-list"
                            value={wf.name}
                            onChange={this.handleChange}>
                      {
                          workflows.map(w => {
                              return (
                                  <option key={w.name.toLowerCase()}
                                          value={w.name}>{w.name}</option>
                              );
                          })
                      }
                    </select>
                    <Button className={classes.button}
                            onClick={this.createWorkflow}
                            variant="contained"
                            color="primary"
                            size="small">
                      Create workflow
                    </Button>
                    <Button className={classes.button}
                            onClick={this.handleOpen}
                            variant="contained"
                            color="primary"
                            size="small">
                      Edit
                    </Button>
                    <Button className={classes.button}
                            onClick={this.handleDelete}
                            variant="contained"
                            color="primary"
                            size="small"
                    >
                      Delete
                    </Button>
                  </div>
                  { wf.commands.map((command, index) => {
                      return (
                          <div key={`${command.name}-${index}`}
                               className={classes.command}>
                            <Command command={command}/>
                            { wf.commands.length !== index + 1 &&
                              <Divider/>
                            }
                          </div>
                      );
                  })
                  }
                </Fragment>
                : null
              }
              <Modal open={this.state.modalOpen}
                     onClose={this.handleClose}>
                <WorkflowEditor />
              </Modal>
            </Section>
        );
    } 
}

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(Workflows)
);
