import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Tab, Tabs } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Close from '@material-ui/icons/Close';
import { CHANGE_TAB,
         DELETE_GRAPHICAL_OUTPUT } from 'Constants/actionTypes';
import { TabContainer } from './OutputTabs';
import Plot from './Plot';
import LatexDiv from './LatexDiv';
import { dashingLowercase } from 'Utils/utils';

const appbarHeight = '30px';

const styles = ({
    root: {
        overflowY: 'auto',
        position: 'absolute',
        display: 'flex',
        justifyContent: 'stretch',
        width: '100%',
        height: '100%',
        flexDirection: 'column'
    },
    img: {
        margin: '20px',
        maxWidth: '100%',
        maxHeight: '100%'
    },
    header: {
        zIndex: 1,
        height: appbarHeight,
        minHeight: appbarHeight
    },
    tab: {
        display: 'flex',
        minHeight: appbarHeight,
        height: appbarHeight,
        minWidth: '20px',
        justifyContent: 'flex-start'
    },
    tabContainer: {
        flex: 1,
        position: 'relative'
    },
    hidden: {
        display: 'none'
    },
    icon: {
        height: '20px',
        width: '20px',
        padding: 0
    },
    label: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        textTransform: 'capitalize',
        height: appbarHeight,
        zIndex: 100
    }
});


const mapStateToProps = state => {
    return {
        graphicalOutputs: state.outputs.graphicalOutputs,
        graphicalTab: state.page.graphicalTab
    };
};

const mapDispatchToProps = dispatch => ({
    onGraphicalOutputDeleted: index =>
        dispatch({ type: DELETE_GRAPHICAL_OUTPUT, index }),
    onChangeTab: (tabType, index) =>
        dispatch({ type: CHANGE_TAB, tabType, index })
});


/** Graphical outputs (plots, graph images and latex pretty printing) container and handler. */
class GraphicalTab extends Component {

    /**
     * 
     */
    componentDidUpdate(prevProps) {
        const { graphicalOutputs, graphicalTab } = this.props;
        //TODO better tab handling
        if (prevProps.graphicalOutputs.length < graphicalOutputs.length) {
            this.props.onChangeTab('graphicalTab', graphicalOutputs.length);
        }
        else if (prevProps.graphicalOutputs.length > graphicalOutputs.length) {
            if (prevProps.graphicalTab >= graphicalOutputs.length) {
                const tab = graphicalOutputs.length > 0 ?
                      graphicalOutputs.length-1: 0;
                this.props.onChangeTab('graphicalTab', tab);
            }
            else {
                const tab = prevProps.graphicalTab === 0 ?
                      0: prevProps.graphicalTab - 1;
                this.props.onChangeTab('graphicalTab', tab);
            }
        }
    }

    /** Handle output selection change. */
    handleChange = (event, index) => {
        this.props.onChangeTab('graphicalTab', index);
    }

    /** Handle delete output event. */
    handleDelete = (event, index) => {
        this.props.onGraphicalOutputDeleted(index);
        event.stopPropagation();
    }

    /** Generate output based on type. */
    generateOutput = (obj) => {
        const { classes } = this.props;
        if (!obj) return null;
        if (obj.type === 'plot') {
            return (
                <Plot plot={obj}/>
            );
        }
        else if (obj.type === 'image') {
            if (obj.image_type === 'text/latex') {
                return (
                    <LatexDiv latex={obj}/>
                );
            } else {
                const source = `data:${obj.image_type};base64, ${obj.image_data}`;
                return (
                    <div>
                      <img src={source}
                           className={classes.img}/>
                    </div>
                );
            }
        }
        return null;
    }

    render() {
        const { classes, graphicalOutputs, current, graphicalTab } = this.props;
        const hasOutputs = graphicalOutputs.length;
             
        return (
            hasOutputs ?
            ( <div className={classes.root}>
                <AppBar position="static" color="default" className={classes.header}>
                  <Tabs className={classes.header}
                        value={graphicalTab}
                        onChange={this.handleChange}
                        textcolor="primary"
                        indicatorColor="primary"
                        variant="scrollable"
                        scrollButtons="on">
                    { graphicalOutputs.map((tab, index) =>
                                           <Tab key={`tab-${index}`}
                                                component='div'
                                                className={classes.tab}
                                                disableRipple
                                                label={<div className={classes.label}>
                                                         <div>{tab.type}</div>
                                                         <IconButton size='small'
                                                                     className={classes.icon}
                                                                     onClick={(e) => this.handleDelete(e, index)}>                                                  <Close className={classes.icon}/>
                                                         </IconButton>
                                                       </div>}/>)}
                  </Tabs>
                </AppBar>
                <TabContainer className={classes.tabContainer}>
                  {this.generateOutput(graphicalOutputs[graphicalTab])}
                </TabContainer>
              </div>) :
            null
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(GraphicalTab)
);
