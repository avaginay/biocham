import React, { Component, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Section from './Section';
import Command from './Command';
import { addToNotebook } from 'Utils/comms';
import { TOGGLE_NUMERICAL_SIMULATION,
         UPDATE_OPTIONS } from 'Constants/actionTypes';

const styles = theme => ({
    inputList: {
        display: 'flex',
        flexDirection: 'column'
    },
    checkbox: {
        paddingLeft: '15px'
    }
});

const mapStateToProps = (state) => {
    return {
        options: state.settings.options,
        numericalSimulation: state.settings.numericalSimulation
    };
};

const mapDispatchToProps = (dispatch) => ({
    onToggleNumSim: () =>
        dispatch({ type: TOGGLE_NUMERICAL_SIMULATION })
});


/**
 * Settings container. It contains all the global options list, and
 * a checkbox to enable/disable automatic numerical simulation after
 * a value change of a biocham object in the model (e.g. parameter, 
 * initial state).
 */
class Settings extends Component {
    
    render() {
        const { classes, options, numericalSimulation } = this.props;

        return (
            <Section title="Settings">
              <FormControlLabel className={classes.checkbox}
                                control=
                                  { <Checkbox checked={numericalSimulation}
                                              name="numsim"
                                              onChange={this.props.onToggleNumSim}
                                              value="numsim"/>
                                  }
                                label="Automatic numerical simulation"/>
              <Command command={{kind: 'command',
                                 name: 'option',
                                 arguments: [],
                                 doc:'',
                                 options: options}}/>
            </Section>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(Settings)
);
