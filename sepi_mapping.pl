:- module(
  sepi_mapping,
  [
    % Commands
    constraints/4
  ]
).

:- use_module(biocham).
:- use_module(sepi_infos).
:- use_module(sepi_util).


%% Mapping restriction
%% Optional, ignored when option(mapping_restriction: [])


%! constraints(+G1, +G2, +Mapping, +Stream)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% writes mapping constraints for mu
%
% @arg  G1      graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg  G2      graph 2 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg  Mapping list of vertex to mapp
% @arg  Stream  Stream for the CNF file
constraints(_, _, [], _).

constraints(G1, G2, [(Label1 -> deleted)|T], Stream) :-
  G1 = [N1, _, _, Id1],
  G2 = [N2, _, _, _],
  species(Label1, X, Id1),
  m_ij(X, N2, N2, Dummy),
  get_option(extremal_sepi, Extremal_sepi),
  (
    Extremal_sepi \= no
  ->
    Top is N1 + 1,
    format(Stream, "~d ~d 0~n", [Top, Dummy])
  ;
    format(Stream, "~d 0~n", [Dummy])
  ),
  constraints(G1, G2, T, Stream), !.

constraints(G1, G2, [(Label1 -> Label2)|T], Stream) :-
  G1 = [N1, _, _, Id1],
  G2 = [N2, _, _, Id2],
  species(Label1, X1, Id1),
  species(Label2, X2, Id2),
  m_ij(X1, X2, N2, X),
  get_option(extremal_sepi, Extremal_sepi),
  (
    Extremal_sepi \= no
  ->
    Top is N1 + 1,
    format(Stream, "~d ~d 0~n", [Top, X])
  ;
    format(Stream, "~d 0~n", [X])
  ),
  constraints(G1, G2, T, Stream).

constraints(G1, G2, [(_Label1 -< _Label2)|T], Stream) :-
  print_message(warning, 'mapping restriction -< is ignored in search_reduction'),
  constraints(G1, G2, T, Stream).
