:- module(
  sepi_neigh_merge,
  [
    % Commands
    merge_neigh/3
  ]
).


:- use_module(biocham).
:- use_module(sepi_infos).
:- use_module(sepi_util).


%%  two-neighbours local merge restriction
%%  Optional, used with option(merge_restriction: neigh)


%! merge_neigh(+G1, +G2, +Stream)
%
% write strict two neighbours merge restriction
%
% @arg G1     graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg G2     graph 2 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg Stream Stream for the CNF file
merge_neigh(G1, G2, Stream):-
  assert_G1(G1),
  assert(arc_G1(0, 0)),
  assert_neighbours(G1),
  debug(sepi, "done listing neighbours", []),
  neigh_clauses_assert(G1, G2, Stream),
  debug(sepi, "done writing 2 neighbours clauses", []),
  retractall(neigh(_, _)),
  retractall(arc_G1(_, _)).


%! assert_G1(+G1)
%
% Asserts a fact arc_G1(A, B) for each arc to optimise the merge restriction
%
% @arg G1     graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
assert_G1(G1):-
  G1 = [_, _, E1, _],
  forall(
    member((A, B), E1),
    assert(arc_G1(A, B))
  ).


%! assert_neighbours(+G1)
%
% Asserts a fact neigh(A,B) when A and B are two neighbours 
%
% @arg G1     graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
assert_neighbours(G1):-
  G1 = [N1, _, _, _],
  N1_1 is N1 - 1,
  numlist(0,N1_1,Vertex_G1),
  forall(
    member(A, Vertex_G1),
    (
      forall(
        (
          arc_G1(A, B)
        ;
          arc_G1(B, A)
        ),
        (
          forall(
            (
              arc_G1(C, B)
            ;
              arc_G1(B, C)
            ),
            assert(neigh(A,C))
          )
        )
      )
    )
  ).


%! neigh_clauses_assert(+G1, +G2, +Stream)
%
% Search for all non two neighbours vertices
%
% @arg G1     graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg G2     graph 2 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg Stream Stream for the CNF file
neigh_clauses_assert(G1, G2, Stream):-
  G1 = [N1, _, _, _],
  N1_1 is N1 - 1,
  numlist(0,N1_1,Vertex_G1),
  forall(
    (
      member(A, Vertex_G1),
      member(B, Vertex_G1)
    ),
    (
      (
        not(neigh(A,B)),
        not(neigh(B,A)),
        not(A is B)
      )
    ->
      debug(sepi, "A ~w B ~w not neigh", [A, B]),
      neigh_clauses_not_neigh_assert(A, B, N1, G2, Stream)
    ;
      debug(sepi, "A ~w B ~w neigh", [A, B])
    )
  ).


%! neigh_clauses_not_neigh_assert(+A, +B, +N1, +G2, +Stream)
%
% mu is a morphism from an initial graph G1 to an image graph G2
% A and B are not two-neighbours, they can't have the same image through mu
% writes '-mu(a)=u or -mu(b)=y' for all y in G2
%
% @arg A      First non neighbours vertex
% @arg B      Second non neighbours vertex
% @arg N1     Number of vertices in graph 1
% @arg G2     graph 2 [NbVertexe, NbSpecie, EdgesList, Id]
% @arg Stream Stream for the CNF file
neigh_clauses_not_neigh_assert(A, B, N1, G2, Stream):-
  G2 = [N2, _, _, _],
  N2_1 is N2 - 1,
  numlist(0,N2_1,Vertex_G2),
  forall(
    (
      member(Y, Vertex_G2)
    ),
    (
      m_ij(A, Y, N2, May),
      m_ij(B, Y, N2, Mby),
      get_option(extremal_sepi, Extremal_sepi),
      (
        Extremal_sepi \= no
      ->
        Top is N1 + 1,
        format(Stream, "~d -~d -~d 0~n", [Top, May, Mby])
      ;
        format(Stream, "-~d -~d 0~n", [May, Mby])
      ),
      debug(merge, "A ~w B ~w Y ~w", [A, B, Y])
    )
  ).
