:- use_module(library(plunit)).

:- begin_tests(arithmetic_rules, [setup((clear_model, reset_options))]).

user:message_hook(undefined_parameter(_Name), warning, _). % block the Warning print_message

test('simplify1', []) :-
  simplify(a+2*a, 3*a),
  simplify(-1*a/b+2*a/b, a/b),
  simplify(1/x/2, 1/x/2).

test('distribute1', [true(Out == a * a + a * b + c)]) :-
  distribute(a * (a + b) + c, Out).

test('distribute2', []) :-
  distribute((a+b)^2, a*a+a*b+(b*a+b*b)),
  distribute((a+b)^ -2, (a+b)^ -2).

test('additive_normal_form') :-
  additive_normal_form(a*(b+c*2), a*b+2*(a*c)),
  additive_normal_form(a*2+b, 2*a+b),
  additive_normal_form(a*2-b, 2*a+ -1*b),
  additive_normal_form((a+b)/(-(b)*2), -0.5*(a/b)+ -0.5),
  additive_normal_form(a+b/(-(b)*2), a+ -0.5),
  additive_normal_form(2*a*3, 6*a),
  additive_normal_form(2*(a+1)^2*3, 6*a^2+6*a+(6*a+6)).

test('determine_sign', []) :-
  parameter([k1 = 2, k2 = 3]),
  determine_sign((a+1)^2, pos),
  determine_sign((a-1)^3, dnk),
  determine_sign(-exp(a), neg),
  determine_sign(a/(a+1), pos),
  determine_sign((2+a)*0, zero),
  determine_sign((x-1)^k1, pos),
  determine_sign((x-1)^k2, dnk).

test('always_positive_negative', []) :-
  parameter([k1 = 2, k2 = 3]),
  always_positive((a+1)^2),
  always_negative(-exp(a)),
  always_positive(a/(a+1)),
  always_positive((2+a)*0),
  always_negative((2+a)*0),
  always_positive((x-1)^k1).

%test('always_positive_warning', [true(Warnings == 1)]) :-
% set_counter(warnings, 0),
% always_positive(w),
% count(warnings, Warnings).

test('is_null', []) :-
  is_null(a*0*1/2),
  \+(is_null(a*b/0)).

:- end_tests(arithmetic_rules).
