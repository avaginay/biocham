:- use_module(library(plunit)).

% Only for separate compilation/linting
:- use_module(ode).
:- use_module(reaction_rules).


:- begin_tests(ode, [setup((clear_model, reset_options))]).


test('ode_system', [ODEs == [d(b)/dt = a, d(a)/dt = -a]]) :-
  clear_model,
  command(a => b),
  ode_system,
  all_odes(ODEs).

test(
  'import_ode',
  [true(
    ODEs == [init(a = 0), init(b = 0), d(a)/dt = -a, par(k = -2),
    par(kk = 1.0), d(b)/dt = k*a])]
) :-
  setup_call_cleanup(
    open('test.ode', write, TestOde),
    write(TestOde, '
# Comment
init a = 0
B(0) = 0
wiener q
da/dt = -a
par k =-2
p kk = 1.0
B''= k*a
@ meth=cvode
done
'),
    close(TestOde)
  ),
  import_ode('test.ode'),
  delete_file('test.ode'),
  get_current_ode_system(Id),
  all_items([parent: Id], ODEs).

test(
  'add_reactions_from_ode_system',
  [true(Reactions == [v*a/(a+k) for a => b])]
) :-
  clear_model,
  new_ode_system,
  parameter([v=1, k=1]),
  add_ode(da/dt = -v*a/(a+k)),
  add_ode(db/dt = v*a/(a+k)),
  add_ode(dc/dt = 1),
  delete_ode([c]),
  add_reactions_from_ode_system,
  all_items([kind: reaction], Reactions),
  load_reactions_from_ode_system,
  all_items([kind: reaction], Reactions).

test(
  'add_reactions_from_ode_system_2',
  [true(Reactions == [v*a/(a+k) for a => b])]
) :-
  clear_model,
  new_ode_system,
  add_ode(da/dt = -v*a/(a+k)),
  add_ode(db/dt = v*a/(a+k)),
  add_ode(dc/dt = 1),
  delete_ode([c]),
  add_reactions_from_ode_system,
  all_items([kind: reaction], Reactions),
  load_reactions_from_ode_system,
  all_items([kind: reaction], Reactions).


test(
  'export_ode',
  [true((Lines, Reactions) == (
    [
      'init b = 0', 'init a = 1', 'par k = 2',
      'db/dt = k*a', 'da/dt = - (k*a)', 'done'
    ],
    ['MA'(k) for a => b]
  ))]
) :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 2)),
  command(present(a)),
  export_ode('test.ode'),
  list_file_lines('test.ode', Lines),
  import_ode('test.ode'),
  delete_file('test.ode'),
  single_model(Id),
  all_items([parent: Id, kind: reaction], Reactions).

test(
  'export_ode_as_latex',
  [true(Lines == [
      '\\begin{align*}', '{b}_0 &= 0\\\\',
      '{a}_0 &= 1\\\\', 'k &= 2\\\\',
      '\\frac{db}{dt} &= k*a\\\\',
      '\\frac{da}{dt} &= - (k*a)\\\\',
      '\\end{align*}'
    ])]
) :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 2)),
  command(present(a)),
  export_ode_as_latex('test.tex'),
  list_file_lines('test.tex', Lines),
  delete_file('test.tex').

test(
  'remove_fraction',[]) :-
  remove_fraction([a], d(a)/dt = a/b, d(a)/dt = a),
  remove_fraction([a], d(a)/dt = a/(b+c) + d, d(a)/dt = a + d*b + d*c),
  remove_fraction([a], d(a)/dt = a/(b+c) + -1*d, d(a)/dt = a - d*b - d*c),
  remove_fraction([a], d(a)/dt = a/b + c/d + e, d(a)/dt = a*d + c*b + e*d*b),
  remove_fraction([a], d(a)/dt = a/b + c/d, d(a)/dt = a*d + c*b).

test(
  'normalize_ode1', []) :-
  clear_model,
  with_current_ode_system((
    add_ode(d(x)/dt = (1*(k9*k1*k2) + (-1)*(k9*k3*x1) + (-1)*(k9*k8*x*x/(x+k7)))/k9),
    normalize_ode([d(x)/dt = k9*k1*k2/k9+ - (k9*k3*x1)/k9+ - (k9*k8*x^2)/(k9*(x+k7))], [x])
  )).

test(
  'normalize_ode2', []) :-
  clear_model,
  with_current_ode_system((
    add_ode(d(x)/dt = k7+ -1*k8*x + -1*k9*x/(x+k1)),
    normalize_ode([d(x)/dt=k7/1+ - (k8*x)/1+ - (k9*x)/(x+k1)], [x])
  )).

test('is_polynomial_sr', [nondet]) :-
  clear_model,
  %Ensure that a, b and c are recognized as molecules
  add_ode([d(a)/dt = 1, d(b)/dt = 1, d(c)/dt = 1]),
  command(parameter(x=1, y=1, z=1)),
  is_polynomial_sr(a*b + c^3 - 2*b),
  is_polynomial_sr(a*(c/4)^2),
  is_polynomial_sr(a/(x+y)),
  \+(is_polynomial_sr(a*(c/4)^ -2)),
  \+(is_polynomial_sr(exp(2*a))),
  \+(is_polynomial_sr(2*a/b)).

:- end_tests(ode).
