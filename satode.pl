/** <satode> SAT-solving ODE generation
 *
 * run "sat(NbVars, NbClauses), pl2xpp('outfile.ode')."
 * then call "biocham outfile.ode" and type-in the parameters for clauses
 * parameter(c_1_1=1, c_1_2=1, c_2_1= -1, c_2_2= -1, c_3_1= -1, c_3_2=1).
 * numerical_simulation.
 * plot(show: {x_1, x_2}).
 *
 * Or "read_dimacs('sat.cnf'), pl2xpp('out.ode')." and then you can directly
 * run "numerical_simulation. plot."
 *
 * @author Sylvain Soliman
 * @license GPL2
 */

:- module(satode, [sat/2, read_dimacs/1]).
:- reexport(minibiocham).
:- use_module(symbolic).

%! sat(+N:integer, +M:integer) is det.
%
% Generates the ODE system for a SAT problem with N variables and M clauses
sat(N, M) :-
  new_ode,
  forall(
    between(1, N, I),
    (
      make_name('x', I, Xi),
      assertz(ode_init(Xi, 0.5)),
      big_sum(I, M, N, Gi),
      simplify(Gi, GGi),
      assertz(ode_dxdt(Xi, GGi)),
      make_name('s', I, Si),
      assertz(ode_func(Si, 2 * Xi - 1))
    )
  ),
  forall(
    between(1, M, J),
    (
      make_name('a', J, Aj),
      make_name('k', J, Kj),
      assertz(ode_init(Aj, 1)),
      assertz(ode_dxdt(Aj, Aj * Kj)),
      big_prod(J, N, 0, Pj),
      simplify(Pj, PPj),
      assertz(ode_func(Kj, PPj)),
      make_name('c', J, Cj),
      forall(
        between(1, N, I),
        (
          make_name(Cj, I, Cji),
          assertz(ode_par(Cji, 0))
        )
      )
    )
  ).


%! make_name(+Prefix:atom, +I:integer, -Name:atom) is det.
%
% Make a name from a prefix and a number
make_name(Prefix, I, Name) :-
  format(atom(Name), "~w_~d", [Prefix, I]).


%! big_prod(+J:integer, +N:integer, +Exclude, -Pj:term) is det.
%
% Pj is the term corresponding to the prod of all 1 - CjiSi except Exclude
% and normalized with 1/2^N
big_prod(J, N, Exclude, Pj) :-
  %% Start is 1 / (2^N),   % Could normalize with 2^k for k-SAT
  Start is 1,
  findall(
    C,
    (
      between(1, N, I),
      I \= Exclude,
      make_name('c', J, Cj),
      make_name(Cj, I, Cji),
      make_name('s', I, Si),
      C = 1 - Cji * Si
    ),
    L
  ),
  foldl(prod, L, Start, Pj).


prod(X, Y, Y*X).


%! big_sum(+I:integer, +M:integer, +N:integer, -Gj:term) is det.
%
% Gj is the term corresponding to the sum of all AjCjiKjiKj
big_sum(I, M, N, Gi) :-
  findall(
    C,
    (
      between(1, M, J),
      make_name('c', J, Cj),
      make_name(Cj, I, Cji),
      make_name('a', J, Aj),
      make_name('k', J, Kj),
      big_prod(J, N, I, Kji),
      C = Aj * Cji * Kj * Kji
    ),
    L
  ),
  foldl(sum, L, 0, Gi).


sum(X, Y, Y+X).


read_dimacs(File) :-
  setup_call_cleanup(
    open(File, read, Stream),
    (
      read_header(Stream, N, M),
      sat(N, M),
      forall(
        between(1, M, J),
        read_clause(Stream, J)
      )
    ),
    close(Stream)
  ).


read_header(Stream, N, M) :-
  read_line_to_string(Stream, Line),
  split_string(Line, " ", " ", ["p", "cnf", Nvar, Nclauses]),
  string_to_number(Nvar, N),
  string_to_number(Nclauses, M).


read_clause(Stream, Index) :-
  read_line_to_string(Stream, Line),
  split_string(Line, " ", " ", VarStrings),
  maplist(string_to_number, VarStrings, Vars),
  maplist(set_clause_parameter(Index), Vars).


string_to_number(String, Number) :-
  read_term_from_atom(String, Number, []),
  number(Number).


set_clause_parameter(_Clause, 0) :-
  !.

set_clause_parameter(Clause, N) :-
  N > 0,
  !,
  make_name('c', Clause, Cj),
  make_name(Cj, N, Cji),
  retractall(ode_par(Cji, _)),
  assertz(ode_par(Cji, 1)).

set_clause_parameter(Clause, N) :-
  N < 0,
  NN is -N,
  make_name('c', Clause, Cj),
  make_name(Cj, NN, Cji),
  retractall(ode_par(Cji, _)),
  assertz(ode_par(Cji, -1)).
