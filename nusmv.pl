:- module(
  nusmv,
  [
    % options
    boolean_initial_states/1,
    boolean_semantics/1,
    boolean_state_display/1,
    % commands
    export_nusmv/1,
    check_ctl/0,
    check_ltl/1,
    % API
    ctl_truth/2,
    check_ctl_impl/3,
    check_several_queries/3,
    enumerate_all_molecules/1,
    display_vector/1,
    display_vectors/1,
    display_vector_header/0
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(objects).
:- use_module(reaction_rules).

:- use_module(util).
:- use_module(influence_properties).


:- doc('CTL properties can be efficiently verified with model-checkers. Biocham uses the NuSMV model checker \\cite{nusmv} for which the following options can be specified:').

:- grammar(boolean_semantics).

boolean_semantics(positive).

boolean_semantics(negative).
:- doc('\\emphright{The default value is \\texttt{negative}}').

:- initial(option(boolean_semantics: negative)).
:- doc('\\emphright{The negative Boolean semantics interpret reaction inhibitors with a negation in the condition. The positive Boolean semantics ignore reaction inhibitors.}').


:- grammar(boolean_initial_states).

boolean_initial_states(all).

boolean_initial_states(some).

boolean_initial_states(all_then_some).

boolean_initial_states(present).
:- doc('
  \\emphright{The default value is \\texttt{all}. The value
  \\texttt{all_then_some} tries for all initial states, and if that fails, for
  some}. The value \\texttt{present} treats undetermined species as absent.').


:- grammar(boolean_state_display).

boolean_state_display(present).

boolean_state_display(table).

boolean_state_display(vector).
:- doc('\\emphright{Display boolean states as a \\texttt{present} command. (default), or a TAB separated table of TRUE/FALSE values, or an association-list with 0/1 values.}').


:- initial(option(boolean_initial_states: all)).
:- doc('\\emphright{CTL formulae can be checked in all or some initial states.}').

:- initial(option(boolean_trace: no)).
:- doc('\\emphright{Display either a Boolean trace proving the formula in some initial state, or a counter example initial state falsifying the formula (default value is no trace).}').

:- initial(option(nusmv_topological_order: no)).
:- doc('\\emphright{Ordering of variables for the NuSMV model-checker.}').

:- initial(option(query: current_spec)).

:- initial(option(boolean_state_display: present)).

check_ctl :-
  biocham_command,
  doc('
    evaluates the current CTL specification (i.e., the conjunction of all
    formulae of the current specification) or the content of option
    \\texttt{query} on the current model by calling the NuSMV model-checker.

    As is usual in Model-Checking, the query is evaluated for all possible
    initial states (\\texttt{Ai} in Biocham v3). This can be changed via
    the \\texttt{boolean_initial_states} option.
  '),
  option(query, ctl, Query, 'Query to evaluate instead of the current CTL
    specification.'),
  option(boolean_initial_states, boolean_initial_states, NusmvInitialState,
	 'specifies whether the truth value of a formula is for all or some completion of the initial states (present/absent/undefined).'),
  option(boolean_trace, yesno, _,
    'shows a proof trace or a counter example initial state if available  (default value is
      "no").'),
  option(boolean_semantics, boolean_semantics, _,
    'choice of positive/negative boolean semantics (positive: reaction inhibitors are ignored; negative: the presence of one reaction inhibitor prevents the reaction to proceed.'),
  option(nusmv_topological_order, yesno, _,
    'tells NuSMV to keep the order of the variables of the BIOCHAM model for building its internal Binary Decision Diagram.'),
  option(boolean_state_display, boolean_state_display, _,
    'choice of display of the boolean states.'),
  (
    Query == current_spec
  ->
    findall(Formula, item([kind: ctl_spec, item: Formula]), Specs),
    join_with_op(Specs, '/\\', Spec)
  ;
    Spec = Query
  ),
  check_ctl_impl(Spec, NusmvInitialState, Result),
  format('~w is ~w\n', [Query, Result]).


:- doc('\\begin{example}\n').
:- biocham_silent(clear_model).
:- biocham(present(a)).
:- biocham(absent(b)).
:- biocham(a => b).
:- biocham(a + b => a).
:- biocham('check_ctl(query: EX(not(a) \\/ EG(not(b))), boolean_trace:yes)').
:- biocham('check_ctl(query: EG(not(b)), boolean_trace:yes)').
:- biocham(check_ctl(query: reachable(b), boolean_trace:yes)).
:- biocham(generate_ctl).
:- biocham(check_ctl).
:- doc('\\end{example}').

:- doc('A Linear Time Logic (LTL) formula contains no path quantifier. The syntax of LTL formulae is given in the next chapter for the more general setting of First-Order LTL (FO-LTL) formulae with numerical constraints.
      Here a boolean LTL formula is true if it is satisfied by all paths and all boolean initial states.
      This can be changed by double negation with the option \\texttt{boolean_initial_states: some} to verify that there exists an initial state and a path satisfying the formula.
      Unlike CTL, the path quantifier cannot be decoupled from the quantifier on the initial state.').
check_ltl(Query) :-
  biocham_command,
  doc('
    evaluates the given boolean LTL specification on the current model by
    calling the NuSMV model-checker in Bounded Model-Checking mode.
  '),
  type(Query, foltl),
  option(boolean_initial_states, boolean_initial_states, NusmvInitialState,
	 'specifies whether the truth value of a LTL formula is for all or some completion of the initial states (present/absent/undefined) and for all or some paths.'),
  option(boolean_trace, yesno, _,
    'shows a proof trace or a counter example initial state if available  (default value is
      "no").'),
  option(boolean_semantics, boolean_semantics, _,
    'Use positive or negative boolean semantics for inhibitors.'),
  option(boolean_state_display, boolean_state_display, _,
    'choice of display of the boolean states.'),
  check_ltl_impl(NusmvInitialState, Query, Result),
  format('~w is ~w\n', [Query, Result]).


:- doc('\\begin{example}\n').
:- biocham_silent(clear_model).
:- biocham(present(a)).
:- biocham(absent(b)).
:- biocham(a => b).
:- biocham(a + b => c).
:- biocham('check_ltl(reachable(c), boolean_trace:yes)').
:- biocham('check_ltl(reachable(c), boolean_initial_states: some, boolean_trace:yes)').
:- doc('\\end{example}').

export_nusmv(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('
    exports the current Biocham set of reactions and initial state in an SMV
    \\texttt{.smv} file.
  '),
  option(boolean_semantics, boolean_semantics, BoolSem,
    'Use positive or negative boolean semantics for inhibitors.'),
  automatic_suffix(OutputFile, '.smv', write, FilenameSmv),
  export_nusmv_file(FilenameSmv, BoolSem).


export_nusmv_file(FilenameSmv, BoolSem) :-
  setup_call_cleanup(
    open(FilenameSmv, write, Stream),
    export_nusmv_stream(Stream, BoolSem),
    close(Stream)
  ).


export_nusmv_stream(Stream, BoolSem) :-
  write(Stream, 'MODULE main\n\n'),
  enumerate_rules(BoolSem, Rules),
  enumerate_all_molecules(Molecules),
  Molecules \= [],
  export_initial_state(Stream, Molecules),
  export_transitions(Stream, Molecules, Rules).


enumerate_rules(BoolSem, Rules) :-
  enumerate_reactions(Reactions),
  mark_subsumed_influences(BoolSem),
  enumerate_positive_influences(PositiveInfluences, BoolSem),
  enumerate_negative_influences(NegativeInfluences, BoolSem),
  append(PositiveInfluences, NegativeInfluences, Influences),
  append(Reactions, Influences, Rules).


enumerate_reactions(Reactions) :-
  findall(
    (Reactants, Inhibitors, Products, []),
    (
      item([kind: reaction, item: Item]),
      reaction(Item, [
        reactants: Reactants,
        inhibitors: Inhibitors,
        products: Products
      ])
    ),
    Reactions
  ).


enumerate_positive_influences(PositiveInfluences, BoolSem) :-
  findall(
    (PositiveInputs, NegativeInputs, [Output | PositiveInputs], NegativeInputs),
    (
      item([kind: influence, item: Item]),
      \+ subsumed_influence(Item),
      influence(Item, [
        positive_inputs: PositiveInputs,
        negative_inputs: NegInputs,
        sign: '+',
        output: Output
      ]),
      (
        BoolSem == positive
      ->
        NegativeInputs = []
      ;
        subtract(NegInputs, [Output], NegativeInputs)
      )
    ),
    PositiveInfluences
  ).


enumerate_negative_influences(NegativeInfluences, BoolSem) :-
  findall(
    (PositiveInputs, NegativeInputs, PositiveInputs, [Output | NegativeInputs]),
    (
      item([kind: influence, item: Item]),
      \+ subsumed_influence(Item),
      influence(Item, [
        positive_inputs: PosInputs,
        negative_inputs: NegInputs,
        sign: '-',
        output: Output
      ]),
      (
        BoolSem == positive
      ->
        NegativeInputs = []
      ;
        NegativeInputs = NegInputs
      ),
      subtract(PosInputs, [Output], PositiveInputs)
    ),
    NegativeInfluences
  ).


export_initial_state(Stream, Molecules) :-
  forall(
    member(Molecule, Molecules),
    (
      make_nusmv_name(Molecule, Name),
      format(Stream, 'VAR ~a: boolean;\n', [Name]),
      get_initial_state(Molecule, InitialState),
      boolean_initial_state(InitialState, NusmvInitialState),
      format(Stream, 'INIT ~a in ~a;~n', [Name, NusmvInitialState])
    )
  ),
  nl(Stream).


:- dynamic(allmolecules/1).


export_transitions(Stream, Molecules, Reactions) :-
  retractall(allmolecules(_)),
  assertz(allmolecules(Molecules)),
  write(Stream, 'TRANS '),
  maplist(reaction_to_trans, Reactions, Trans),
  maplist(reaction_to_nofire, Reactions, Loops),
  all_nochange(NoChange),
  atomic_list_concat([NoChange | Loops], ' & ', Loop),
  atomic_list_concat([Loop | Trans], ' | ', TransOred),
  write(Stream, TransOred),
  nl(Stream).


reaction_to_trans((PositiveInputs, NegativeInputs, Products, Degraded), Trans) :-
  (
    PositiveInputs == []
  ->
    PositiveNames = [],
    NusmvPositive = 'TRUE'
  ;
    maplist(reactant_to_name, PositiveInputs, PositiveNames),
    apply_and_join(
      [make_nusmv_name],
      PositiveNames,
      ' & ',
      NusmvPositive
    )
  ),
  (
    NegativeInputs == []
  ->
    NegativeNames = [],
    NusmvNegative = 'TRUE'
  ;
    maplist(reactant_to_name, NegativeInputs, NegativeNames),
    apply_and_join(
      [make_nusmv_name, prepend_not],
      NegativeNames,
      ' & ',
      NusmvNegative
    )
  ),
  (
    Products == []
  ->
    ProductNames = [],
    NusmvProducts = 'TRUE'
  ;
    maplist(reactant_to_name, Products, ProductNames),
    apply_and_join(
      [make_nusmv_name, surround_next],
      ProductNames,
      ' & ',
      NusmvProducts
    )
  ),
  (
    Degraded == []
  ->
    DegradedNames = [],
    NusmvDegraded = 'TRUE'
  ;
    maplist(reactant_to_name, Degraded, DegradedNames),
    apply_and_join(
      [make_nusmv_name, surround_next, prepend_not],
      DegradedNames,
      ' & ',
      NusmvDegraded
    )
  ),
  allmolecules(Molecules),
  subtract(Molecules, PositiveNames, NoPositive),
  subtract(NoPositive, NegativeNames, NoNegative),
  subtract(NoNegative, ProductNames, NoProducts),
  subtract(NoProducts, DegradedNames, OtherMolecules),
  (
    OtherMolecules == []
  ->
    NusmvOthers = 'TRUE'
  ;
    apply_and_join(
      [make_nusmv_name, equiv_next],
      OtherMolecules,
      ' & ',
      NusmvOthers
    )
  ),
  atomic_list_concat(
    [NusmvPositive, NusmvNegative, NusmvProducts, NusmvDegraded, NusmvOthers],
    ' & ',
    Trans
  ).


reaction_to_nofire((PosInputs, NegInputs, _, _), ParenLoop) :-
  (
    PosInputs == []
  ->
    NusmvPos = 'FALSE'
  ;
    maplist(reactant_to_name, PosInputs, PosNames),
    apply_and_join(
      [make_nusmv_name, prepend_not],
      PosNames,
      ' | ',
      NusmvPos
    )
  ),
  (
    NegInputs == []
  ->
    NusmvNeg = 'FALSE'
  ;
    maplist(reactant_to_name, NegInputs, NegNames),
    apply_and_join(
      [make_nusmv_name],
      NegNames,
      ' | ',
      NusmvNeg
    )
  ),
  atomic_list_concat([NusmvPos, NusmvNeg], ' | ', Loop),
  format(atom(ParenLoop), '(~a)', [Loop]).


all_nochange(Result) :-
  allmolecules(Molecules),
  apply_and_join(
    [make_nusmv_name, equiv_next],
    Molecules,
    ' & ',
    Result
  ).


apply_and_join([], List, Join, Result) :-
  atomic_list_concat(List, Join, Result).

apply_and_join([Function | Functions], List, Join, Result) :-
  maplist(Function, List, NewList),
  apply_and_join(Functions, NewList, Join, Result).


surround_next(Atom, Next) :-
  format(atom(Next), 'next(~a)', [Atom]).


prepend_not(Atom, Not) :-
  format(atom(Not), '!~a', [Atom]).


equiv_next(Atom, Equiv) :-
  format(atom(Equiv), '(~a <-> next(~a))', [Atom, Atom]).


reactants_to_condition(Reactants, Condition) :-
  apply_and_join([reactant_to_name, make_nusmv_name], Reactants, ' & ', Condition).


reactant_to_name(_ * Name, Name) :-
  !.

reactant_to_name(Name, Name).


objects_to_condition(Objects, Condition) :-
  apply_and_join(make_nusmv_name, Objects, ' & ', Condition).


boolean_initial_state(initial_state(_=N), 'TRUE') :-
  \+ (is_null(N)).

boolean_initial_state(absent, 'FALSE').

boolean_initial_state(undefined, R):-
  get_option(boolean_initial_states, Option),
  (Option=present
  ->
   R='FALSE'
  ;
   R='{FALSE, TRUE}'
  ).


make_nusmv_name(Molecule@Location, Name) :-
  !,
  format(atom(A), '~w', [Molecule@Location]),
  make_nusmv_name(A, Name).

make_nusmv_name(Molecule, Name) :-
  reserved(Molecule),
  !,
  atom_concat('_', Molecule, Name).

make_nusmv_name(Molecule, Name) :-
  atom_chars(Molecule, MoleculeChars),
  findall(
    TranslatedChar,
    (
      member(Char, MoleculeChars),
      (
        translate_char(Char, TranslatedChar)
      ->
        true
      ;
        TranslatedChar = Char
      )
    ),
    TranslatedList
  ),
  atomic_list_concat(TranslatedList, Name).


translate_char(' ', '_').

translate_char('_', '__').

translate_char(',', '_c').

translate_char('~', '').

translate_char('{', '_l').

translate_char('}', '_r').

translate_char('(', '_L').

translate_char(')', '_R').

translate_char('@', '_#').


reserved('B').

reserved('F').

reserved('G').

reserved('H').

reserved('O').

reserved('S').

reserved('T').

reserved('V').

reserved('W').

reserved('X').

reserved('Y').

reserved('Z').

reserved('A').

reserved('E').

reserved('U').

reserved('xor').

reserved('R').

reserved('MAX').

reserved(Atom) :-
  ctl_unary_op(Atom).


ctl_unary_op(Op) :-
  memberchk(Op, ['AF', 'EF', 'AG', 'EG', 'AX', 'EX']).


ltl_unary_op(Op) :-
  memberchk(Op, ['F', 'G', 'X']).


ltl_binary_op('=>', '->').

ltl_binary_op('<=>', '<->').

ltl_binary_op('\\/', '|').

ltl_binary_op('/\\', '&').

ltl_binary_op('U', 'U').

ltl_binary_op('R', 'V').

ltl_binary_op('W', 'V').



% stores the counter-example trace from NuSMV
% argument is a list, with R, then molecules in enumerate_all_molecules order
:- dynamic(trace/1).


% stores unknown molecules coming from the query
:- dynamic(query_molecules/1).


query_molecules([]).


enumerate_all_molecules(Molecules) :-
  enumerate_molecules(ModelMolecules),
  query_molecules(QueryMolecules),
  % order of the second set remains unchanged
  union(QueryMolecules, ModelMolecules, Molecules).



check_ctl_impl(Query, all_then_some, Result) :-
  check_ctl_impl(Query, all, AllResult),
  (
    AllResult == 'true'
  ->
    Result = 'true'
  ;
    check_ctl_impl(Query, some, Result)
  ).


check_ctl_impl(Query, some, Result) :-
  check_ctl_impl(not(Query), all, NotResult),
  (
    NotResult == 'true'
  ->
    Result = 'false'
  ;
    NotResult == 'false'
  ->
    Result = 'true'
  ;
    Result = 'error'
  ).

% export initial state will instanciate to FALSE the undetermined species
check_ctl_impl(Query, present, Result) :-
  check_ctl_impl(Query, all, Result). 

% care that if Result is instantiated, the predicate might fail before parsing
% the full output/trace
check_ctl_impl(Query, all, Result) :-
  get_option(boolean_trace, NusmvCX),
  get_option(nusmv_topological_order, Topo),
  retractall(trace(_)),
  normalize_query(Query, NQuery, Mols),
  retractall(query_molecules(_)),
  assertz(query_molecules(Mols)),
  translate_ctl_for_nusmv(NQuery, NusmvQuery),
  export_nusmv_tmpfile(SMVFile),
  tmp_file_stream(text, CmdFile, Stream),
  format(Stream, 'set input_file "~a"\ngo\n', [SMVFile]),
  (
    NusmvCX == 'no'
  ->
    write(Stream, 'set counter_examples 0\n')
  ;
    write(Stream, 'set default_trace_plugin 2\nset counter_examples 1\n')
  ),
  (
    Topo == 'no'
  ->
    true
  ;
    write(Stream, 'set vars_order_type topological\n')
  ),
  format(Stream, 'check_ctlspec -p "~a"\nquit\n', [NusmvQuery]),
  close(Stream),
  call_nusmv(CmdFile, NusmvOut),
  enumerate_all_molecules(Molecules),
  parse_and_clean(NusmvOut, NusmvCX, Result, Molecules, CmdFile, SMVFile).


call_nusmv(CmdFile, NusmvOut) :-
  call_subprocess(
    path('NuSMV'),
    ['-df', '-source', CmdFile],
    [stdin(null), stdout(pipe(NusmvOut))]
  ).


ctl_truth(Query, Result):-
  devdoc('checks the truth value of a CTL formula.'),
  get_option(boolean_initial_states, NusmvInitialState),
  check_ctl_impl(Query, NusmvInitialState, Result).


%% FIXME code duplication with check_ctl_impl
check_ltl_impl(all_then_some, Query, Result) :-
  check_ltl_impl(all, Query, AllResult),
  (
    AllResult == 'true'
  ->
    Result = 'true'
  ;
    check_ltl_impl(some, Query, Result)
  ).


check_ltl_impl(some, Query, Result) :-
  check_ltl_impl(all, not(Query), NotResult),
  (
    NotResult == 'true'
  ->
    Result = 'false'
  ;
    NotResult == 'false'
  ->
    Result = 'true'
  ;
    Result = 'error'
  ).

% export initial state will instanciate the undetermined species
check_ltl_impl(present, Query, Result):-
  check_ltl_impl(all, Query, Result).


% care that if Result is instantiated, the predicate might fail before parsing
% the full output/trace
check_ltl_impl(all, Query, Result) :-
  get_option(boolean_trace, NusmvCX),
  retractall(trace(_)),
  expand_formula(Query, NQuery),
  retractall(query_molecules(_)),
  assertz(query_molecules([])),   % FIXME would something better be needed?
  enumerate_all_molecules(Molecules),
  length(Molecules, NVar),
  nb_setval(given_bound, NVar),
  translate_ltl_for_nusmv(NQuery, NusmvQuery),
  export_nusmv_tmpfile(SMVFile),
  tmp_file_stream(text, CmdFile, Stream),
  format(Stream, 'set input_file "~a"\ngo_bmc\n', [SMVFile]),
  (
    NusmvCX == 'no'
  ->
    write(Stream, 'set counter_examples 0\n')
  ;
    write(Stream, 'set default_trace_plugin 2\nset counter_examples 1\n')
  ),
  format(Stream, 'check_ltlspec_bmc -k ~d -p "~a"\nquit\n', [NVar, NusmvQuery]),
  close(Stream),
  call_nusmv(CmdFile, NusmvOut),
  parse_and_clean(NusmvOut, NusmvCX, Result, Molecules, CmdFile, SMVFile).


export_nusmv_tmpfile(SMVFile) :-
  get_option(boolean_semantics, BoolSem),
  tmp_file_stream(text, SMVFile, Stream),
  close(Stream),
  export_nusmv_file(SMVFile, BoolSem).


parse_and_clean(NusmvOut, NusmvCX, Result, Molecules, CmdFile, SMVFile) :-
  call_cleanup(
    parse_nusmv_out(NusmvOut, NusmvCX, Result),
    (
      close(NusmvOut),
      delete_file(CmdFile),
      delete_file(SMVFile)
    )
  ),
  (
    NusmvCX == 'yes',
    trace(_)
  ->
    write('Trace:\n'),
    findall(T, trace(T), Trace),
    display_states(Molecules, Trace),
    nl,
    findall(State, nusmv:trace(State), States),
    revision:rules_from_trace(States, Molecules, Rules),
    (
      Rules = [_ | _]
    ->
      write('Reactions used for that path:\n'),
      forall(
        member(Rule, Rules),
        format("~w~n", [Rule])
      )
    ;
      true
    )
  ;
    true
  ).


display_states(Molecules, States) :-
  get_option(boolean_state_display, Display),
  (
    (Display == table)
  ->
    % header
    S = [Molecules | States]
  ;
    S = States
  ),
  forall(
    member(Line, S),
    (
      display_state(Molecules, Line),
      nl
    )
  ).


display_state(Molecules, State) :-
  get_option(boolean_state_display, Display),
  (
    (Display == table)
  ->
    atomic_list_concat(State, '\t', Atom),
    write(Atom)
  ;
    (Display == present)
  ->
    table_to_present(State, Molecules, Present),
    write(Present)
  ;
    % Display == vector
    table_to_vector(State, Molecules, Vector),
    write(Vector)
  ).


display_vector(Vector) :-
  table_to_vector(State, Molecules, Vector),
  display_state(Molecules, State).


display_vectors([]) :-
  !.

display_vectors(Vectors) :-
  maplist(table_to_vector, States, [Molecules | _], Vectors),
  display_states(Molecules, States).


display_vector_header :-
  enumerate_all_molecules(Molecules),
  display_states(Molecules, []).


table_to_vector(State, Molecules, Vector) :-
  maplist(state_to_vector, State, Molecules, Vector).


state_to_vector("TRUE", M, M-1).

state_to_vector("FALSE", M, M-0).


table_to_present(State, Molecules, Present) :-
  maplist(state_to_present, State, Molecules, P),
  exclude(==(''), P, PP),
  atomic_list_concat(PP, ', ', Prez),
  format(atom(Present), "present({~w}).", [Prez]).


state_to_present("TRUE", M, M).

state_to_present("FALSE", _, '').

% specialized version of check_ctl_impl that only calls once NuSMV
% the counter-examples cannot be read back
check_several_queries(Queries, BoolSem, Results) :-
  maplist(normalize_query, Queries, NQueries, _),
  maplist(translate_ctl_for_nusmv, NQueries, NusmvQueries),
  % FIXME better template, in $TMP, check if not existing, etc.
  SMVFile = 'nusmv.smv',
  export_nusmv_file(SMVFile, BoolSem),
  CmdFile = 'nusmv.cmd',
  open(CmdFile, write, Stream),
  format(Stream, 'set input_file "~a"\ngo\n', [SMVFile]),
  write(Stream, 'set counter_examples 0\n'),
  forall(
    member(NusmvQuery, NusmvQueries),
    format(Stream, 'check_ctlspec -p "~a"\n', [NusmvQuery])
  ),
  write(Stream, 'quit\n'),
  close(Stream),
  call_subprocess(
    path('NuSMV'),
    ['-df', '-source', CmdFile],
    [stdin(null), stdout(pipe(NusmvOut))]
  ),
  call_cleanup(
    findall(
      Result,
      (
        member(_, Queries),
        parse_nusmv_out(NusmvOut, no, Result)
      ),
      Results
    ),
    (
      close(NusmvOut),
      delete_file(CmdFile),
      delete_file(SMVFile)
    )
  ).


translate_ctl_for_nusmv(A -> B, Atom) :-
  !,
  translate_ctl_for_nusmv(A, AA),
  translate_ctl_for_nusmv(B, BB),
  atomic_list_concat(['(', AA, ' -> ', BB, ')'], Atom).

translate_ctl_for_nusmv(A \/ B, Atom) :-
  !,
  translate_ctl_for_nusmv(A, AA),
  translate_ctl_for_nusmv(B, BB),
  atomic_list_concat(['(', AA, ' | ', BB, ')'], Atom).

translate_ctl_for_nusmv(A /\ B, Atom) :-
  !,
  translate_ctl_for_nusmv(A, AA),
  translate_ctl_for_nusmv(B, BB),
  atomic_list_concat(['(', AA, ' & ', BB, ')'], Atom).

translate_ctl_for_nusmv(not(A), Atom) :-
  !,
  translate_ctl_for_nusmv(A, AA),
  atomic_list_concat(['!', AA], Atom).

translate_ctl_for_nusmv('EU'(A, B), Atom) :-
  !,
  translate_ctl_for_nusmv(A, AA),
  translate_ctl_for_nusmv(B, BB),
  atomic_list_concat(['E [', AA, ' U ', BB, ']'], Atom).

translate_ctl_for_nusmv('AU'(A, B), Atom) :-
  !,
  translate_ctl_for_nusmv(A, AA),
  translate_ctl_for_nusmv(B, BB),
  atomic_list_concat(['A [', AA, ' U ', BB, ']'], Atom).

translate_ctl_for_nusmv('ER'(A, B), Atom) :-
  !,
  translate_ctl_for_nusmv(A, AA),
  translate_ctl_for_nusmv(B, BB),
  atomic_list_concat(['E [', AA, ' R ', BB, ']'], Atom).

translate_ctl_for_nusmv('AR'(A, B), Atom) :-
  !,
  translate_ctl_for_nusmv(A, AA),
  translate_ctl_for_nusmv(B, BB),
  atomic_list_concat(['A [', AA, ' R ', BB, ']'], Atom).

translate_ctl_for_nusmv(CTL, Atom) :-
  CTL =.. [Operator, A],
  ctl_unary_op(Operator),
  !,
  translate_ctl_for_nusmv(A, AA),
  atomic_list_concat([Operator, AA], ' ', Atom).

translate_ctl_for_nusmv(Object, Atom) :-
  make_nusmv_name(Object, Atom).


translate_ltl_for_nusmv(Object, Atom) :-
  Object =.. [Operator, A, B],
  ltl_binary_op(Operator, Op),
  !,
  translate_ltl_for_nusmv(A, AA),
  translate_ltl_for_nusmv(B, BB),
  atomic_list_concat(['(', AA, ' ', Op, ' ', BB, ')'], Atom).

translate_ltl_for_nusmv(not(A), Atom) :-
  !,
  translate_ltl_for_nusmv(A, AA),
  atomic_list_concat(['!', AA], Atom).

translate_ltl_for_nusmv(Object, Atom) :-
  Object =.. [Operator, A],
  ltl_unary_op(Operator),
  !,
  translate_ltl_for_nusmv(A, AA),
  atomic_list_concat([Operator, AA], ' ', Atom).

translate_ltl_for_nusmv(Object, Atom) :-
  make_nusmv_name(Object, Atom).


parse_nusmv_out(Out, NusmvCX, Result) :-
  read_line_to_codes(Out, Codes),
  debug(nusmv, "read: '~s'", [Codes]),
  (
    Codes = end_of_file
  ->
    Result = 'error'
  ;
    % "-- "
    Codes = [0'-, 0'-, 0'  | _]
  ->
    (
      append(_, [0'i, 0's, 0' , 0't, 0'r, 0'u, 0'e], Codes)
    ->
      Result = 'true'
    ;
      append(_, [0'i, 0's, 0' , 0'f, 0'a, 0'l, 0's, 0'e], Codes)
    ->
      Result = 'false'
    ;
      Codes = [0'-, 0'-, 0' , 0'n, 0'o | _]
    ->
      string_codes(String, Codes),
      string_concat("-- no counterexample found with bound ", Bound, String),
      number_string(CxBound, Bound),
      nb_getval(given_bound, GivenBound),
      (
        CxBound == GivenBound
      ->
        (
          Result = 'true',
          format("No counterexample was found with bound: ~d~n", [GivenBound])
        )
      ;
        parse_nusmv_out(Out, NusmvCX, Result)
      )
    ;
      Result = 'error'
    ),
    (
      NusmvCX == 'no'
    ->
      true
    ;
      parse_nusmv_out_trace(Out)
    )
  ;
    parse_nusmv_out(Out, NusmvCX, Result)
  ).


parse_nusmv_out_trace(Out) :-
  % -- as demonstrated by the following execution sequence
  read_line_to_codes(Out, [0'-, 0'-, 0'  | _]),
  !,
  read_line_to_codes(Out, [0'N, 0'a, 0'm, 0'e, 0'\t | _]),
  read_string(Out, "\n", "\r", _End, InitialString),
  state_string_to_trace(InitialString, InitialTrace),
  assertz(trace(InitialTrace)),
  repeat,
  read_line_to_codes(Out, _Constants),
  read_line_to_codes(Out, _Inputs),
  read_string(Out, "\n", "\r", End, StateString),
  (
    End = -1
  ->
    !
  ;
    state_string_to_trace(StateString, StateTrace),
    assertz(trace(StateTrace)),
    fail
  ).

parse_nusmv_out_trace(Out) :-
  read_line_to_codes(Out, end_of_file).


state_string_to_trace(String, Trace) :-
  string_concat("S", _, String),
  !,
  split_string(String, "\t", "\t", [_ | Trace]).

state_string_to_trace(_, _).
