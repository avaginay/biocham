#include <stdlib.h>
#include <cstddef>
#include <random>
#include <chrono>
#include <ppl.hh>
#include <math.h>
#include "ppl_validity_domain.hh"
#include "gsl_solver.hh"
#include "cmaes_interface.h"
#include "satisfaction_degree.hh"
#include "ode.inc"
#include "check.inc"
#include "search_parameters.inc"
#include "domain_distance.inc"
#include "filter.inc"

#include <mpi.h>

static double
satisfaction_degree(gsl_solver *solver, gsl_solver_config *config, double values[DIMENSION])
{
    gsl_solver_state state;
    Table table;
    double max_distance;
    Domain domain;

    init_gsl_solver_state(&state, solver);
    for (int j = 0; j < DIMENSION; j++) {
        const struct search_parameter *param = &search_parameters[j];
        double value = values[j];
        solver->p[param->index] = value;
        #ifdef DEBUG_ROBUSTNESS
        std::cerr << "param " << j << " = "  << value << std::endl;
        #endif
    }

    // initial values might depend on parameters
    initial_values(state.x, solver->p);

    double hstart = fmax(config->initial_step_size, (config->time_final - config->time_initial) * config->minimum_step_size);
    #ifdef DEBUG_ROBUSTNESS
    fprintf(stderr, "hmin: %f, hmax: %f, hstart: %f\n", solver->d->hmin, solver->d->hmax, solver->d->h);
    #endif
    gsl_solver_iterate(&state, &table);
    free_gsl_solver_state(&state);
    #ifdef DEBUG_ROBUSTNESS
    fprintf(stderr, "hmin: %f, hmax: %f, hfinal: %f\n", solver->d->hmin, solver->d->hmax, solver->d->h);
    fprintf(stderr, "hstart to reset to: %f\n", hstart);
    #endif
    // reset step size and evolution, forces again initial step size because
    // otherwise seems useless
    gsl_odeiv2_driver_reset(solver->d);
    // we have to manually reset, since gsl_odeiv2_driver_reset_hstart seems to think fabs(x) < x
    solver->d->h = hstart;
    #ifdef DEBUG_ROBUSTNESS
    fprintf(stderr, "hstart after manual reset: %f\n", solver->d->h);
    #endif

    PPL::set_rounding_for_PPL();
    domain = (*compute_condition_domain[0])(table);
    #ifdef DEBUG_ROBUSTNESS
    using PPL::IO_Operators::operator<<;
    std::cerr << domain << "." << std::endl;
    std::cerr << DIMENSION << " rows" << std::endl;
    #endif
    // Ignore negative distances for robustness computation
    max_distance = fmax(0.0, domain_distance(domain, 0));
    #ifdef DEBUG_ROBUSTNESS
    std::cerr << "distance " << max_distance << std::endl;
    #endif
    PPL::restore_pre_PPL_rounding();

    return 1/(1 + max_distance);
}


int
main(int argc, char *argv[])
{
    double population[DIMENSION];
    std::default_random_engine generator;
    if (SEED > 0) {
        generator.seed(SEED);
    } else {
        generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    }
    std::normal_distribution<double> distribution[DIMENSION];
    // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
    double robustness_mean = 0, new_value;
    // double robustness_ssd = 0, delta, delta2, relative_robustness_error;
    gsl_solver_config config;
    gsl_solver solver;
    int loc_samples;

    MPI_Init(NULL, NULL);
    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    #ifdef DEBUG_ROBUSTNESS
    fprintf(stderr, "proc %d of %d\n", world_rank, world_size);
    #endif

    // Different seed on each processor
    generator.seed(SEED*(world_rank + 1));
    init_gsl_solver_config(&config);
    init_gsl_solver(&solver, &config);
    for (int i = 0; i < DIMENSION; i++) {
        const struct search_parameter *param = &search_parameters[i];
        #ifdef DEBUG_ROBUSTNESS
        fprintf(stderr, "param %d init %g\n", i, param->init);
        #endif
        distribution[i] = std::normal_distribution<double>(param->init, COEFF_VAR*param->init);
    }
    if (world_rank == 0) {
        loc_samples = (int) SAMPLES / world_size + SAMPLES % world_size;
    } else {
        loc_samples = (int) SAMPLES / world_size;
    }
    #ifdef DEBUG_ROBUSTNESS
    fprintf(stderr, "proc %d handling %d samples\n", world_rank, loc_samples);
    #endif
    new_value = 0;
    for (int j = 0; j < loc_samples; ++j) {
        for (int i = 0; i < DIMENSION; i++) {
            population[i] = distribution[i](generator);
	    if (population[i]<0 && POSITIVE_SAMPLING == 1) {
	      population[i] = - population[i];
	    };
        }
        new_value += satisfaction_degree(&solver, &config, population);
        // NOTE removed in favor of MPI parallel code
        // delta = new_value - robustness_mean;
        // robustness_mean += delta/(j+1);
        // delta2 = new_value - robustness_mean;
        // robustness_ssd += delta*delta2;
        // https://en.wikipedia.org/wiki/Monte_Carlo_integration
        // https://en.wikipedia.org/wiki/Variance#Population_variance_and_sample_variance
        // relative_robustness_error = sqrt(robustness_ssd)/((j+1)*robustness_mean);
        // fprintf(stderr, "%d: %g %g\n", j, robustness_mean, relative_robustness_error);
        // if (j > SAMPLES*RELATIVE_ROBUSTNESS_ERROR &&
        //         relative_robustness_error < RELATIVE_ROBUSTNESS_ERROR) {
        //     break;
        // }
    }

    #ifdef DEBUG_ROBUSTNESS
    fprintf(stderr, "proc %d total robustness %f\n", world_rank, new_value);
    #endif
    MPI_Reduce(&new_value, &robustness_mean, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    if (world_rank == 0) {
        #ifdef DEBUG_ROBUSTNESS
        fprintf(stderr, "proc %d sum of robustness %f\n", world_rank, robustness_mean);
        #endif
        robustness_mean = robustness_mean / SAMPLES;
        printf("%f\n", robustness_mean);
    }
    free_gsl_solver(&solver);

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();

    return EXIT_SUCCESS;
}
