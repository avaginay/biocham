:- module(
  sat,
  [
    run_sat/2,
    run_sat/3,
    run_sat_iteratively/2,
    write_sat_header/1,
    sat_vars/1,
    sat_comment/1
  ]
).


:- dynamic(solution/1).

%! run_sat_iteratively(+File, -Solutions)
%
% call a SAT solver repeatedly on file File. If a solution is found, add its
% negation to file and repeat. Store all solutions in solution/1
%
% @arg File       Name of the input file
% @arg Solutions  List of all solutions found
run_sat_iteratively(File, Solutions) :-
  retractall(solution(_)),
  open(File, update, Header),
  write_sat_header(Header),
  close(Header),
  (
    repeat,
    run_sat(File, Solution),
    (
      Solution = []
    ->
      debug(sat, "done", []),
      findall(
        Sol,
        solution(Sol),
        Solutions
      ),
      !
    ;
      debug(sat, "found ~w restart", [Solution]),
      assertz(solution(Solution)),
      add_solution(File, Solution),
      fail
    )
  ).


%! add_solution(+File, +Solution)
%
% negate the solution Solution to add it as new clause in DIMACS file File
%
% @arg File     Name of the DIMACS CNF file
% @arg Solution Solution as a list of positive/negative integers
add_solution(File, Solution) :-
  open(File, update, Header),
  write_sat_header(Header),
  close(Header),
  open(File, append, Stream),
  maplist(invert, Solution, NegSolution),
  atomic_list_concat(NegSolution, ' ', NegatedAtom),
  format(Stream, "~a 0~n", [NegatedAtom]),
  close(Stream).


%! run_sat(+File, -Solution)
%
% call a SAT solver on input file File and return a solution as list of
% positive/negative integers, or [] if unsatisfiable
%
% @arg File     Name of the cnf file in DIMACS format
% @arg Solution List of positive/negative integers for literals, or []
run_sat(File, Solution) :-
  run_sat(File, no, Line),
  (
    Line = []
  ->
    throw(error(process_error('glucose.py', 'Unknown error')))
  ;
    Line = ['UNSAT']
  ->
    Solution = []
  ;
    Line = [LLine],
    split_string(LLine, " \n", " \n", CleanStrings),
    append(RealClean, ["0"], CleanStrings),
    maplist(number_string, Solution, RealClean)
  ).


%! run_sat(-FileIn, -Maximize, +Solutions)
%
% call a SAT or MAXSAT solver (if Maximize is anything else than 'no',
% like min_bottom or max_bottom). return a list of lines.
run_sat(FileIn, Maximize, Solutions) :-
  statistics(walltime, [_ | [_]]),
  (
    Maximize = no
  ->
    call_sat(FileIn, Solutions)
  ;
    call_maxsat(FileIn, Solutions)
  ),
  debug(sepi, "Output: ~w", [Solutions]),
  statistics(walltime, [_ | [ExecutionTime]]),
  get_option(stats,Stats),
  (
    Stats = yes
  ->
    format('SAT solver time: ~w ms~n', [ExecutionTime])
  ;
    true
  ).


call_maxsat(FileIn, Sepis):-
  debug(sepi, "running maxsatsolver", []),
  process_create(
    path('rc2.py'),
    ['-vv', FileIn],
    [process(Id), stderr(null), stdout(pipe(Stream))]
  ),
  (
    wait_timeout_option(Id)
  ->
    read_lines(Stream, Sepis),
    close(Stream)
  ;
    Sepis=[]
  ).



call_sat(FileIn, Sepis):-
  debug(sepi, "running satsolver", []),
  tmp_file_stream(text, FileOut, StreamFile),
  close(StreamFile),
  process_create(
    path('glucose.py'),
    [FileIn, FileOut],
    [process(Id), stderr(null), stdout(null)]
  ),
  (
    wait_timeout_option(Id)
  ->
    list_file_lines(FileOut, Sepis)
  ;
    Sepis=[]
  ).

:- dynamic(sat_comment/1).
:- dynamic(sat_vars/1).

%! write_sat_header(+Stream)
%
% write to stream Stream a SAT cnf header with vars number stored in sat_vars/1
% comment stored in sat_comment/1 and clause number in counter sat_clauses
write_sat_header(Stream) :-
  sat_comment(Comment),
  count(sat_clauses, Clauses),
  sat_vars(Vars),
  debug(sepi, "c ~w~np cnf ~d ~d~n", [Comment, Vars, Clauses]),
  format(Stream, "c ~w~np cnf ~d ~d~n", [Comment, Vars, Clauses]).


%%  read_lines for rc2.py/qqmaxsat
read_lines(Stream, Output) :-
  read_line_to_codes(Stream, Codes),
  read_lines(Codes, Stream, Solutions, Optimum),
  append(Solutions, [Optimum], Output).

%!  read_lines(+Codes, +Stream, -Solutions, -Optimum)
%
% Case 1: Codes is end_of_file
%   -> instantiate Solutions to empty list
% Case 2: Line start with "o"
%   means it's the optimum
%   -> instantiate optimum
% Case 4: Line start with "v "
%   means it's a solution
%   -> add solution to the list and continue reading
% Case 5: none of the above
%   -> continue reading*/
%
%  @arg Codes     to read lines
%  @arg Stream    output of rc2 sat solver
%  @arg Solutions list of sepis
%  @arg Optimum   optimum found
read_lines(end_of_file, _, [], _) :- !.

read_lines(Codes, Stream, Solutions, Optimum) :-
  atom_codes(Line, Codes),
  atomic_list_concat(['o', COptimum | _], ' ', Line),
  !,
  atom_string(COptimum, CurrentOptimum),
  read_line_to_codes(Stream, NewCodes),
  read_lines(NewCodes, Stream, Solutions, SubOptimum),
  (
    var(SubOptimum)
  ->
    Optimum = CurrentOptimum
  ;
    Optimum = SubOptimum
  ).

read_lines(Codes, Stream, [Output], Optimum) :-
  atom_codes(Line, Codes),
  atom_concat('v ', Sepi, Line),
  !,
  read_line_to_codes(Stream, NewCodes),
  read_lines(NewCodes, Stream, Solutions, Optimum),
  (
    Solutions=[]
  ->
    Output = Sepi
  ;
    nth0(0, Solutions, First),
    string_concat(Sepi, First, Output)
  ).

read_lines(_, Stream, Solutions, Optimum) :-
  read_line_to_codes(Stream, NewCodes),
  read_lines(NewCodes, Stream, Solutions, Optimum).
