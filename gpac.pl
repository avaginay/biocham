/** <gpac> Tools to generate set of chemical reactions

The two main functions to be exported are compile_from_expression and
compile_from_pivp that generates set of equations starting from a
mathematical expression or a PIVP define here as a tuple like object.

Three main type of objects are manipulated here:

expression - mathematical description of function
example : cos

pivp_string - tuple like description of a set of ODE (corresponding to a
chemical reaction set)
example : ((1,(d(y)/dt=z)); (0,(d(z)/dt= (-1*y))))

pivp_list - a list description of the PIVP used for internal computation
in the format [N_species,[reactions_s1,reactions_s2,...],Initial_concentration]
where reactions_s1 is a list of lists of the form
[k, [exponent s1,..., exponent sn]]
example : [2, [ [[1, [0, 1]]] , [[-1, [1, 0]]] ], [1, 0]]

Rq: The distinction between pivp_string and pivp_list is not always obvious.

@author Guillaume Le Guludec
@author M. Hemery
@license GNU-GPL v.2
*/

:- module(
  gpac,
  [
    %Commands
    compile_from_expression/2,
    compile_from_expression/3,
    compile_from_pivp/2,
    compile_from_pivp/3,
    compile_from_ode/2,
    %Grammar
    polynomial/1,
    monomial/1,
    pode/1,
    pivp/1,
    %API
    format_pivp/3
  ]).

:- use_module(library(clpfd)).
:- use_module(doc).
:- use_module(quadratic_reduction).
:- use_module(biocham).
:- use_module(lazy_negation_gpac).
:- use_module(objects).
:- use_module(reaction_rules).
:- use_module(util).

:- doc('The Turing completeness of continuous CRNs \\cite{FLBP17cmsb} states that any computable function over the reals can be computed by a CRN over a finite set of molecular species. Biocham uses the proof of that result to compile any computable real function presented as the solution of a polynomial differential equation system with polynomial initial values  (PIVP) into a finite CRN.').

:- doc('The compilation from mathematical expression is restricted to some standard functions and simple operations using a functional notation where \\texttt{id} represents the operand.').

:- doc('The compilation from PIVPs is implemented with full generality.').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Main Tools of the module %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- doc('The option for quadratic reduction restricts the synthesis to reactions with at
 most two reactants (the default is not, since this process may be costly).
 Two methods are available to perform this a posteriori reduction: natively
 or using an external SAT solver.').

:- initial(option(quadratic_reduction: fastnSAT)).

:- initial(option(quadratic_bound: carothers)).

:- doc('Another option is the lazy introduction of molecular species for negative real values (the default is yes).').

:- initial(option(lazy_negatives: yes)).

:- initial(option(negation_first: yes)).

%! compile_from_expression(+Expr, +Output)
%
% biocham command
%
% Creates a biochemical reaction network such that Output = Expr(t)
%
% Typical usage will be:
% ==
% :- compile_from_expression(4 + id*id, output).
% ==

compile_from_expression(Expr, Output) :-
  biocham_command,
  type(Expr, arithmetic_expression),
  type(Output, name),
  doc('
    creates a biochemical reaction network such that Output = Expr(time).
   The expression is restricted to standard functions and simple operations using a functional notation where \\texttt{id} represents the time operand.
  '),
  option(
    quadratic_reduction,
    reduction_methods,
    _Reduction,
    'Determine if the quadratic reduction for synthesizing reactions with at most two reactants has to be performed'
  ),
  option(
    quadratic_bound,
    reduction_bounds,
    _Bound,
    'Use the bound from Carother''s article or that from Bychkov and Pogudin'
  ),
  option(
    lazy_negatives,
    yesno,
    _Lazyness,
    'Switch between systematic and lazy introduction of molecular species for negative values'
  ),
  option(
    negation_first,
    yesno,
    _Negation_first,
    'Determine if the negation of variables is perform before or after the quadratization'
  ),
  compile_from_expression(Expr, time, Output).


%! compile_from_expression(+Expr, +Input, +Output)
%
% biocham command
%
% Creates a biochemical reaction network such that Output = Expr(Input)
%
% Typical usage will be:
% ==
% :- compile_from_expression(4 + id*id, Input, Output).
% ==

compile_from_expression(Expr, Input, Output) :-
  biocham_command,
  type(Expr, arithmetic_expression),
  type(Input, name),
  type(Output, name),
  doc('
  creates a biochemical reaction network to compute the function Output = Expr(Input).
  The expression is restricted to standard functions and simple operations using a functional notation where \\texttt{id} represents the operand.
  '),
  doc('
    The Input species is initialized with the value of a special parameter named input. This species may be degraded by the computation.
  '),
  option(
    quadratic_reduction,
    reduction_methods,
    _Reduction,
    'Determine if the quadratic reduction for synthesizing reactions with at most two reactants has to be performed'
  ),
  option(
    quadratic_bound,
    reduction_bounds,
    _Bound,
    'Use the bound from Carother''s article or that from Bychkov and Pogudin'
  ),
  option(
    lazy_negatives,
    yesno,
    _Lazyness,
    'Switch between systematic and lazy introduction of molecular species for negative values'
  ),
  expression_to_PIVP(Expr, PIVP),
  PIVP = [N, _PODE, _IC],
  abstract_names(N, Output, Name_list),
  reduction:determine_keep_list([Output], Name_list, Keep_list),
  main_compiler(PIVP, Name_list, Keep_list, Input).

:- doc('
  \\begin{example} Compilation of the expression 4+time^2:\n
').
:- biocham(compile_from_expression(4 + id*id, output)).
:- biocham(list_model).
:- biocham(numerical_simulation(time:4)). %, method:msbdf)).
:- biocham(plot).
:- doc('
  \\end{example}
  ').

:- doc('
  \\begin{example} Compilation of the expression cos(time):\n
').
:- biocham(compile_from_expression(cos, costime)).
:- biocham(list_model).
:- biocham(numerical_simulation(time:10)). %, method:msbdf)).
:- biocham(plot).
:- doc('
  \\end{example}
').


:- doc('
  \\begin{example} Compilation of the expression cos(time):\n
').
:- biocham(compile_from_expression(cos+1, costime1)).
:- biocham(list_model).
:- biocham(numerical_simulation(time:10)). %, method:msbdf)).
:- biocham(plot).
:- doc('
  \\end{example}
').


:- doc('
  \\begin{example} Compilation of the expression cos(x) and computation of cos(4):\n
').
:- biocham(compile_from_expression(cos, x, cosx)).
:- biocham(list_model).
:- biocham(parameter(input = 4)).
:- biocham(numerical_simulation(time:10)). %, method:msbdf)).
:- biocham(plot).
:- doc('
  \\end{example}
').

:- devdoc('\\begin{todo} share common subexpression\\end{todo}').

:- doc('One can also compile real valued functions defined as solutions of Polynomial Initial Value Problems (PIVP), i.e. solutions of polynomial differential equations with initial values defined by polynomials of the input. PIVPs are written with the following grammar:').


% Definition of the various grammars used in this module:
% pivp, pode, polynomial and monomial
% the tagging ':- grammar(_)' is here for the documentation


%! pivp(+Expression)
%
% Test if the expression correspond to a valid PIVP_string

:- grammar(pivp).

pivp((X;Y)):-
  pivp(X),
  pivp(Y).

pivp((X,Y)):-
  number(X),
  pode(Y).


%! pode(+Expression)
%
% Test if the expression correspond to a valid pODE

:- grammar(pode).

pode((d(X)/dt = Y)):-
  name(X),
  polynomial(Y).


%! polynomial(+Expression)
%
% Test if the expression correspond to a valid polynomial

:- grammar(polynomial).

polynomial(+ X):-
  polynomial(X).

polynomial(- X):-
  polynomial(X).

polynomial(X + Y):-
  polynomial(X),
  polynomial(Y).

polynomial(X - Y):-
  polynomial(X),
  monomial(Y).

polynomial(X):-
  monomial(X).


%! monomial(+Expression)
%
% Test if the expression correspond to a valid monomial

:- grammar(monomial).

monomial(X ^ Y):-
  name(X),
  number(Y).

monomial(X * Y):-
  monomial(X),
  monomial(Y).

monomial(X) :-
  number(X).

monomial(X) :-
  name(X).


%! compile_from_pivp(+PIVP_string, +Output)
%
% biocham command
%
% Creates a biochemical reaction network that computes the projection of multivariate function f(t) on one variable Output,
% where f(t) is the solution of the PIVP.
%
% Typical usage will be:
% ==
% :- compile_from_pivp( ((1,(d(y)/dt=z)); (0,(d(z)/dt= (-1*y)))), y).
% ==

compile_from_pivp(PIVP, Output) :-
  biocham_command,
  type(PIVP, pivp),
  type(Output, '*'(name)),
  doc('
    creates a CRN that immplements a function of time, specified by the variable "Output" of the solution of the PIVP.
  '),
  option(
    quadratic_reduction,
    reduction_methods,
    _Reduction,
    'Determines if the quadratic reduction has to be performed'
  ),
  option(
    quadratic_bound,
    reduction_bounds,
    _Bound,
    'Use the bound from Carother''s article or that from Bychkov and Pogudin'
  ),
  compile_from_pivp(PIVP, time, Output).

%:- doc('
%  \\begin{example} Compilation of a simple oscillator with 2 species (Lotka-Volterra)\n
%').
%%:- biocham(option(lazy_negatives:yes)).
%:- biocham(compile_from_pivp((0.5,(d(x)/dt= x - x*y));(0.25,(d(y)/dt= x*y - 0.5*y)), [x])).
%:- biocham(list_model).
%:- biocham(numerical_simulation(time:10)). % method:msbdf
%:- biocham(plot(show:{'x','y'})).
%:- doc('
%  \\end{example}
%      ').


:- doc('
  \\begin{example} Compilation of the Hill function of order 2 as a function of time, $h(t)=t^2/(1+t^2)$ \n
').
%:- biocham(option(lazy_negatives:yes)).
:- biocham(option(quadratic_reduction:native)).
:- biocham(compile_from_pivp((0.0,d(h)/dt= 2*n^2*t;1.0,d(n)/dt= -2*n^2*t;0.0,d(t)/dt=1),[h])).
:- biocham(list_model).
:- biocham(numerical_simulation(time:10)). % method:msbdf
:- biocham(plot(show:{h})).
:- biocham(plot(show:{h}, logscale:x)).
:- doc('
  \\end{example}
').


%! compile_from_pivp(+PIVP_string, +Input , +Output)
%
% biocham command
%
% Creates a biochemical reaction network that computes the projection of multivariate function f(Input) on one variable Output,
% where f(t) is the solution of the PIVP, and Input is a time value
%
% Typical usage will be:
% ==
% :- compile_from_pivp( ((1,(d(y)/dt=z)); (0,(d(z)/dt= (-1*y)))), 4, y).
% ==

compile_from_pivp(PIVP, Input, Output) :-
  biocham_command,
  type(PIVP, pivp),
  type(Input, name), % FF   type(Input, expr),
  type(Output, '*'(name)),
  doc('
  creates a CRN that implements a function of the variable "Input". The function is specified by the value of the variable "Output" of the solution of a PIVP at time "Input".
     The "Input" variable must not appear in the PIVP. It is created and  initialized with a parameter named "input".
  '),
  option(
    quadratic_reduction,
    reduction_methods,
    _Reduction,
    'Determines if the quadratic reduction has to be performed'
  ),
  option(
    quadratic_bound,
    reduction_bounds,
    _Bound,
    'Use the bound from Carother''s article or that from Bychkov and Pogudin'
  ),
  format_pivp(PIVP, P, Name_list),
  reduction:determine_keep_list(Output, Name_list, Output_list),
  debug(compilation, "Now we generate the CRN", []),
  main_compiler(P, Name_list, Output_list, Input).


%! compile_from_ode(+Input, +Output)
%
% Same as compile_from_pivp but load ODE from the current ode system

compile_from_ode(Input, Output) :-
  biocham_command,
  type(Input, name),
  type(Output, '*'(name)),
  doc('Compile the current ODE system in a CRN, Input is usually "time" but could be the name of a new species, Output have to be the name of an existing variable.'),
  option(
    quadratic_reduction,
    reduction_methods,
    _Reduction,
    'Determines if the quadratic reduction has to be performed'
  ),
  option(
    quadratic_bound,
    reduction_bounds,
    _Bound,
    'Use the bound from Carother''s article or that from Bychkov and Pogudin'
  ),
  option(
    lazy_negatives,
    yesno,
    _Lazyness,
    'Switch between systematic and lazy introduction of molecular species for negative values'
  ),
  (
    % test the polynomiality of the current ode_system
    with_current_ode_system(
      forall(ode(_Var, Expr),is_polynomial(Expr))
    )
  ->
    true
  ;
    format("The current ODE system is not polynomial.~n", []),
    fail
  ),
  debug(compilation, "ODE is effectively poynomial", []),
  read_ode_system(PIVP),
  debug(compilation, "PIVP: ~w", [PIVP]),
  compile_from_pivp(PIVP, Input, Output).



:- doc('
  \\begin{example} Compilation of the Hill function of order 2 as a function of an input $h(x)=x^2/(1+x^2)$.
  This time \\texttt{sat_species} is used (minimizing the number of variables as is the case for \\texttt{native}) and then
  \\texttt{sat_reactions} that minimizes the number of monomes.
').
%:- biocham(option(lazy_negatives:yes)).
:- biocham(option(quadratic_reduction:sat_species)).
:- biocham(compile_from_pivp((0.0,d(h)/dt= 2*n^2*t;1.0,d(n)/dt= -2*n^2*t;0.0,d(t)/dt=1),x,[h])).
:- biocham(parameter(input=2)).
:- biocham(list_model).
:- biocham(numerical_simulation(time:10)). % method:msbdf
:- biocham(plot(show:{h})).
:- biocham(plot(show:{h}, logscale:x)).
:- biocham(compile_from_pivp((0.0,d(h)/dt= 2*n^2*t;1.0,d(n)/dt= -2*n^2*t;0.0,d(t)/dt=1),x,[h], quadratic_reduction: sat_reactions)).
:- biocham(list_model).
:- doc('
  \\end{example}
').



:- devdoc('
  The internal data structure for PIVPs (Polynomial differential equations
  Initial Value Problems) is
  \\texttt{[ max(1,dimension), [ differential equations ] , [initial values] ]}

  The differential equations are represented by the list of lists (sums) of
  monomials (list of exponents) for each variable.

  \texttt{[ [ [coeff, [exponent x1,..., exponent xn]], ... other monomials for
  x1 differential... ], ... other differential functions for xi differential... ]}
').


%! main_compiler(+PIVP_list, +Name_list, +Output_list, +Input)
%
% Compile a PIVP_list, this predicate dispatch the various options of compilation
% it first check if the PIVP should be reduce to a quadratic form
% then implement a lazy negation if asked
% when Input is not time it then call g_to_c_PIVP/3.
% and finally make a call to compile_to_biocham_model to make the final implementation

main_compiler(PIVP_raw, Name_list_raw, Output1, Input):-
   get_option(negation_first, NegFirst),
   copy_parameters,
   clear_model,
   paste_parameters,
   % convert PIVP to function of input if necessary
   (
      Input = time
   ->
      PIVP_input = PIVP_raw,
      Name_list_input = Name_list_raw,
      Output2 = Output1
   ;
      g_to_c_PIVP(PIVP_raw, PIVP_input, 1.0),
      append(Name_list_raw, [Input], Name_list_input),
      maplist([L,L0]>>append(L, [0], L0), Output1, Output2)
   ),
   ( % determine the order for quadratization/negation
     NegFirst = yes
  ->
     negation_step(PIVP_input, Name_list_input, Output2, PIVP_tempo, Name_list_tempo, Keep),
     quadratization_step(PIVP_tempo, Name_list_tempo, Keep, PIVP_final, Name_list_final)
   ;
     quadratization_step(PIVP_input, Name_list_input, Output2, PIVP_tempo, Name_list_tempo),
     negation_step(PIVP_tempo, Name_list_tempo, [], PIVP_final, Name_list_final, _Empty)
   ),
   !, % avoid strangebacktrack effect while loading file
   compile_to_biocham_model(PIVP_final, Name_list_final),
   % FIXME several results above???
   !.

negation_step(PIVP_input, Names_input, Keep_input, PIVP_output, Names_output, Keep_output) :-
   get_option(fast_rate, Fast),
   get_option(lazy_negatives, Lazyness),
   (
      Lazyness = yes
   ->
      lng:rewrite_PIVP(PIVP_input, PIVP_output, VarNeg)
   ;
      lng:rewrite_PIVP_all_negated(PIVP_input, PIVP_output, VarNeg)
   ),
   sort(VarNeg, VarNegSort),
   negate_name(Names_input, VarNegSort, Names_output),
   negate_keep(Keep_input, Names_input, Names_output, Keep_output),
   (
      \+(VarNeg = [])
   ->
      set_parameter(fast, Fast)
   ;
      true
   ).

quadratization_step(PIVP_input, Names_input, Keep, PIVP_output, Names_output) :-
   get_option(quadratic_reduction, Reduction),
   (
      Reduction \= no
   ->
      reduction:list_reduction(PIVP_input, Names_input, Keep, PIVP_output, Names_output)
   ;
      PIVP_output = PIVP_input,
      Names_output = Names_input
   ).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Tools to format a PIVP_string to a PIVP_list %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! format_pivp(+PIVP_string, -PIVP_list, -Name_list)
%
% Generates the PIVP_list object and the Name_list corresponding to a given PIVP_string

format_pivp(P, [N, Pode, Init], Name_list):-
   find_variables(P, L, Init),
   length(L, N), % should put 1 if no variable ?
   parse_pivp(P, L, Pode),
   extract_names(P, Name_list).

:- devdoc('
  \\command{format_pivp/3} encodes a PIVP syntax tree into the internal list
  representation for PIVPs.
').


%! find_variables(+PIVP_string, -ListVariableNames, -InitialConcentrations)
%
% Scan a PIVP_string to extract the variables and initial concentrations.

find_variables((I, (d(X)/dt=_)), [X], [I]).

find_variables((X;Y), L, Init):-
   find_variables(X, L1, I1),
   find_variables(Y, L2, I2),
   append(L1, L2, L),
   append(I1, I2, Init).


%! sort_output(+PIVP, +Output, -PIVP_sorted)
%
% Ensure that Output is the first variable of the PIVP (here as a string)

sort_output((IC, d(Out)/dt = Deriv), Out, (IC, d(Out)/dt = Deriv)) :- !.

sort_output((IC, d(Out)/dt = Deriv);Remainder, Out, (IC, d(Out)/dt = Deriv);Remainder) :- !.

sort_output(Rmd;(IC, d(Out)/dt = Deriv), Out, (IC, d(Out)/dt = Deriv);Rmd) :- !.

sort_output((ICt, d(Vt)/dt = Dt);Rt, Out, (IC, d(Out)/dt = Deriv);(ICt, d(Vt)/dt = Dt);Rtt) :-
  !,
  sort_output(Rt, Out, (IC, d(Out)/dt = Deriv);Rtt).

sort_output(_PIVP1, _Out, _PIVP2) :-
  print_message(warning, 'Output should be a variable of the PIVP'),
  fail.


%! parse_pivp(+PIVP_string, +ListVariableNames, -Pode)
%
% Giving the PIVP_string and its variable, extract the list representation
% of the polynomials associated with PIVP_list

parse_pivp((X;Y), L, Pode):-
   parse_pivp(X, L, P1),
   parse_pivp(Y, L, P2),
   append(P1, P2, Pode).

parse_pivp((_,(d(_)/dt=P)), L, [Pol]):-
   parse_polynomial_frontdoor(P, L, Polr),
   reverse(Polr, Pol).


%! parse_polynomial(+Polynomial_string, +ListVariableNames, -Polynomial_list)
%
% Scan the right hand side of the PIVP_string representation (a polynomial)
% to produce the list representation used by PIVP_list
% Work by cutting the polynomial in the different monomial and delegates to
% parse_monomial/3.

parse_polynomial_frontdoor(Poly, L, Output) :-
  distribute(Poly, DisPoly),
  simplify(DisPoly, SimPoly),
  parse_polynomial(SimPoly, L, Output).

parse_polynomial(Poly+Mono, L, [[R,E]|Tail]):-
  parse_monomial(Mono, L, [R, E]),
  !,
  parse_polynomial(Poly, L, Tail).

parse_polynomial(Poly-Mono, L, [[-R,E]|Tail]):-
  parse_monomial(Mono, L, [R, E]),
  !,
  parse_polynomial(Poly, L, Tail).

parse_polynomial(+X, L, [[R, E]]) :-
  !,
  parse_monomial(X, L, [R, E]).

parse_polynomial(-X, L, [[-R, E]]) :-
  !,
  parse_monomial(X, L, [R, E]).

parse_polynomial(X, L, [[R, E]]) :-
  !,
  parse_monomial(X, L, [R, E]).

%! parse_monomial(+Monomial_string, +ListVariableNames, -Monomial_list)
%
% Scan a Monomial_string representation to produce the list representation

parse_monomial(Var, L, [1, Expo]) :-
  nth1(Index, L, Var),
  !,
  length(L, N),
  Left is Index-1,
  Right is N-Index,
  displace_exponent([1], Left, Right, Expo).

parse_monomial(Var^Pow, L, [1, Expo]) :-
  nth1(Index, L, Var),
  !,
  length(L, N),
  Left is Index-1,
  Right is N-Index,
  displace_exponent([Pow], Left, Right, Expo).

parse_monomial(Num, L, [Num, Expo]) :-
  is_numeric(Num),
  !,
  length(L, N),
  displace_exponent([], N, 0, Expo).

parse_monomial(M1*M2, L, [Rate12, Expo12]) :-
  parse_monomial(M1, L, [Rate1, Expo1]),
  parse_monomial(M2, L, [Rate2, Expo2]),
  (
    Rate1 = 1
  ->
    Rate12 = Rate2
  ;
    Rate2 = 1
  ->
    Rate12 = Rate1
  ;
    Rate12 = Rate1*Rate2
  ),
  add_list(Expo1, Expo2, Expo12).

parse_monomial(M1/Num, L, [Rate/Num, Expo]) :-
  is_numeric(Num),
  parse_monomial(M1, L, [Rate, Expo]).

parse_monomial_deviation(Mono, L, [Rate, Expo]) :-
  separate_monomial(Mono, L, Nom, Den, Var),
  make_exponent(Var, L, Expo),
  make_rate(Nom, Den, Rate).

separate_monomial(X*Y, L, Nom, Den, [Y^1|Var]) :-
  member(Y, L),!,
  separate_monomial(X, L, Nom, Den, Var).

separate_monomial(X*Y^N, L, Nom, Den, [Y^N|Var]) :-
  member(Y, L),!,
  separate_monomial(X, L, Nom, Den, Var).

separate_monomial(X*Y, L, [Y|Nom], Den, Var) :-
  !,
  separate_monomial(X, L, Nom, Den, Var).

separate_monomial(X/D, L, Nom, [D|Den], Var) :-
  !,
  separate_monomial(X, L, Nom, Den, Var).

separate_monomial(X, L, [], [], [X^1]) :-
  member(X,L),!.

separate_monomial(X^N, L, [], [], [X^N]) :-
  member(X,L),!.

separate_monomial(X, _L, [X], [], []) :- !.


%! read_ode_system(-PIVP)
%
% read the current ode_system and return it as a formated PIVP_string

read_ode_system(PIVP) :-
  with_current_ode_system((
    get_current_ode_system(Id),
    findall(
      Stuff,
      (
        item([parent: Id, kind: ode, item: Item_Der]),
        Item_Der = d( Var )/dt = _Deriv,
        (
          item([parent: Id, kind: initial_concentration, item: Item_Init]),
          Item_Init = init(Var = Num)
        ->
          Stuff = (Num, Item_Der)
        ;
          Stuff = (0.0, Item_Der)
        )
      ),
      PIVP_unf
    )
  )),
  derivative_list_to_pivp(PIVP_unf, PIVP).


% derivative_list_to_pivp(+List, -PIVP)

derivative_list_to_pivp([Head|[]], Head) :- !.
derivative_list_to_pivp([Head|Tail], Head;MTail) :-
  !,
  derivative_list_to_pivp(Tail, MTail).


%! make_exponent(+Var, +List_var, -Expo)
%
% from the variable present in a monomial, construct the exponent
% make a sub call to count_exponent

make_exponent(Var, [V|Tail], [E|ETail]) :-
  !,
  count_exponent(Var, V, E),
  make_exponent(Var, Tail, ETail).

make_exponent(_Var, [], []).

count_exponent([V^N|Tail], V, Enew) :-
  !,
  count_exponent(Tail, V, E),
  Enew is E+N.

count_exponent([_W^_N|Tail], V, E) :-
  !,
  count_exponent(Tail, V, E).

count_exponent([],_V,0).


%! make_rate(+Nom, +Den, -Rate)
%
%

make_rate([], [], 1) :- !.

make_rate(Nom, [], R) :-
  !,
  foisld(Nom, R).

make_rate([], Den, 1/R) :-
  !,
  foisld(Den, R).

make_rate(Nom, Den, N/D) :-
  !,
  foisld(Nom, N),
  foisld(Den, D).

fois(A,B,A*B).
foisld([Head|Tail], T) :-
	foldl(fois, Tail, Head, T).


%! extract_names(+PIVP_string, -Name_list)
%
% Extract the names of the variables of the PIVP_string

extract_names((_IC, d(Name)/dt = _Deriv), [Name]) :- !.

extract_names((_IC, d(Name)/dt = _Deriv; Remainder), [Name|Tail]) :-
  extract_names(Remainder, Tail).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Tools to manipulate PIVP_list internal objects %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% A multivar is a list of monomial
% A monomial is a unique object of the form: [K,[Exp1,Exp2,Exp3]]
% corresponding to the term K v1^Exp1 V2^Exp2 etc.

:- dynamic(input_name/1).
:- dynamic(output_name/1).
:- dynamic(name_spec/0).

%! compare_exponants(+ExponentSet1,+ExponentSet2,-Comparator)
%
% Compare ExponentSet1 to ExponentSet2 (in this order)
% Comparator will be g (greater), l (lower) or e (equal).
% Note that the ordering is done with respect to the firsts not equal
% coefficients so that:
% ==
% ?- compare_exponants([1,1,0],[1,0,9],g).
% true.
% ==
% It thus provides a complete order on the set of exponent sets.

compare_exponants([A1], [A2], C) :-
  (
    A1 < A2
  ->
    C = l
  ;
    A1 =:= A2
  ->
    C = e
  ;
    A1 > A2
  ->
    C = g
  ),
  !.

compare_exponants([A1|T1], [A2|T2], C) :-
  compare_exponants([A1], [A2], K),
  !,
  (
    K = e
  ->
    compare_exponants(T1, T2, C)
  ;
    C = K
  ).

%! add_multivar(+ReactionSet1,+ReactionSet2,-ReactionSetSum)
%
% Add two sets of reactions so that terms with same exponents are added

add_multivar([], ReactionSet, ReactionSet) :- !.

add_multivar(ReactionSet, [], ReactionSet) :- !.

add_multivar([[K1, ExpList1]|T1], [[K2, ExpList2]|T2], ReactionSum) :-
   compare_exponants(ExpList1, ExpList2, C),
   (
      C = l
   ->
      add_multivar(T1, [[K2, ExpList2]|T2], T),
      ReactionSum = [[K1, ExpList1]|T]
   ;
      C = e
   ->
      add_multivar(T1, T2, T),
      catch(K is K1 + K2,
            error(type_error(_,_),_),
            K = +(K1,K2)),
      (
         catch(K =\= 0, error(type_error(_,_),_), true)
      ->
         ReactionSum = [[K, ExpList1]|T]
      ;
         ReactionSum = T
      )
   ;
      C = g
   ->
      add_multivar([[K1, ExpList1]|T1], T2, T),
      ReactionSum = [[K2, ExpList2]|T]
   ).


%! multiply_multivar(+ReactionSet1,+ReactionSet2,-ReactionProduct)
%
% Multiply two sets of reactions

multiply_multivar(P, one, P) :- !.

multiply_multivar(one, P, P) :- !.

multiply_multivar(_, [], []) :- !.

multiply_multivar(Polynom, [Monom|Tail], ReactionProduct) :-
   multiply_multivar_monomial(Polynom, Monom, Polynom_times_Monom),
   multiply_multivar(Polynom, Tail, Polynom_times_Tail),
   add_multivar(Polynom_times_Monom, Polynom_times_Tail, ReactionProduct).


%! multiply_multivar_monomial(+ReactionSet1,+Reaction2,-ReactionProduct)
%
% Multiply a set of reactions by a unique monome to gives ReactionProduct

multiply_multivar_monomial([], _, []) :- !.

multiply_multivar_monomial([[K1, Exp1]|Tail], [K2, Exp2], [[K, Exp]|TailProd]):-
   catch(K is K1 * K2,
         error(type_error(_,_),_),
         K = K1*K2),
   add_list(Exp1, Exp2, Exp),
   multiply_multivar_monomial(Tail, [K2, Exp2], TailProd).


%! multiply_multivar_vector(+MultivarVector,+Multivar,-MultivarVectorResult)
%
% Multiply all the Multivar in MultivarVector by the second argument

multiply_multivar_vector([], _, []) :- !.

multiply_multivar_vector([P1|T1], P2, [P1P2|T]) :-
   multiply_multivar(P1, P2, P1P2),
   multiply_multivar_vector(T1, P2, T).


%! translate_exponant(+ExpSet,+N,-ExpSetResult)
%
% Add N '0' to the Left of ExpSet

translate_exponant(E, 0, E) :- !.

translate_exponant(E, N, [0|Ep]) :-
   Np is N - 1,
   translate_exponant(E, Np, Ep).


%! extend_exponant(+ExpSet,+N,-ExpSetResult)
%
% Add N '0' to the Right of ExpSet

extend_exponant([], N, Z) :-
   translate_exponant([], N, Z), !.

extend_exponant([A|T], N, [A|ET]) :-
   extend_exponant(T, N, ET).


%! displace_exponent(+ExpSet,+Left,+Right,-ExpSetResult)
%
% Add 0 to the Left and Right of ExpSet

displace_exponent(E, Left, Right, Ed) :-
   translate_exponant(E, Left, Et),
   extend_exponant(Et, Right, Ed).


%! displace_multivar(+Multivar,+Left,+Right,-MultivarResult)
%
% Add 0 to the Left and Right of all the Exponants of Multivar

displace_multivar([], _, _, []) :- !.

displace_multivar([[X, E]|T], Left, Right, [[X, Ed]|Td]) :-
   displace_exponent(E, Left, Right, Ed),
   displace_multivar(T, Left, Right, Td).


%! displace_multivar_vector(+MultivarVector,+Left,+Right,-MultivarVectorResult)
%
% Add 0 to the Left and Right of all the Exponants of MultivarVector

displace_multivar_vector([], _, _, []) :- !.

displace_multivar_vector([P|T], Left, Right, [Pd|Td]) :-
   displace_multivar(P, Left, Right, Pd),
   displace_multivar_vector(T, Left, Right, Td).


%! unitary(+N,List)
%
% test if List is a list of exactly N zeros.

unitary(0, []) :- !.

unitary(N, [0|Tail]) :-
   Nm is N - 1,
   unitary(Nm, Tail).


%! power_multivar(+N,+Multivar,-MultivarResult)
%
% return Multivar to the power N

power_multivar(0, [[_, Exp]|_], [[1, Result]]) :-
   length(Exp, N), !,
   unitary(N, Result).

power_multivar(N, Multivar, Result) :-
   Np is N - 1,
   power_multivar(Np, Multivar, MultivarTempo),
   multiply_multivar(Multivar, MultivarTempo, Result).

%! scalar_multivar(+N,+Multivar,-MultivarResult)
%
% return Multivar multiplied by N

scalar_multivar(0, _, []) :- !.

scalar_multivar(_, [], []) :- !.

scalar_multivar(N, [[K, Exponant]|Tail], [[NK, Exponant]|TailResult]) :-
   simplify(N*K, NK),
   scalar_multivar(N, Tail, TailResult).


%! compose_multivar_monomial(+Monomial,+MultivarVector,-MultivarResult)
%
% If Monomial is [K,[E1,E2]] and MultivarVector [M1,M2]
% MultivarResult will be K * M1^E1 * M2^E2

compose_multivar_monomial([X, E], PL, P) :-
   compose_multivar_unitary(E, PL, P0),
   scalar_multivar(X, P0, P).


%! compose_multivar_unitary(+ExponentList,+MultivarVector,-MultivarResult)
%
% If ExponentList is [E1,E2] and MultivarVector [M1,M2]
% MultivarResult will be M1^E1 * M2^E2

compose_multivar_unitary([], [], one) :- !.

compose_multivar_unitary([A|AT], [P|PT], Q) :-
   power_multivar(A, P, P0),
   compose_multivar_unitary(AT, PT, P1),
   multiply_multivar(P0, P1, Q).


%! compose_multivar(+Multivar,+MultivarVector,-MultivarResult)
%
% same as compose_multivar_monomial but with a multivar (ie. a sum of monomial)

compose_multivar([], _, []) :- !.

compose_multivar([[X, E]|T], PL, Q) :-
   compose_multivar_monomial([X, E], PL, P0),
   compose_multivar(T, PL, P1),
   add_multivar(P0, P1, Q).


%! compose_multivar_vector(+MultivarVector,+MultivarVector,-MultivarVectorResult)
%
% Composition of two MultivarVectors to gives a third one

compose_multivar_vector([], _, []) :- !.

compose_multivar_vector([P|PV], PL, [Q|QV]) :-
   compose_multivar(P, PL, Q),
   compose_multivar_vector(PV, PL, QV).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Tools to manipulate directly PIVP_list objects %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This part is intented to manipulate PIVP_list as a representation of function
% that are their first component.
% So when adding, multiplying and so on, we actually create a new PIVP that
% realize the function that correspond to the addition of the function realized
% by the PIVP given in arguments.

%! add_PIVP(+PIVP1,+PIVP2,-PIVP_Sum)
%
% add the two PIVP and create a new variable that corresponds to the sum
% of the two first variables of PIVP1 and PIVP2 and is the new first variable.

add_PIVP([D1, PV1, IV1], [D2, PV2, IV2], [D, PV, IV]) :-
   D1_p_1 is D1 + 1,
   displace_multivar_vector(PV1, 1, D2, PV1d),
   displace_multivar_vector(PV2, D1_p_1, 0, PV2d),
   nth1(1, PV1d, P1d),
   nth1(1, PV2d, P2d),
   nth1(1, IV1, I1),
   nth1(1, IV2, I2),
   add_multivar(P1d, P2d, P),
   I is I1 + I2,
   D is D1_p_1 + D2,
   append([[P], PV1d, PV2d], PV),
   append([[I], IV1, IV2], IV).

%! multiply_PIVP(+PIVP1,+PIVP2,-PIVP_Product)
%
% add the two PIVP and create a new variable that corresponds to the product
% of the two first variables of PIVP1 and PIVP2 and is the new first variable.

multiply_PIVP([D1, PV1, IV1], [D2, PV2, IV2], [D, PV, IV]) :-
   D1_p_1 is D1 + 1,
   Right1 is D1 + D2 - 1,
   Right2 is D2 - 1,
   displace_multivar_vector(PV1, 1, D2, PV1d),
   displace_multivar_vector(PV2, D1_p_1, 0, PV2d),
   nth1(1, PV1d, P1d),
   nth1(1, PV2d, P2d),
   nth1(1, IV1, I1),
   nth1(1, IV2, I2),
   displace_exponent([1], 1, Right1, E1),
   displace_exponent([1], D1_p_1, Right2, E2),
   multiply_multivar_monomial(P1d, [1, E2], Q1),
   multiply_multivar_monomial(P2d, [1, E1], Q2),
   add_multivar(Q1, Q2, Q),
   I is I1 * I2,
   D is D1_p_1 + D2,
   append([[Q], PV1d, PV2d], PV),
   append([[I], IV1, IV2], IV).

:- devcom('\\begin{todo} ^, log, compose PIVP\\end{todo}').

:- devcom('\\begin{todo}automatic simulation time horizon option for having stabilization on one component at epsilon from the middle (or two third) of the time horizon\\end{todo}').


%! invert_PIVP(+PIVP,-PIVP_Inverse)
%
% copy the PIVP and create a new variable that corresponds to the inverse
% of the first variable, it became the new first variable.

invert_PIVP([D1, PV1, IV1], [D, PV, IV]) :-
   D is D1 + 1,
   displace_multivar_vector(PV1, 1, 0, PV1d),
   nth1(1, PV1d, P1d),
   nth1(1, IV1, I1),
   displace_exponent([2], 0, D1, E1),
   multiply_multivar_monomial(P1d, [-1, E1], Q),
   I is 1 / I1,
   append([[Q], PV1d], PV),
   append([[I], IV1], IV).

%! g_to_c_PIVP(+PIVP,-PIVP_modified,+Input)
%
% Used to switch from a response to time to a response to an input
% Add a species at the tail of the PIVP and initialize to Input
% this species slow down to zero and down regulate every other reactions.

g_to_c_PIVP([D, PV, IV], [Dc, PVc, IVc], X) :-
  Dc is D + 1,
  displace_multivar_vector(PV, 0, 1, PVd),
  displace_exponent([1], D, 0, Egamma),
  multiply_multivar_vector(PVd, [[1,Egamma]], PVm),
  set_parameter(input, X),

  append([ PVm, [[[-1,Egamma]]] ], PVc),
  append([IV, [input]], IVc).


:- devcom('\\begin{todo}compilation option for not splitting the variables, assumed positive.\\end{todo}').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Compilation Tools for PIVP_list %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! compile_to_biocham_model(+PIVP, +Species_names)
%
% Compilation setup, Species_names gives the list for the different species

compile_to_biocham_model([_D, PODE, IV], Names) :-
   set_initial_vector(Names, IV),
   chemical_conversion(Names, PODE, 0).


%! set_initial_vector(+Names, +InitialValueVector)
%
% Set up the initial concentration for the simulation, see initial_state.pl

set_initial_vector([], []) :- !.

set_initial_vector([Name|Tail_name], [IV|Tail_iv]) :-
   (
      IV \= 0
   ->
      present([Name], IV)
   ;
      true
   ),
   set_initial_vector(Tail_name, Tail_iv).


%! chemical_conversion(+Names, +PODE, +Epsilon)
%
% Convert the PODE into a Chemical Reactions Network
% Start by constructing the list of [rate,reactants,products] and then write them
% tries to group together close enough monomials into a single reaction

chemical_conversion(Names, PODE, Epsilon) :-
  gather_reactions(PODE, Epsilon, Reaction_list),
  maplist(write_reaction(Names), Reaction_list).

%! gather_reactions(+PODE, +Epsilon, -Reaction_list)
%
% Goes from the PODE format to the Reaction list used by write_reaction

gather_reactions(PODE, _Epsilon, Reaction_list) :-
  convert_to_list(PODE, Reaction_list, 1).
  

%! convert_to_list(+PODE, +List, +N)
%
% deconstruct the PODE into a list, N is the offset for the current reaction
% it groups together the reaction that have exactly the same monomial

convert_to_list([], [], _N).

convert_to_list([[]|Tail], List, N) :-
  Np is N+1,
  convert_to_list(Tail, List, Np).

convert_to_list([[[R,E]|Tail1]|Tail2], List, N) :-
  catch(
    RR is float(abs(R)),
    _Caught,
    (
      is_numeric(R, Value),
      (Value>0 -> RR=R ; simplify(-R, RR))
    )
  ),
  forge_products(R, E, N, Prod),
  convert_to_list([Tail1|Tail2], List_tempo, N),
  ( % group similar monomials together
    member([RR,E,OldProd],List_tempo)
  ->
    add_list(OldProd, Prod, Prod_tempo),
    multiply_list(E,-1,Em),
    add_list(Prod_tempo,Em,NewProd),
    delete(List_tempo, [RR,E,OldProd], List_temp2),
    List = [[RR,E,NewProd]|List_temp2]
  ; % case where no same monomial exists
    List = [[RR,E,Prod]|List_tempo]
  ).


%! forge_products(+Rate, +Exponent, +N_sp, -Product)
%
% Determine the product set of the reaction correspoding to the momomial [rate, exponent]
% in the derivative of the N_sp' species

forge_products(Rate, Exponent, N_sp, Product) :-
  length(Exponent, Len),
  Left is N_sp-1,
  Right is Len-N_sp,
  (
    is_numeric(Rate, Value),
    Value > 0
  ->
    displace_exponent([1], Left, Right, Modif)
  ;
    displace_exponent([-1], Left, Right, Modif)
  ),
  add_list(Exponent, Modif, Product).


%! write_reaction(+Names, +Reaction)
%
% implement a single reaction

write_reaction(Names, [Rate, Reactants, Products]) :-
  write_species(Names, Reactants, RString),
  write_species(Names, Products, PString),
  add_reaction('MA'(Rate) for RString => PString).


%! write_species(+Names, +Species, -String)
%
% Write the name of products/reactants associated to a given integer set

write_species(Names, Species, String) :-
  (
    sum_list(Species, 0)
  ->
    String = '_'
  ;
    write_species_sr(Names, Species, String)
  ).

write_species_sr([], [], void) :- !.

write_species_sr([_Head|TailNm], [0|TailSp], String) :-
  !,
  write_species_sr(TailNm, TailSp, String).

write_species_sr([Head|TailNm], [1|TailSp], String) :-
  !,
  write_species_sr(TailNm, TailSp, Tempo),
  (
    Tempo = void
  ->
    String = Head
  ;
    String = Head+Tempo
  ).

write_species_sr([Head|TailNm], [N|TailSp], String) :-
  write_species_sr(TailNm, TailSp, Tempo),
  (
    Tempo = void
  ->
    String = N*Head
  ;
    String = N*Head+Tempo
  ).


%! negate_name(+Name_list_raw, +Varneg, -Name_list)
%
% Add _p and _m to the name of variables if necessary

negate_name(NLR, VN, NL) :-
  negate_name(NLR, 1, VN, NL).

negate_name([], _N, [], []) :- !.

negate_name([Name|TailName], N, [N|TailNeg], [Name_p,Name_m|TailNewName]) :-
  !,
  name_p_m(Name,Name_p,Name_m),
  add_reaction('MA'(fast) for Name_p+Name_m=>_),
  NN is N+1,
  negate_name(TailName, NN, TailNeg, TailNewName).

negate_name([Name|TailName], N, VarNeg, [Name|TailNewName]) :-
  !,
  NN is N+1,
  negate_name(TailName, NN, VarNeg, TailNewName).


%! name_p_m(+Name,-Positive_name,-Negative_name)
%! name_p_m(+Name1,+Name2,-Positive_name,-Negative_name)
%
% Produce names for positive and negative terms

name_p_m(X,Xp,Xm):-
   atom_concat(X,'_p',Xp),
   atom_concat(X,'_m',Xm).

name_p_m(X,I,Xp,Xm):-
   atom_concat(X,I,XI),
   name_p_m(XI,Xp,Xm).

%! negate_keep(+Keep_input, +Names_input, +Names_output, -Keep_output)
%
% Determine which of the new variables should be preserved, if x is in keep_input, we
% should have x_p and x_m in keep_output

negate_keep(Keep_input, Names_input, Names_output, Keep_output) :-
  negate_keep_sr(Keep_input, Names_input, Names_output, Tempo),
  reduction:determine_keep_list(Tempo, Names_output, Keep_output).

negate_keep_sr([], _NI, _NO, []) :- !.

negate_keep_sr([IHead|ITail], Names_input, Names_output, Output) :-
  nth1(Index, IHead, 1),!,
  nth1(Index, Names_input, Name),
  (
    member(Name, Names_output)
  ->
    Output = [Name|OTail]
  ;
    name_p_m(Name, Namep, Namem),
    Output = [Namep, Namem|OTail]
  ),
  negate_keep_sr(ITail, Names_input, Names_output, OTail).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Tools for conversion from expression to PIVP_list %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

id_PIVP([1, [ [ [1, [0]] ] ], [0]]).

inv_PIVP([1, [ [ [-1, [2]] ] ], [1]]).

exp_PIVP([1, [ [ [1, [1]] ] ], [1]]).

invexp_PIVP([1, [ [ [-1, [1]] ] ], [1]]).

cos_PIVP([2, [ [[1, [0, 1]]] , [[-1, [1, 0]]] ], [1, 0]]).

sin_PIVP([2, [ [[1, [0, 1]]] , [[-1, [1, 0]]] ], [0, 1]]).


%! expression_to_PIVP(+expression,-PIVP_list)
%
% Construct the PIVP_list object for a given expression

expression_to_PIVP(N, PIVP) :-
  number(N),
  !,
  PIVP = [1, [ [] ], [N]], !.

expression_to_PIVP(id, PIVP) :-
  !,
  id_PIVP(PIVP).

expression_to_PIVP(exp, PIVP) :-
  !,
  exp_PIVP(PIVP).

expression_to_PIVP(cos, PIVP) :-
  !,
  cos_PIVP(PIVP).

expression_to_PIVP(sin, PIVP) :-
  !,
  sin_PIVP(PIVP).

expression_to_PIVP(E + F, PIVP) :-
  !,
  expression_to_PIVP(E, PIVP1),
  expression_to_PIVP(F, PIVP2),
  add_PIVP(PIVP1, PIVP2, PIVP).

expression_to_PIVP(E * F, PIVP) :-
  !,
  expression_to_PIVP(E, PIVP1),
  expression_to_PIVP(F, PIVP2),
  multiply_PIVP(PIVP1, PIVP2, PIVP).

expression_to_PIVP(1 / E, PIVP) :-
  !,
  expression_to_PIVP(E, PIVP1),
  invert_PIVP(PIVP1, PIVP), !.

expression_to_PIVP(E / F, PIVP) :-
  !,
  expression_to_PIVP(E, PIVP1),
  expression_to_PIVP(1 / F, PIVP2),
  multiply_PIVP(PIVP1, PIVP2, PIVP).

expression_to_PIVP(E, _) :-
  throw(error(illegal_expression(E), expression_to_PIVP)).


%! abstract_names(+N, +Output, -Name_list)

abstract_names(N, Output, [Output|RTail]) :-
  Nm is N-1,
  abstract_names(Nm, Tail),
  reverse(Tail, RTail).

abstract_names(0, []) :- !.

abstract_names(N, [Head|Tail]) :-
  Nm is N-1,
  new_molecule(Head),
  abstract_names(Nm, Tail).

%! stutter(N,In,Out)

stutter(1, In, In) :- !.

stutter(N, In, Out) :-
  Nm is N-1,
  stutter(Nm, p, Tempo),
  atom_concat(In, Tempo, Out).
