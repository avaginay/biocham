/** <quadratic_reduction> Tools to manipulate PODE

Apart from the internal tools, two main functions are defined here:
- the list_reduction function that transform a PODE described through the internal
representation of the gpac module.
- the ODE_quadratic_reduction function aimed to be a biocham command to reduce the current ODE
system.

@author M. Hemery & S. Soliman
@license GNU-GPL v.2
*/
:- module(
  reduction,
  [
    % Grammar
    reduction_methods/1,
    reduction_bounds/1,
    %Commands
    quadratize_ode/1, 
    polynomial_ODE/0,
    %API
    list_reduction/5,
    determine_keep_list/3
  ]
).

:- use_module(library(clpfd)).
:- use_module(arithmetic_rules).
:- use_module(biocham).
:- use_module(gpac).
:- use_module(ode).

%! reduction_methods(?Expression) is det.
%
% Available reduction methods.
:- grammar(reduction_methods).

reduction_methods(no).

reduction_methods(native).

reduction_methods(fast).

reduction_methods(fastnSAT).

reduction_methods(sat_species).

reduction_methods(sat_reactions).


:- grammar(reduction_bounds).

reduction_bounds(carothers).

reduction_bounds(bychkov).


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Biocham Command %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

quadratize_ode(Keep) :-
  biocham_command(*),
  type(Keep, '*'(object)),
  doc('Modify the ODE system in order to make it quadratic while keeping the prescribed variables'),
  (
    polynomial_ODE
  ->
    true
  ;
    writeln("Not polynomial"),
    fail
  ),
  % Generate the appropriate format to call our reduction algorithm
  ode:with_current_ode_system((
    ode:get_current_ode_system(Id),
    findall(V, ode:ode_variables(Id, V), VarList),
    reduction:extract_poly(Id, VarList, PODE)
  )),
  determine_keep_list(Keep, VarList, Keep_list),
  % Call the algorithm and companion tools
  scan_order_multivarvector(PODE,Order),
  (
    Order > 2
  ->
    generate_sufficient_variables(PODE, ListAllVariable),
    append(ListAllVariable, Keep_list, AllVarWithKeep),
    breadth_search(PODE, AllVarWithKeep, Keep_list, ListVariable),
    % writeln(ListVariable),
    rewrite_pode(ListVariable, PODE, ListVariable, NewPODE),
    generate_names(VarList, ListVariable, Modified_Names),
    gather_initial_value(VarList, ConcList),
    % Now we have to rewrite the ODE system
    ode:new_ode_system(NewId),
    ode:set_ode_system_name(NewId, quadratic_ode),
    convert_to_ode(NewId, NewPODE, Modified_Names),
    set_initial_concentration(NewId, ListVariable, ConcList, Modified_Names),
    !,
    ode:select_ode_system(quadratic_ode)
  ;
    writeln("already quadratic"),
    true
  ).


%! polynomial_ODE
%
% check that the current ODE system is quadratic

polynomial_ODE :-
  biocham_command,
  doc('Check if the current ODE system is polynomial'),
  (
    ode:with_current_ode_system(
      forall(
        ode:ode(_A, Expr),
        (
          util:expand_polynomial(Expr, Simp_expr),
          ode:is_polynomial(Simp_expr)
        )
      )
    )
  ->
    writeln("polynomial")
  ;
    writeln("not polymonial"),
    fail
  ).


%%%%%%%%%%%%%%%%%%%%%
%%%%% API parts %%%%%
%%%%%%%%%%%%%%%%%%%%%

%! put_keep_in_first(+Unsort, +Keep, -Sort)
%
% Sort contain all the element of Unsort, but those of keep are in front

put_keep_in_first(Unsort, Keep, Sort) :-
  sort_keep(Unsort, Keep, Keeped, UnKeeped),
  append(Keeped, UnKeeped, Sort).

sort_keep([], _K, [], []).

sort_keep([Head|Tail], Keep, [Head|KTail], UTail) :-
  member(Head, Keep),
  !,
  sort_keep(Tail, Keep, KTail, UTail).

sort_keep([Head|Tail], Keep, KTail, [Head|UTail]) :-
  sort_keep(Tail, Keep, KTail, UTail).


%! list_reduction(+PIVP, +Name_list, +Keep_list, -Modified_PIVP, -Modified_Names)
%
% Reduce a given PIVP to a new one which only need quadratic
% (Correspond to the old reduce_to_quadratic)

list_reduction([N,PODE,IV], Name_list, Keep_list, [NewN,NewPODE,NewIV], Modified_Names) :-
   scan_order_multivarvector(PODE,Order),
   (
      Order > 2
   ->
      generate_sufficient_variables(PODE, ListAllVariable),
      append(ListAllVariable, Keep_list, AllVarWithKeep),
      breadth_search(PODE, AllVarWithKeep, Keep_list, ListVariable),
      length(ListVariable,NewN),
      % quadratic_branch_bound(ListAllVariable, PODE, ListVariable, NewN),
      rewrite_pode(ListVariable,PODE,ListVariable,NewPODE),
      generate_iv(ListVariable,IV,NewIV),
      generate_names(Name_list, ListVariable, Modified_Names)
   ; % PIVP is already quadratic
      NewN = N,
      NewPODE = PODE,
      NewIV = IV,
      Modified_Names = Name_list
   ).



%! compute_derivative(+Exponent,+PODE,-Derivative)
%
% compute the derivative of Exponent in PODE framework

compute_derivative(Exponent,PODE,Derivative) :-
   compute_derivative(Exponent,PODE,1,Derivative).

compute_derivative(Exponent,_PODE,Counter,[]) :-
   CounterM is Counter - 1,
   length(Exponent,CounterM), !.

compute_derivative(Exponent,PODE,Counter,Derivative) :-
   CounterP is Counter + 1,
   nth1(Counter, Exponent, N),
   (
      N =:= 0
   ->
      compute_derivative(Exponent,PODE,CounterP,Derivative)
   ;
      length(Exponent,Len),
      CounterM is Counter - 1,
      Remainder is Len - Counter,
      gpac:displace_exponent([1],CounterM,Remainder,Spec),
      substract_N_exp(Exponent,1,Spec,ExpWithoutSpec),
      nth1(Counter, PODE, This_Derivative),
      gpac:multiply_multivar_monomial(This_Derivative,[N,ExpWithoutSpec],ToAdd),
      compute_derivative(Exponent,PODE,CounterP,Derivative_modif),
      gpac:add_multivar(Derivative_modif,ToAdd,Derivative),
      !
   ).


%! substract_N_exp(Exp_m,N,Exp,Exp_m_modif)
%
% As list: Exp_m_modif = Exp_m - N * Exp

substract_N_exp([],_N,[],[]).

substract_N_exp([Head_m|Tail_m],N,[Head_s|Tail_s],[Head_new|Tail_new]):-
   Head_new is Head_m - N*Head_s,
   substract_N_exp(Tail_m,N,Tail_s,Tail_new).


%! generate_sufficient_variables(+PODE, -Var_set)
%
% Generate all exponents that are not null and strictly smaller than existing monomial
% These are sufficient variables for quadratic reduction. Subroutines are:
% is_monomial(PODE, Monom) to extract the monomial from the PODE
% highest_exponent(Set, Exp) that extract the highest exponent for each place in the set
% highest(A,B,Max) such that Max is the maximum of A and B
% strictly_smaller(+E1,-E2) generate E2 as non null and strictly smaller than E1
% smaller(+E1,-E2) generate E2 smaller than E1

generate_sufficient_variables(PODE, Final_set) :-
   setof(Exp, is_monomial(PODE,Exp), Exp_set),
   highest_exponent(Exp_set, Highest_Exp),
   get_option(quadratic_bound, Bound),
   (
     Bound == carothers
   ->
     setof(Var, strictly_smaller(Highest_Exp, Var), Var_set),
     [Head|_Remainder] = Var_set,
     length(Head, L)
   ;
     max_list(Highest_Exp, MaxDegree),
     length(Highest_Exp, L),
     setof(Var, all_smaller_or_equal(MaxDegree, L, Var), Var_set)
   ),
   Lm is L - 1,
   gpac:displace_exponent([1],0,Lm,First_Var),
   ( % add the first var if needed
      member(First_Var, Var_set)
   ->
      Final_set = Var_set
   ;
      Final_set = [First_Var|Var_set]
   ).

is_monomial(PODE,Monom) :-
   member(Der,PODE),
   member([_R,Monom],Der).


% determine_keep_list(+Keep, +VarList, -KeepList)
%
% used tu determine the initial set of variable to include in the final PODE

determine_keep_list([], _VL, []).

determine_keep_list([Var|Tail], VarList, [ListedVar|KLTail]) :-
  determine_keep_list_sr(VarList, Var, ListedVar),
  determine_keep_list(Tail, VarList, KLTail).

determine_keep_list_sr([], _Var, []).
determine_keep_list_sr([Var|T], Var, [1|Tail]) :- !,
  determine_keep_list_sr(T, Var, Tail).
determine_keep_list_sr([_H|T], Var, [0|Tail]) :- !,
  determine_keep_list_sr(T, Var, Tail).

% highest_exponent(+Exp_Set, -Highest_Exp)

highest_exponent([Head|Tail], HExp) :-
   highest_exponent(Tail,HExp_tmp),
   highest(Head,HExp_tmp,HExp),!.

highest_exponent([HExp], HExp) :- !.


% highest(+H1,+H2,-Max)

highest([Head1|Tail1],[Head2|Tail2],[HeadM|TailM]) :-
   max_list([Head1,Head2],HeadM),
   highest(Tail1,Tail2,TailM).

highest([],[],[]).


% strictly_smaller(+ExpM,+Exp)
%
% Exp is smaller than ExpM

strictly_smaller(ExpM,Exp) :-
   sum_list(ExpM, SumM),
   smaller(ExpM,Exp),
   sum(Exp, #=, Sum),
   Sum #> 0,
   Sum #< SumM,
   label(Exp).

smaller([HeadM|TailM],[Head|Tail]) :-
   Head #>= 0,
   Head #=< HeadM,
   smaller(TailM,Tail).

smaller([],[]).


%!all_smaller_or_equal(+Max, +N, -List) is nondet.
%
% generates all lists of length N with values between 0 and Max
all_smaller_or_equal(Max, N, List) :-
  length(List, N),
  List ins 0..Max,
  label(List).


%! compute_number_species(+HighestExp,Acc,AccZero).
%
% subroutine to return the number of variables in the new set of variable

compute_number_species([],1,0).

compute_number_species([Head|Tail],Acc,AccZero) :-
   (
      Head =:= 0
   ->
      compute_number_species(Tail,Acc,AccZeroTempo),
      AccZero is AccZeroTempo+1
   ;
      compute_number_species(Tail,AccTempo,AccZero),
      Acc is AccTempo * (Head+1)
   ).


%! change_exponant(+OldExp,+ListVariable,-NewExp)
%
% Switch from the old variable set to the new one

change_exponant(_OldExp,[],[]).

change_exponant(OldExp,[OldExp|Tail],[1|T]) :- change_exponant(OldExp,Tail,T),!.

change_exponant(OldExp,[_Head|Tail],[0|T]) :- change_exponant(OldExp,Tail,T),!.


%! generate_pode(+ListVariable,+PODE,+ListVariable,+HighestExp,-NewPODE)
%
% Generate the MultivarVector corresponding to PODE in the new set of variables

generate_pode([],_PODE,_ListVariable,_HighestExp,[]) :- !.

generate_pode([Var|TailVar],PODE,ListVariable,HighestExp,[Multivar|Tail]) :-
   sum_list(Var,VarTot),
   (
      VarTot =:= 1
   ->
      % The variable already exist in the old set
      extract_from_vector(Var,PODE,OldMultivar),
      set_to_new_variable(OldMultivar,ListVariable,Multivar)
   ;
      % This is a new variable
      derive_new_variable([],Var,PODE,ListVariable,HighestExp,Multivar)
   ),
   generate_pode(TailVar,PODE,ListVariable,HighestExp,Tail).


%! rewrite_pode(+ListVariable,+PODE,-NewPODE)
%
% Rewrite the MultivarVector corresponding to PODE in the new set of variables

rewrite_pode(ListVariable,PODE,NewPODE) :-
   rewrite_pode(ListVariable,PODE,ListVariable,NewPODE).

rewrite_pode([],_PODE,_ListVariable,[]) :- !.

rewrite_pode([Var|TailVar],PODE,ListVariable,[RW_Der|Tail]) :-
   compute_derivative(Var,PODE,Der),
   rewrite_der(Der,ListVariable,RW_Der),
   rewrite_pode(TailVar,PODE,ListVariable,Tail).

rewrite_der([],_ListVariable,[]) :- !.

rewrite_der([[Coeff,Expo]|Tail],ListVariable,[[Coeff,RWExpo]|RWTail]) :-
   rewrite_expo(Expo,ListVariable,RWExpo),
   rewrite_der(Tail,ListVariable,RWTail).

rewrite_expo(Expo,ListVariable,RWExpo) :-
   sum_list(Expo,0),
   length(ListVariable,L),
   gpac:displace_exponent([],0,L,RWExpo),!.

rewrite_expo(Expo,ListVariable,RWExpo) :-
   nth0(Index,ListVariable,Expo),
   length(ListVariable,L),
   Right is L-1-Index,
   gpac:displace_exponent([1],Index,Right,RWExpo),!.

rewrite_expo(Expo,ListVariable,RWExpo) :-
   nth0(Index1,ListVariable,Expo1),
   nth0(Index2,ListVariable,Expo2),
   add_list(Expo1,Expo2,Expo),
   length(ListVariable,L),
   (
      Index1 = Index2
   ->
      Right is L-1-Index1,
      gpac:displace_exponent([2],Index1,Right,RWExpo)
   ;
      min_list([Index1,Index2],Min),
      max_list([Index1,Index2],Max),
      Right is Max-1-Min,
      gpac:displace_exponent([1],Min,Right,ExpoTempo),
      append(ExpoTempo,[1],ExpoTempo2),
      Right2 is L-Max-1,
      gpac:displace_exponent(ExpoTempo2,0,Right2,RWExpo)
   ).


%! extract_from_vector(+Mask,+Vector,-Result)
%
% given a Mask of the form [0,0,.,1,.,0,0] and a Vector of same length
% return the corresponding element

extract_from_vector([],_Vector,[]).

extract_from_vector([1|_Tail],[Element|_Vector],Element) :- !.

extract_from_vector([0|Tail],[_Skip|Vector],Element) :-
   extract_from_vector(Tail,Vector,Element).


%! set_to_new_variable(+Multivar,+ListVariable,-NewMultivar)
%
% Rephrase an old pODE in the new variable set

set_to_new_variable([],_,[]).

set_to_new_variable([[K,Exp]|Tail],ListVariable,[[K,NewExp]|NewTail]) :-
   change_exponant(Exp,ListVariable,NewExp),
   set_to_new_variable(Tail,ListVariable,NewTail).


%! derive_new_variable(Done,+Exponent,+MultivarVector,+ListVariable,+HighestExp,-Multivar)
%
% Gives the derivative of an old exponent set in the new variable setting
% The MultivarVector is the core of the old PIVP, that is the derivative of
% the old (basic) variables.

derive_new_variable(_,[],[],_ListVariable,_HighestExp,[]).

derive_new_variable(Done,[Exp|Tail],[Deriv|TailDeriv],ListVariable,HighestExp,Multivar) :-
   (
      Exp >= 1
   ->
      Expm is Exp-1,
      append(Done,[Expm],Exp_Tempo),
      append(Exp_Tempo,Tail,ExpDeriv),
      append(Done,[Exp],DoneE),
      derive_new_variable(DoneE,Tail,TailDeriv,ListVariable,HighestExp,MultivarTempo),
      multiply_derivative(ExpDeriv,Deriv,ListVariable,HighestExp,ThisDerivative),
      scalar_multivar(Exp,ThisDerivative,K_time_ThisDerivative),
      append(K_time_ThisDerivative,MultivarTempo,Multivar)
   ;
      Exp =:= 0
   ->
      append(Done,[Exp],DoneE),
      derive_new_variable(DoneE,Tail,TailDeriv,ListVariable,HighestExp,Multivar)
   ).


%! multiply_derivative(ExpSet,Deriv,ListVariable,HighestExp,ThisDerivative)
%
% Given an old ExpSet and a derivative of a basic variable
% return the corresponding Multivar in the new variable set

multiply_derivative(_ExpSet,[],_ListVariable,_HighestExp,[]).

multiply_derivative(ExpSet,[[K,Exp]|Deriv],ListVariable,HighestExp,[[K,NewExp]|ThisDerivative]) :-
   change_exponant(ExpSet,ListVariable,NewExpSet),
   change_exponant(Exp,ListVariable,NewExpDeriv),
   add_list(NewExpSet,NewExpDeriv,NewExp),
   multiply_derivative(ExpSet,Deriv,ListVariable,HighestExp,ThisDerivative).


%! generate_iv(+ListVariable,+IV,-NewIV)
%
% generates all the initial concentration for the new set of variables

generate_iv([],_IV,[]) :- !.

generate_iv([Var|TailVar],IV,[NewValue|TailValue]) :-
   set_new_iv(Var,IV,NewValue_comp),
   clean_writing(NewValue_comp, NewValue_temp),
   simplify(NewValue_temp,NewValue),
   generate_iv(TailVar,IV,TailValue).


%! set_new_iv(+Exponent,+IV,-NewValue)
%
% determine the concentration of one variable (corresponding to Exponent)
% in the new set

set_new_iv([],_IV,1) :- !.

set_new_iv([Exp|TailE],[Value|TailV],NewValue) :-
   set_new_iv(TailE,TailV,NewValueTempo),
   (
      Exp = 0
   ->
      NewValue = NewValueTempo
   ;
      NewValue = NewValueTempo * Value^Exp
   ).

%! generate_names(+Name_list, +List_variable, -Modified_Names)
%
% Generate the names of the new variable

generate_names(_Name_list, [], []) :- !.

generate_names(Name_list, [Var|TailVar], [Name|TailName]) :-
  write_new_name(Name_list, Var, Name_as_list),
  atomic_list_concat(Name_as_list, Name),
  generate_names(Name_list, TailVar, TailName).


write_new_name([], [], []) :- !.

write_new_name([_Name|TailName], [0|TailExp], Name_as_list) :- !,
  write_new_name(TailName, TailExp, Name_as_list).

write_new_name([Name|TailName], [1|TailExp], [Name|Name_as_list]) :- !,
  write_new_name(TailName, TailExp, Name_as_list).

write_new_name([Name|TailName], [N|TailExp], [Name,N|Name_as_list]) :- !,
  write_new_name(TailName, TailExp, Name_as_list).


%! scan_order_multivarvector(+MultivarVector,-Order)
%
% Scan a MultivarVector to determine its order through recursive call to
% scan_order_multivar/2

scan_order_multivarvector([],0).

scan_order_multivarvector([Head|Tail],Order) :-
   scan_order_multivarvector(Tail,OrderTail),
   scan_order_multivar(Head,OrderHead),
   Order is max(OrderHead,OrderTail).


%! scan_order_multivar(+Multivar,-Order)
%
% Scan a Multivar to determine its order

scan_order_multivar([],0).

scan_order_multivar([[_K,Exponent]|Tail],Order) :-
   scan_order_multivar(Tail,OrderTail),
   sum_list(Exponent,OrderHead),
   Order is max(OrderHead,OrderTail).


%! max_list(List1,List2,MaxList)
%
% determine the maximum at each index of two lists

max_list([],[],[]).

max_list([],List,List).

max_list(List,[],List).

max_list([Head1|Tail1],[Head2|Tail2],[HeadMax|TailMax]) :-
   HeadMax is max(Head1,Head2),
   max_list(Tail1,Tail2,TailMax).


%! varlist_is_quadratic(+Variable_list, +Derivative_list)
%
% determine if Derivative_list may be quadraticy expressed with variable

varlist_is_quadratic(_Var_list, []) :- !.

varlist_is_quadratic(Var_list, [Der|Tail]) :-
   derivative_is_quadratic(Var_list, Der),
   varlist_is_quadratic(Var_list, Tail).

derivative_is_quadratic(_Var_list, []) :- !.

derivative_is_quadratic(Var_list, [[_Coeff, Expo]|Tail]) :-
   (
      constant(Expo)
   ;
      member(Expo,Var_list)
   ;
      member(X, Var_list), member(Y, Var_list),
      add_list(X, Y, Expo)
   ),!,
   derivative_is_quadratic(Var_list, Tail).

constant([]) :- !.
constant([0|Tail]) :-
   constant(Tail).

%! breadth_search(+PODE, +ListVariable, +Keep_list, -Redvar)
%
% perform a breadth search to find the smallest quadratic Reduction
% ListVariable is the list of all possibly needed variables
% Keep_list gives the variable that needs to be preserved

breadth_search(PODE, ListVariable, Keep_list, Redvar) :-
   construct_all_derivatives(PODE,ListVariable,ListDerivative),
   get_option(quadratic_reduction, Reduction),
   (
     atom_concat('sat', _, Reduction)
   ->
     maxsat_quadratic_reduction(ListVariable, ListDerivative, Keep_list, Redvar)
   ;
     member(Reduction, [fast, fastnSAT])
   ->
     fast_search(PODE, Keep_list, ListVariable, Redvar1),
     (
       Reduction = fast
     ->
       Redvar = Redvar1
     ;
       construct_all_derivatives(PODE, Redvar1, ListDerivative1),
       with_option(
         quadratic_reduction:sat_species,
         maxsat_quadratic_reduction(Redvar1, ListDerivative1, Keep_list, Redvar)
       )
     )
   ;
     breadth_search_subroutine([Keep_list], ListVariable, ListDerivative, _Next_level, Redvar)
   ).

breadth_search_subroutine([], ListVariable, ListDerivative, Next_level, Redvar) :-
   breadth_search_subroutine(Next_level, ListVariable, ListDerivative, [], Redvar).

breadth_search_subroutine([Head|_Tail], ListVariable, ListDerivative, _Next_level, Head) :-
   extract_derivatives(ListVariable,ListDerivative,Head,ListDer),
   varlist_is_quadratic(Head, ListDer),!.

breadth_search_subroutine([Head|Tail], ListVariable, ListDerivative, Next_level, Redvar) :-
   add_all_sons(Head, ListVariable, Next_level, ToAdd),
   append(Next_level,ToAdd,Next_level_2),!,
   breadth_search_subroutine(Tail, ListVariable, ListDerivative, Next_level_2, Redvar).


%! add_all_sons(+Current, +List_variable, +Current_next_list, -Element_to_add)
% is used to generate the variable lists needed for the next iteration

add_all_sons(_Test, [], _Next_level, []) :- !.

add_all_sons(Test, [Head|Tail], Next_level, ToAdd) :-
   member(Head, Test),
   !,
   add_all_sons(Test, Tail, Next_level, ToAdd).

add_all_sons(Test, [Head|Tail], Next_level, ToAdd) :-
   append(Test,[Head],TestExp),
   sort(TestExp,TestOrd),
   (
      member(TestOrd, Next_level)
   ->
      add_all_sons(Test, Tail, Next_level, ToAdd)
   ;
      append([TestOrd],ToAddTempo,ToAdd),
      add_all_sons(Test, Tail, Next_level, ToAddTempo)
   ).


%! construct_all_derivatives(+PODE,+ListVariable,-ListDerivative)
%
% Construct a list of all the derivatives in the variabel list in the PODE, usde to avoid
% recomputing it at each step in the quadratic reduction

construct_all_derivatives(_PODE, [], []).

construct_all_derivatives(PODE, [Head_V|Tail_V], [Head_R|Tail_R]) :-
   compute_derivative(Head_V, PODE, Head_R),
   construct_all_derivatives(PODE, Tail_V, Tail_R).


%! extract_derivatives(+AllVar,+AllDer,+ListVar,-ListDer)
%
% Gives the derivatives corresponding to ListVar, AllVar is needed for indexing

extract_derivatives([], _AllDer, _ListVar, []).

extract_derivatives([HeadV|TailV], [HeadD|TailAllD], ListVar, [HeadD|TailD]) :-
   member(HeadV,ListVar),!,
   extract_derivatives(TailV, TailAllD, ListVar, TailD).

extract_derivatives([_HeadAV|TailAV], [_HeadAD|TailAD], ListVar, TailD) :-
   extract_derivatives(TailAV, TailAD, ListVar, TailD).


%! quadratic_branch_bound(+Varlist, +PODE, -Reduction, -NewN)
%
% perform a branch_and_bound search to find the smallest quadratic Reduction
% VarList is the list of all possibly needed variables
%
% RQ : breadth_search perform better so is the one used

quadratic_branch_bound(Varlist, PODE, [First_Var|Bestlist], NewN) :-
   length(Varlist, Length),
   Varlist =  [Head|_Tail], length(Head,L), Lm is L - 1, gpac:displace_exponent([1],0,Lm,First_Var),
   selectchk(First_Var, Varlist, Varlist_co),
   quadratic_search_bb(First_Var, Varlist_co, PODE, _Sumlist_tmp, Bestlist, Length, Best),
   NewN is 1+Best.


%! quadratic_search_bb(First_Var, Varlist, PODE, _Sumlist, Bestlist, Length, Best)
%
% The branch and bound method that recursively call quadratic_bounded_search to
% verify if there is a list of variable that quadraticize PODE, Bestlist and Best
% are the accumulators while Sumlist and Length are the current results.

quadratic_search_bb(First_Var, Varlist, PODE, _Sumlist_tmp, Bestlist, Length, Best) :-
   Length > 0,
   Length_m is Length-1,
   quadratic_bounded_search(First_Var, Varlist, [], Sumlist, PODE, Length_m, Shorter),
   !,
   New_Best is Length_m - Shorter,
   quadratic_search_bb(First_Var, Varlist, PODE, Sumlist, Bestlist, New_Best, Best).

quadratic_search_bb(_First_var, _Varlist, _PODE, Bestlist, Bestlist, Best, Best).

%! quadratic_bounded_search(First_Var, Varlist, Currentlist, Sumlist, PODE, Length, Remainder)
%
% A bounded quadratic search that try to find a list less than Length to reduce PODE
% when call with a given length, return as remainder the number of variable that are not
% used in the current try.

quadratic_bounded_search(First_Var, _Varlist, Currentlist, Currentlist, PODE, Length, Length) :-
   varlist_is_quadratic([First_Var|Currentlist], PODE).

quadratic_bounded_search(First_Var, Varlist, Currentlist, Sumlist, PODE, Bound, Remainder) :-
   Bound > 0,
   Bound_m is Bound-1,
   member(Var, Varlist),
   selectchk(Var, Varlist, Varlist_co),
   quadratic_bounded_search(First_Var, Varlist_co, [Var|Currentlist], Sumlist, PODE, Bound_m, Remainder).


% clean_writing(+ExprIn, -ExprOut)
%
% eliminate unneeded arithmtic operator in an expression
% clean_writing(1*input, input).
% clean_writing(input^2*0, 0).

is_one(One) :-
  catch(
    One =:= 1,
    error(_A,_B),
    fail
  ).

clean_writing(ExprIn, ExprOut) :-
   clean_writing_sr(ExprIn, ExprTempo),
   (
      ExprIn = ExprTempo
   ->
      ExprOut = ExprTempo
   ;
      clean_writing(ExprTempo, ExprOut)
   ).

clean_writing_sr(Zero*_Other, 0) :- is_null(Zero), !.
clean_writing_sr(_Other*Zero, 0) :- is_null(Zero), !.
clean_writing_sr(One*OtherIn, OtherOut) :- is_one(One), !, clean_writing_sr(OtherIn, OtherOut).
clean_writing_sr(OtherIn*One, OtherOut) :- is_one(One), !, clean_writing_sr(OtherIn, OtherOut).
clean_writing_sr(OtherIn^One, OtherOut) :- is_one(One), !, clean_writing_sr(OtherIn, OtherOut).
clean_writing_sr(Other1In*Other2In, Other1Out*Other2Out) :-
   clean_writing_sr(Other1In, Other1Out),
   clean_writing_sr(Other2In, Other2Out).
clean_writing_sr(Other1In+Other2In, Other1Out+Other2Out) :-
   clean_writing_sr(Other1In, Other1Out),
   clean_writing_sr(Other2In, Other2Out).
clean_writing_sr(Other1In^Other2In, Other1Out^Other2Out) :- !,
   clean_writing_sr(Other1In, Other1Out),
   clean_writing_sr(Other2In, Other2Out).
clean_writing_sr(Other, Other) :- !.


%! extract_poly(+Id, +VariableList, -PODE)
%
% From the list of variable and an ode system Id, extract the pode in the list format

extract_poly(Id, VarList, PODE) :-
  extract_poly(Id, VarList, VarList, PODE).
extract_poly(_Id, [], _VarList, []) :- !.
extract_poly(Id, [Var|VarTail], VarList, [List|ListTail]) :-
  ode:ode(Id, Var, Expr),
  util:expand_polynomial(Expr, SExpr),
  gpac:parse_polynomial(SExpr, VarList, List),
  reduction:extract_poly(Id, VarTail, VarList, ListTail).


%! convert_to_ode(+ODE_Id, +PODE, +VarList)
%
% From a list_PODE as used in gpac, reconstruct an ODE system
% subfunctions convert_poly/mono/expo_to_ode are self explained

convert_to_ode(ODE_Id, PODE, VarList) :-
  convert_to_ode(ODE_Id, PODE, VarList, VarList).
convert_to_ode(ODE_Id, [Poly|PODE_Tail], [Var|VarTail], VarList) :-
  convert_poly_to_ode(Poly, VarList, Expr),
  ode:add_ode(ODE_Id, d(Var)/dt = Expr),
  convert_to_ode(ODE_Id, PODE_Tail, VarTail, VarList).
convert_to_ode(_Id, [], [], _VL) :- !.

convert_poly_to_ode([Monomial], VarList, Expr) :-
  !,
  convert_mono_to_ode(Monomial, VarList, Expr).
convert_poly_to_ode([Monomial|Tail], VarList, Remainder+Expr) :-
  !,
  convert_mono_to_ode(Monomial, VarList, Expr),
  convert_poly_to_ode(Tail, VarList, Remainder).

convert_mono_to_ode([Rate, Expo], VarList, Expr) :-
  convert_expo_to_ode(Expo, VarList, Expo_raw),
  simplify(Expo_raw, Monomial),
  (
    Rate = 1
  ->
    Expr = Monomial
  ;
    catch(
      Ratef is float(Rate),
      _Caught,
      Ratef = Rate
    ),
    Expr = Ratef*Monomial
  ).

convert_expo_to_ode([0|ETail], [_V|VTail], Expr) :-
  !,
  convert_expo_to_ode(ETail, VTail, Expr).
convert_expo_to_ode([N|ETail], [V|VTail], V^N*Expr) :-
  convert_expo_to_ode(ETail, VTail, Expr).
convert_expo_to_ode([], [], 1).


% gather_initial_value(+Varlist, -ConcList)
%
% retrieve the initial concentration for all variable

gather_initial_value([],[]).

gather_initial_value([Name|Tail], [Value|VTail]) :-
  get_ode_initial_state(Name, initial_state(_V = Value)),
  gather_initial_value(Tail, VTail).


% set_initial_concentration(+Id, +List_new_variable, +Init_conc_list, +Modified_Name)
%
%

set_initial_concentration(_Id, [], _L, []).

set_initial_concentration(Id, [Head|Tail], InitList, [Name|TailN]) :-
  compute_init(Head, InitList, Value),
  set_ode_initial_value(Id, Name, Value),
  set_initial_concentration(Id, Tail, InitList, TailN).


% compute_init(+Exponent_list, +Conc_list, -Value)

compute_init([], [], 1).

compute_init([E|ETail], [C|CTail], Value) :-
  compute_init(ETail, CTail, Tempo),
  Value is Tempo*(C**E).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% SAT Implementation %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Due tu S. Soliman

%! maxsat_quadratic_reduction(+AllVariables, +AllDerivatives, +OutVar_list, -Solution) is det.
%
% Calls a MAX-Sat solver to find the smallest quadratic reduction of PODE

maxsat_quadratic_reduction(AllVariables, AllDerivatives, OutVar_list, Solution) :-
  debug(pivp, "all vars: ~w~nall derivs: ~w", [AllVariables, AllDerivatives]),
  nb_setval(count_clauses, 0),
  tmp_file_stream(text, FileRoot, StreamFileInInit),
  close(StreamFileInInit),
  % we create a boolean var for each possible ODE var
  % except the output one they will all be associated to a soft clause
  % hence their number is a good Top value for the hard clauses
  length(AllVariables, N),
  findall(
    OutIndex,
    (
      member(OutVar, OutVar_list),
      nth1(OutIndex, AllVariables, OutVar)
    ),
    OutIndex_list
  ),
  debug(pivp, "Will be writing clauses to: ~w", [FileIn]),
  string_concat(FileRoot, ".wcnf", FileIn),
  get_option(quadratic_reduction, Reduction),
  (
    Reduction == sat_species
  ->
    % we have N-1 soft clauses for each variable
    Top = N,
    NVar = N,
    NMono = 0,
    M = 0
  ;
    derivatives_to_allmonomials(AllDerivatives, AllMonomials),
    debug(pivp, "all mono: ~w", [AllMonomials]),
    append(AllMonomials, MonoClauses),
    length(MonoClauses, NMono),
    sort(MonoClauses, SMonoClauses),
    length(SMonoClauses, M),
    % we have M soft clauses for each unique monomial
    Top is M + 1,
    NVar is N + M
  ),
  setup_call_cleanup(
    open(FileIn, write, TmpStream),
    (
      % fake header that is overwrite safe
      format(TmpStream, "fake hdr cccccccccccccccccc~n", []),
      derivatives_to_variables(AllDerivatives, AllVariables, TmpStream, Top)
    ),
    close(TmpStream)
  ),
  % we overestimate the number of clauses, but rc2.py is ok with that
  nb_getval(count_clauses, NCov),
  NClauses is NMono + Top + NCov,
  setup_call_cleanup(
    open(FileIn, update, UpStream),
    % header
    format(UpStream, "p wcnf ~d ~d ~d~n", [NVar, NClauses, Top]),
    close(UpStream)
  ),
  setup_call_cleanup(
    open(FileIn, append, Stream),
    (
      write_variable_clauses(Stream, N, M, OutIndex_list, Top, Reduction),
      (
        Reduction == sat_reactions
      ->
        write_monomial_clauses(Stream, N, Top, SMonoClauses, AllMonomials)
      ;
        true
      )
    ),
    close(Stream)
  ),
  debug(pivp, "~d clauses written to: ~w", [NClauses, FileIn]),
  debug(pivp, "Now calling MaxSAT", []),
  call_cleanup(
    (
      % there is always a solution, so we get solution and weight
      run_sat(FileIn, yes, [ResVariables, _Weight]),
      variable_string_to_solution(ResVariables, AllVariables, Solution),
      debug(pivp, "Solution: ~w", [Solution])
    ),
    (
      have_to_delete_temporary_files
    ->
      delete_file(FileIn)
    ;
      format("Temporary file saved in: ~w~n",FileIn)
    )
  ).

%! write_variable_clauses(+Stream, +N, +M, +OutIndex_list, +Top, +Reduction) is det.
%
% Write to stream one clause per possible variable to include.
% it is a hard clause (weight is Top) if it is the output variable
% otherwise the negation is a soft clause
write_variable_clauses(Stream, N, M, OutIndex_list, Top, Reduction) :-
  forall(
    (
      Reduction == sat_species
    ->
      between(1, N, I)
    ;
      (
        % if we minimize monomials, no soft clauses for variables
        member(I, OutIndex_list)
      ;
        Start is N + 1,
        End is N + M,
        between(Start, End, I)
      )
    ),
    (
      (
        member(I, OutIndex_list)
      ->
        Weight = Top,
        Value = I
      ;
        Weight = 1,
        Value is -I
      ),
      format(Stream, "~d ~d 0~n", [Weight, Value])
    )
  ).


%! write_monomial_clauses(Stream, N, SMonoClauses, AllMonomials) is det.
%
% Write clauses imposing that if a variable is present, all monomials of its derivative are present too
write_monomial_clauses(Stream, N, Top, SMonoClauses, AllMonomials) :-
  forall(
    between(1, N, I),
    (
      nth1(I, AllMonomials, Monomials),
      maplist(get_mono(SMonoClauses, N), Monomials, Numbers),
      forall(
        member(Number, Numbers),
        format(Stream, "~d -~w ~w 0~n", [Top, I, Number])
      )
    )
  ).


get_mono(List, Offset, Element, Position) :-
  nth1(Index, List, Element),
  Position is Index + Offset.


%! variable_string_to_solution(+String: string, AllVariables: list, -Solution: list) is det.
%
% Convert the output of the MaxSAT solver (-1 2 3 -4 ...) to a list of the
% variables that are present (i.e. positive)
variable_string_to_solution(String, AllVariables, Solution) :-
  split_string(String, " ", " ", VarStrings),
  maplist(atom_to_int, VarStrings, VarNumbers),
  % keep only the present variables
  exclude(>(0), VarNumbers, SolutionIndexes),
  findall(
    Var,
    (
      member(Idx, SolutionIndexes),
      nth1(Idx, AllVariables, Var)
    ),
    Solution
  ).


%! derivatives_to_variables(+AllDerivatives, +AllVariables, +TmpStream, +Top) is det.
%
% Write to TmpStream the clauses for covering each derivative
derivatives_to_variables(AllDerivatives, AllVariables, TmpStream, Top) :-
  nb_setval(varcount, 1),
  maplist(derivative_to_variables(AllVariables, TmpStream, Top), AllDerivatives).


%! derivative_to_variables(+AllVariables, +TmpStream, -Top, +Derivative) is det.
%
% Write to TmpStream clauses covering all monomials of Derivative
derivative_to_variables(AllVariables, TmpStream, Top, Derivative) :-
  maplist(monomial_remove_coeff, Derivative, Monomials),
  maplist(monomial_to_variables(AllVariables, TmpStream, Top), Monomials),
  nb_getval(varcount, Count),
  debug(pivp, "var ~d done", [Count]),
  CCount is Count + 1,
  nb_setval(varcount, CCount).


%! monomial_remove_coeff([+Coeff, +Monomial], -Monomial) is det.
%
% Remove the coefficient of a monomial
monomial_remove_coeff([_, M], M).


derivatives_to_allmonomials(AllDerivatives, AllMonomials) :-
  maplist(derivative_to_allmonomials, AllDerivatives, AllMonomials).


derivative_to_allmonomials(Derivative, Monomials) :-
  maplist(monomial_remove_coeff, Derivative, Monomials).


%! monomial_to_variables(+AllVariables, +TmpStream, +Top, +Monomial) is det.
%
% make a list of clauses for the possible coverings of Monomial (ignoring its constant
% part) by variables of AllVariables
monomial_to_variables(_Variables, _Stream, _Top, Monomial) :-
  % The derivative is constant, we return the empty conjunction
  max_list(Monomial, 0),
  !.

monomial_to_variables(AllVariables, Stream, Top, Monomial) :-
  % all sums of two different variables
  findall(
    [I1, I2],
    (
      nth1(I1, AllVariables, V1),
      nth1(I2, AllVariables, V2),
      I1 < I2,
      add_list(V1, V2, Monomial)
    ),
    Coverings
  ),
  % possible square of a variable
  (
    nth1(I, AllVariables, V),
    add_list(V, V, Monomial)
  ->
    SQCoverings = [[I] | Coverings]
  ;
    SQCoverings = Coverings
  ),
  % or even an existing variable
  (
    nth1(N, AllVariables, Monomial)
  ->
    DNFCovering = [[N] | SQCoverings]
  ;
    DNFCovering = SQCoverings
  ),
  nb_getval(varcount, Count),
  debug(pivp, "Var: ~d~nMonomial: ~w~nDNF: ~w~n~n", [Count, Monomial, DNFCovering]),
  dnf_cnf(DNFCovering, Count, Top, Stream).


%! dnf_cnf(+DNF:list, +Count:integer, +Top:integer, +Stream) is det.
%
% simple DNF to CNF conversion
% writing to Stream the clause for variable Count
dnf_cnf(DNF, Count, Top, Stream) :-
  forall(
    (
      maplist(member, L0, DNF),
      \+ member(Count, L0)
    ),
    (
      sort(L0, L),
      atomic_list_concat([Count | L], ' ', Atom),
      format(Stream, "~w -~a 0~n", [Top, Atom]),
      nb_getval(count_clauses, C),
      CC is C+1,
      nb_setval(count_clauses, CC)
    )
  ).


%! to_int(+String:atom, -Int:integer) is det.
%
% Read an Int from a String
to_int(String, Int) :-
  read_term_from_atom(String, Int, []),
  integer(Int).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FAST BINOMIAL REDUCTION %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% fast_search(+PODE, +Keep_list, +ListAllVariable, -BinomialSubSet)
%
% perform a fast but inoptimal reduction

fast_search(PODE, Keep_list, AllVariables, BinomialSubSet) :-
  add_keep_list(Keep_list, PODE, Exponents),
  fast_search_sr(Keep_list, BinomialSubSet, Exponents, AllVariables, PODE).

fast_search_sr(Acc, RAcc, [], _LAV, _LD) :-
  reverse(Acc,RAcc).

fast_search_sr(OldVar, Acc, Exponents, LAV, PODE) :-
  heuristic_few_expo(Exponents, OldVar, LAV, NewVar, PODE),
  add_variable_to_exponents(Exponents, PODE, NewVar, OldVar, NewExponents),
  fast_search_sr([NewVar|OldVar], Acc, NewExponents, LAV, PODE).


%! add_variable_to_exponents(+ExpOld, +PODE, +NewVar, +OldVar, -ExpNew)
%
% compute the new set of unreachable exponents (ExpNew) once we add NewVar to the set of
% variables

add_variable_to_exponents(ExpOld, PODE, NewVar, OldVar, ExpNew) :-
  compute_derivative(NewVar, PODE, Derivative),
  concatenate_exponents(ExpOld, Derivative, ExpTempo),
  sieve_exponents(ExpTempo, [NewVar|OldVar], ExpNew).


%! add_keep_list(+Keep_list, +Pode, -Exponents)
%
% recursive call to add_variable_to_exponents to initialize the exponent list

add_keep_list([], _PODE, []).

add_keep_list([Head|Tail], PODE, Exponents) :-
  add_keep_list(Tail, PODE, Exponents_tempo),
  add_variable_to_exponents(Exponents_tempo, PODE, Head, Tail, Exponents).


%! heuristic_few_expo(+Exponents, +OldVar, +AllVar, -NewVar, +PODE)
%
% determine the NewVar to be added to the set of variable by looking first at all the
% variable that are useful alone (either because they are in the set exponent, or because
% added to an old variable they form a useful exponent) if this set is empty, it try any
% remainding variable. Among this set, it choose the one that will introduce as few new
% exponents as possible.

heuristic_few_expo(Exponents, OldVar, AllVar, NewVar, PODE) :-
  maybe_useful(AllVar, Exponents, OldVar, Useful, PODE),
  (
    Useful = []
  ->
    maybe_useless(AllVar, Exponents, OldVar, Useless, PODE),
    keysort(Useless, [_K-NewVar|_UselessSortedTail])
  ;
    keysort(Useful, [_K-NewVar|_UsefulSortedTail])
  ).

%! maybe_useful(+AllVar, +Exponents, +OldVar, -ChoiceList, +PODE)
%
% Determine the list and cost of all variable that are useful alone
% Choicelist is a list of the form [Cost-Variable|Tail]

maybe_useful([], _Exponents, _OldVar, [], _PODE).

maybe_useful([Head|TailAll], Exponents, OldVar, [Key-Head|TailCut], PODE) :-
  (
    member(Head, Exponents)
  ;
    member(Other, OldVar),
    add_list(Other, Head, Expo),
    member(Expo, Exponents)
  ),!,
  estimate_value(Exponents, PODE, Head, OldVar, Key),
  maybe_useful(TailAll, Exponents, OldVar, TailCut, PODE).

maybe_useful([_Head|TailAll], Exponents, OldVar, TailCut, PODE) :-
  maybe_useful(TailAll, Exponents, OldVar, TailCut, PODE).

%! maybe_useless(+AllVar, +Exponents, +OldVar, -ChoiceList, +PODE)
%
% Determine the list and cost of all variable that are not already used 
% Choicelist is a list of the form [Cost-Variable|Tail]

maybe_useless([], _Exponents, _OldVar, [], _PODE).

maybe_useless([Head|TailAll], Exponents, OldVar, [Key-Head|TailCut], PODE) :-
  \+(member(Head, OldVar)),
  member(Expo, Exponents),
  strictly_smaller(Expo, Head),
  !,
  estimate_value(Exponents, PODE, Head, OldVar, Key),
  maybe_useless(TailAll, Exponents, OldVar, TailCut, PODE).

maybe_useless([_Head|TailAll], Exponents, OldVar, TailCut, PODE) :-
  maybe_useless(TailAll, Exponents, OldVar, TailCut, PODE).


%! estimate_value(+Exponents, +PODE, +NewVar, +OldVar, -Cost)
%
% Determine the cost (number of new exponents to be computed) of NewVar

estimate_value(ExpOld, PODE, NewVar, OldVar, Value) :-
  add_variable_to_exponents(ExpOld, PODE, NewVar, OldVar, ExpNew),
  length(ExpNew, Value).

%! concatenate_exponents(+Exponents, +Derivative, -Exponent2)
%
% add all the exponent of the derivative to the list of Exponents

concatenate_exponents(Exponents, Derivative, Exponent2):-
  extract_exponent(Derivative, TempoDer),
  append(Exponents, TempoDer, Exponent2).

extract_exponent([], []) :- !.

extract_exponent([[_R,E]|TailD], [E|TailE]) :- extract_exponent(TailD, TailE).

%! sieve_exponents(+Exponents, -LV, +NewExponents)
%
% remove from the list of exponents all the ones that are quadraticly reachable with the
% set of variables LV

sieve_exponents([], _Var_list, []) :- !.

sieve_exponents([Exp|Tail1], Var_list, Tail2) :-
  (
    constant(Exp)
  ;
    member(Exp,Var_list)
  ;
    member(X, Var_list), member(Y, Var_list),
    add_list(X, Y, Exp)
  ),
  !,
  sieve_exponents(Tail1, Var_list, Tail2).

sieve_exponents([Exp|Tail1], Var_list, [Exp|Tail2]) :-
  sieve_exponents(Tail1, Var_list, Tail2).
