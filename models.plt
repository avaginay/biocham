:- use_module(library(plunit)).

:- begin_tests(models, [setup((clear_model, reset_options))]).


test('new_model', [Reactions == []]) :-
  new_model,
  single_model(Id0),
  add_reaction(a => b),
  new_model,
  single_model(Id1),
  all_items([kind: reaction], Reactions),
  delete_item(Id0),
  delete_item(Id1).

test('new_model inherits from initial') :-
  new_model,
  single_model(Id),
  once((
    models:inherits_from(Id, InitialId),
    get_model_name(InitialId, 'initial')
  )).

test('load', [Name == mapk]) :-
  command(load(library:examples/mapk/mapk)),
  single_model(Id),
  get_model_name(Id, Name),
  delete_item(Id).

test(
  'list_model',
  [Output == 'MA(k) for a=>b.\nparameter(\n  k = 1\n).\n']
) :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  with_output_to(
    atom(Output),
    list_model
  ).

test('test_wf_reactant', []) :-
  with_output_to(atom(_), (
    test_wf_reactant(a, k*a^2),
    test_wf_reactant(a, exp(a)*b),
    \+(test_wf_reactant(m, -m)),
    \+(test_wf_reactant(m, exp(-m)))
  )).

test('test_wf_inhibitor', []) :-
  with_output_to(atom(_), (
    parameter([k=1, n=2, k100=10]),
    test_wf_inhibitor(a, b/(1+a)),
    test_wf_inhibitor(a, b/(k+a)^2),
    test_wf_inhibitor(c1, v10*t3^n/(k100^n+t3^n+c1^n)),
    \+(test_wf_inhibitor(a, k*a*b))
  )).

test('test_wf_other', []) :-
  with_output_to(atom(_), (
    test_wf_other(a, k*b*c),
    \+(test_wf_other(a, k*b*c/exp(a)))
  )).

%test('test_strictness', []):-
%  with_output_to(atom(_), (
%    models:test_strictness_sr([a,b,c], a*b for a+b=>c),
%    \+(models:test_strictness_sr([mapk], j0*mk/(1+(mapk/j1)^j2)/(j1+mk)) for mapk => mk),
%    \+(models:test_strictness_sr([a], k1*a - k2*b) for a => b)
%  )).

test('test_model_with_function', []) :-
  (
    biocham(function(f(a,b)=a*b)),
    biocham(f(a,b) for a+b => c),
    biocham(check_model)
  ).

:- end_tests(models).
