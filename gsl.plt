:- use_module(library(plunit)).

:- begin_tests(gsl, [condition(flag(slow_test, true, true)),
                     setup((clear_model, reset_options))]).

test('van_der_pol') :-
  Options = [
    fields: ['Time': t, 'x0': x(0), 'x1': x(1)],
    equations: [[1], -[0] + p(0) * [1] * (1 - [0] ^ 2)],
    initial_values: [1.0, 0.0],
    initial_parameter_values: [10],
    method: rk8pd,
    error_epsilon_absolute: 1e-6,
    error_epsilon_relative: 1e-6,
    initial_step_size: 1e-6,
    minimum_step_size: 1e-6,
    maximum_step_size: 1,
    precision: 5,
    filter: no_filter,
    time_initial: 0,
    time_final: 100
  ],
  solve(Options, Table),
  (
    append(_, [LastRow], Table),
    LastRow = row(LastTimeStamp, _, _),
    LastTimeStamp =:= 100.0
  ->
    true
  ;
    print(Table),
    nl,
    fail
  ).

:- end_tests(gsl).
