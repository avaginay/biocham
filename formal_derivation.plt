:- use_module(library(plunit)).

:- begin_tests(formal_derivation, [setup((clear_model, reset_options))]).

test('dx/dt(x) = 1', [true(E == 1)]) :-
  derivate(x, x, E).

test('dx/dt(y) = 0', [true(E == 0)]) :-
  derivate(y, x, E).

test('dx/dt(x^2) = 2x', [true(E == 2 * x)]) :-
  derivate(x ^ 2, x, E).

test('dx/dt(cos(sqrt(x))) = - 0.5 / sqrt(x) * sin(sqrt(x))',
     [true(E == - ( x^(-0.5) / 2 * sin(sqrt(x))))]) :-
  derivate(cos(sqrt(x)), x, E).

test('derivate quotient', [true(E == -(x / y^2))]) :-
  derivate(x/y, y, E).

test('derivate real test 1', [true(D == a*c^2)]) :-
  derivate(a*b*c^2, b, D).

:- end_tests(formal_derivation).
