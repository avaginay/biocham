:- module(
  rate_independence,
  [
    % Commands
   test_rate_independence/0,
   test_rate_independence_invariants/0
  ]
).


:- use_module(library(clpfd)).
:- use_module(doc).


%! test_rate_independence is det.
%
% Test the 3 graphical conditions (and not yet well-formedness) for rate independence.
test_rate_independence :-
  biocham_command,
  doc('Tests graphical sufficient conditions for rate independence of the current model for all output species (assuming well-formed kinetics).'),
  statistics(walltime, _),
  reaction_graph,
  get_current_graph(Id),
  (
    is_fork_free(Id),
    is_synthesis_free,
    is_loop_free(Id)
  ->
    writeln('The model is rate independent.')
  ;
    write('Undecided, sufficient condition for rate independence not verified: model '),
    nb_getval(rate_dependence_reason, Failed),
    Failed =.. [Stage, Place],
    format("not ~w-free (on ~w).~n", [Stage, Place])
  ),
  statistics(walltime, [_, Time]),
  TTime is Time / 1000,
  format("Time: ~p s~n", [TTime]).


%%% API

%! is_synthesis_free is det.
%
% check for the absence of a reaction with reactants included in products
is_synthesis_free :-
  \+(
      (
        item([kind: reaction, item: Reaction]),
        reaction(Reaction, [reactants: R, products: P]),
        forall(
          member(SR * M, R),
          (
            memberchk(SP * M, P),
            SR =< SP
          )
        ),
        nb_setval(rate_dependence_reason, synthesis(Reaction))
      )
    ).


%! is_fork_free(+GraphId) is det.
%
% check for the absence of a species participating in two different reactions
is_fork_free(GraphId) :-
  \+(
      (
        item([parent: GraphId, kind: vertex, id: VertexId, item: Vertex]),
        get_attribute(VertexId, kind=place),
        item([parent: GraphId, kind: edge, id: Id1, item: (Vertex -> _)]),
        item([parent: GraphId, kind: edge, id: Id2, item: (Vertex -> _)]),
        Id1 \= Id2,
        nb_setval(rate_dependence_reason, fork(Vertex))
      )
    ).


:- dynamic(seen/1).
:- dynamic(visiting/1).

%! is_loop_free(+GraphId) is det.
%
% check for the absence of loop using a simple DFS.
is_loop_free(GraphId) :-
  retractall(seen(_)),
  retractall(visiting(_)),
  forall(
    item([parent: GraphId, kind: vertex, item: Vertex]),
    (
      % at top level, seen vertices are ok, we're just in the same connected component
      seen(Vertex)
    ->
      true
    ;
      dfs_no_loop(GraphId, Vertex)
    )
  ).


%! dfs_no_loop(+GraphId, +Vertex) is det.
%
% mark a Vertex as under visit, and loop on all its children that shouldn't be under visit.
dfs_no_loop(GraphId, Vertex) :-
  assertz(visiting(Vertex)),
  forall(
    item([parent: GraphId, kind: edge, item: (Vertex -> Child)]),
    (
      (
        % check this first since we do not retract visiting
        seen(Child)
      ->
        true
      ;
        visiting(Child)
      ->
        nb_setval(rate_dependence_reason, loop(Vertex)),
        fail
      ;
        dfs_no_loop(GraphId, Child)
      )
    )
  ),
  assertz(seen(Vertex)).


% Dynamic predicate for storing structural information of the graph
:- dynamic(p_invariant/1).
%% :- dynamic(sur_invariant/1).
:- dynamic(output/1).
:- dynamic(siphon/1).

%! test_rate_independence_invariants is det.
%
% test sufficient condition for rate independence based on Petri net invariants
test_rate_independence_invariants :-
  biocham_command,
  doc('Tests invariant sufficient conditions for rate independence of the current model for all output species (assuming well-formed kinetics).'),
  statistics(walltime, _),
  retractall(p_invariant(_)),
  %% retractall(sur_invariant(_)),
  retractall(output(_)),
  retractall(siphon(_)),
  with_output_to(atom(_), invariants:find_invar_aux(4, is_place, is_transition, '#=<')),
  forall(
    retract(invariants:base_mol(B)),
    (
      normalize(B, BB),
      %% assertz(sur_invariant(BB)),
      %% debug(rate, "sur_invariant: ~w", [BB]),
      (
        BB = [Out]
      ->
        debug(rate, "output: ~w", [BB]),
        assertz(output(Out))
      ;
        true
      )
    )
  ),
  with_output_to(atom(_), invariants:find_invar_aux(4, is_place, is_transition, '#=')),
  forall(
    retract(invariants:base_mol(B)),
    (
      normalize(B, BB),
      debug(rate, "p_invariant: ~w", [BB]),
      assertz(p_invariant(BB))
    )
  ),
  reaction_graph,
  get_current_graph(Id),
  find_emptyable_siphons(Id),
  (
    output(_)
  ->
    (
      output(Out),
      test_rate_independence_invariants(Out, Id),
      fail
    ;
      true
    )
  ;
    writeln('Undecided: no outputs!')
  ),
  statistics(walltime, [_, Time]),
  TTime is Time / 1000,
  format("Time: ~p s~n", [TTime]).


%! test_rate_independence_invariants(+Output, -Id) is det.
%
% check the rate independence of species Output in reaction graph Id
test_rate_independence_invariants(Output, _Id) :-
  debug(rate, "Trying for output ~w", [Output]),
  (
    is_persistent(Output),
    p_invariant(P),
    member(Output, P)
  ->
    format("The output ~w is rate independent.~n", [Output])
  ;
    format("Undecided, ~w not persistent or not covered by any P-invariant.~n", [Output]),
    fail
  ).


%! is_persistent(+Species) is det.
%
% check sufficient condition for persistency (i.e. is not in any siphon that does not contain
% the support of a P-invariant)
is_persistent(Species) :-
  siphon(S),  % union of all emptyable siphons
  \+ member(Species, S),
  !.


%! normalize(+Invar, -Normalized) is det.
%
% remove stoichiometry and sort
normalize(Invar, Normalized) :-
  maplist(nusmv:reactant_to_name, Invar, NoStoichiometry),
  sort(NoStoichiometry, Normalized).


%! find_emptyable_siphons(+GraphId) is det.
%
% find all siphons that do not contain the support of a p-invariant
find_emptyable_siphons(GraphId) :-
  findall(
    Species,
    (
      item([parent: GraphId, kind: vertex, id: VertexId, item: Species]),
      get_attribute(VertexId, kind=place)
    ),
    LLSpecies
  ),
  sort(LLSpecies, LSpecies),
  length(LSpecies, N),
  length(LVars, N),
  LVars ins 0..1,
  siphon_constraints(LSpecies, LVars),
  pinv_constraints(LSpecies, LVars),
  assertz(siphon([])),
  (
    repeat,
    (
      % only get maximal such siphons
      get_max_constr(LSpecies, LVars),
      labeling([ff, down], LVars)
    ->
      invariants:to_mols(LVars, LSpecies, Siphon),
      retract(siphon(Current)),
      ord_union(Siphon, Current, Union),
      assertz(siphon(Union)),
      debug(rate, "found siphon: ~w", [Siphon]),
      debug(rate, "current union of siphons: ~w", [Union]),
      fail
    ;
      debugging(rate)
    ->
      siphon(Union),
      debug(rate, "Union of emptyable siphons: ~w", [Union])
    ;
      true
    )
  ->
    true
  ).


%! pinv_constraints(+LSpecies, ?LVars) is det.
%
% add constraints that siphons should not cover any P-invariant
pinv_constraints(LSpecies, LVars) :-
  foreach(
    p_invariant(P),
    is_not_covered(P, LSpecies, LVars)
  ).


%! is_not_covered(+P, +LSpecies, ?LVars) is det.
%
% enforce that P is not covered
is_not_covered(P, LSpecies, LVars) :-
  maplist(get_var_from_species(LSpecies, LVars), P, PVars),
  debug(rate, "constraint: ~w", [element(_, PVars, 0)]),
  element(_, PVars, 0).


%! get_var_from_species(+LSpecies, ?LVars, +Species, ?Var) is det.
%
% Var is the variable at the same position in LVars as Species in LSpecies
get_var_from_species(LSpecies, LVars, Species, Var) :-
  nth1(N, LSpecies, Species),
  !,
  nth1(N, LVars, Var).


%! siphon_constraints(+LSpecies, ?LVars) is det.
%
% siphon constraints (or of products => or of reactants) on all reactions
siphon_constraints(LSpecies, LVars) :-
  findall(
    (NR, NP),
    (
      item([kind: reaction, item: Reaction]),
      reaction(Reaction, [reactants: R, products: P]),
      normalize(R, NR),
      normalize(P, NP)
    ),
    L
  ),
  siphon_constraints_aux(L, LSpecies, LVars).


%! siphon_constraints_aux(+L, +LSpecies, ?LVars) is det.
%
% siphon constraint on a list of reactants/products
siphon_constraints_aux([], _, _).

siphon_constraints_aux([(NR, NP) | L], LSpecies, LVars) :-
  maplist(get_var_from_species(LSpecies, LVars), NR, VR),
  maplist(get_var_from_species(LSpecies, LVars), NP, VP),
  sum(VR, #=, SR),
  sum(VP, #=, SP),
  SP #> 0 #==> SR #> 0,
  debug(rate, "constraint ~w", [sum(VP) #> 0 #==> sum(VR) #> 0]),
  siphon_constraints_aux(L, LSpecies, LVars).


%! get_max_constr(+LSpecies, ?LVars) is det.
%
% enforce that future siphons have at least one species not yet covered
get_max_constr(LSpecies, LVars) :-
  siphon(S),
  ord_subtract(LSpecies, S, Others),
  maplist(get_var_from_species(LSpecies, LVars), Others, SV),
  element(_, SV, 1).
