ADDITIONAL_MODULES= \
  modules/sbml/sbml_utils.pl \
  modules/partialfrac/partialfraction.pl
MODULES=$(shell sed -n -E 's/^[+-] (.*\.pl)$$/\1/p' toc.org) \
  $(ADDITIONAL_MODULES)
# load_test_files/1 should make this useless, but I cannot find how to use it
TEST_MODULES=$(wildcard $(MODULES:.pl=.plt)) junit.pl

ifndef $(PREFIX)
  PREFIX=/usr/local
endif

$(foreach var, CC PLBASE PLARCH PLLIB PLLIBDIR PLCFLAGS PLLDFLAGS PLSOEXT PLVERSION, \
  $(eval \
    $(shell \
      swipl --dump-runtime-variables | \
      grep ^$(var)= | \
      sed -E 's/^/export /;s/="/=/;s/";$$//')))

# from version 7.7.13 default toplevel is 'halt' if there are initialization
# goals
SWIPL_GE_7713=$(shell [ $(PLVERSION) -ge 70713 ] && echo true)
TOPLEVEL=
ifeq ($(SWIPL_GE_7713),true)
  TOPLEVEL=--toplevel=prolog
endif
SUBDIRS=$(dir $(wildcard modules/*/Makefile))

INCLUDEDIRS=$(PLBASE)/include $(SUBDIRS)

RPATH=
ifneq ($(LD_RUN_PATH),)
  RPATH=-Wl,-rpath=$(LD_RUN_PATH)
endif
CFLAGS=$(addprefix -I, $(INCLUDEDIRS)) $(PLCFLAGS)
CXXFLAGS=$(CFLAGS) -std=c++11 `pkg-config --cflags libgvc`

# FIXME PLLDFLAGS always empty?
LDFLAGS=$(PLLDFLAGS) $(RPATH) -L $(PLLIBDIR)

LIBSBML=$(shell pkg-config --silence-errors --libs libsbml)
ifeq ($(strip $(LIBSBML)),)
  LIBSBML=-lsbml
endif
LDLIBS=$(PLLIB) `pkg-config --libs libgvc` $(LIBSBML) -lgsl -lgslcblas -lm

SWIPL=$(CURDIR)/swipl-biocham

CMAES_LIB=library/cmaes.c library/cmaes.h library/cmaes_interface.h

JUPYTER_DIR=$(CURDIR)/biocham_jupyter
KERNEL_DIR=$(JUPYTER_DIR)/kernel/biocham_kernel
WORKFLOWS_DIR=$(JUPYTER_DIR)/guinbextension/src/config/workflows

# NOTEBOOKS=$(shell find . -type f -name '*.ipynb' -print)
NOTEBOOKS=library/examples/tutorial/tutorialShort.ipynb library/examples/C2-19-Biochemical-Programming/TD1_lotka_volterra.ipynb library/examples/C2-19-Biochemical-Programming/TD2_enzyme_kinetics.ipynb library/examples/C2-19-Biochemical-Programming/TD4_genetic_switch.ipynb library/examples/C2-19-Biochemical-Programming/TD5_protein_switch.ipynb library/examples/C2-19-Biochemical-Programming/TD7_switches.ipynb library/examples/C2-19-Biochemical-Programming/TD7_rate_independence.ipynb library/examples/C2-19-Biochemical-Programming/TD3_mapk_signalling_boolean.ipynb library/examples/C2-19-Biochemical-Programming/TD4_mapk_signalling_differential.ipynb
# library/examples/C2-19-Biochemical-Programming/TD6_doctor_in_the_cell.ipynb library/examples/C2-19-Biochemical-Programming/TD6_oscillators.ipynb library/examples/tutorial/tutorialLongHistory.ipynb

REFDIR=nbrefs

all: biocham biocham_debug quick doc/index.html pldoc install_kernel install_gui

quick: unit_tests

.PHONY: all slow test unit_tests clean web cabernet cabernet_restart \
  install_kernel gadagne gadagne_restart jupyter_tests install pldoc devdoc \
  refs

install: biocham biocham_debug
	mkdir -p $(PREFIX)/bin
	mkdir -p $(PREFIX)/share/biocham
	cp $^ $(PREFIX)/share/biocham/
	ln -fs $(PREFIX)/share/biocham/biocham $(PREFIX)/bin/biocham
	ln -fs $(PREFIX)/share/biocham/biocham_debug $(PREFIX)/bin/biocham_debug
	cp -r library $(PREFIX)/share/biocham/
	cp -r doc $(PREFIX)/share/biocham/

biocham: $(SWIPL) $(MODULES) toc.org $(CMAES_LIB) \
	library/gsl_solver.o library/cmaes.o library/csv_reader.o Makefile
	$(SWIPL) -q -o biocham \
		--goal="\
			set_prolog_flag(verbose, normal), \
			start" \
		--toplevel=toplevel --stand_alone=true -O -c $(MODULES)

biocham_debug: $(SWIPL) $(MODULES) $(TEST_MODULES) toc.org $(CMAES_LIB) \
	library/gsl_solver.o library/cmaes.o library/csv_reader.o Makefile
	$(SWIPL) -q -o biocham_debug \
		--goal="\
			set_prolog_flag(verbose, normal), \
			initialize" \
		$(TOPLEVEL) --stand_alone=true -c $(MODULES) $(TEST_MODULES)

$(SWIPL): $(SWIPL).c \
		modules/graphviz/graphviz_swiprolog.c \
		modules/sbml/sbml_swiprolog.c \
		modules/partialfrac/roots.c
	for dir in $(SUBDIRS) ; do $(MAKE) -C $$dir ; done
	$(CXX) $(CXXFLAGS) $^ $(LDFLAGS) $(LDLIBS) -lstdc++ -o $@

doc/index.html doc/commands.js: biocham_debug
	echo "generate_doc." | ./biocham_debug

pldoc:
	echo "doc_save(., [if(true), doc_root('doc/pldoc')])." | swipl -q > /dev/null

devdoc:
	echo "generate_devdoc." | ./biocham_debug

# quick ones only
unit_tests: biocham_debug
	echo "run_tests_and_halt." | ./biocham_debug

# slow tests too
test: biocham_debug
	echo "flag(slow_test, _, true), run_tests_and_halt." | ./biocham_debug
	$(MAKE) jupyter_tests
	$(MAKE) refs

jupyter_tests: install_kernel
	PATH=$(CURDIR):$(PATH) jupyter nbconvert --execute --stdout --to notebook library/examples/cmsb_2017/sigmoids.ipynb > /dev/null

# runs test unit %
test_%: biocham_debug
	echo "flag(slow_test, _, true), run_tests_and_halt('$*')." | ./biocham_debug

install_kernel: biocham $(KERNEL_DIR)/commands.js $(KERNEL_DIR)/commands.py $(WORKFLOWS_DIR)/workflows.js
	- python3 -m pip install --user $(JUPYTER_DIR)/kernel || pip install --user $(JUPYTER_DIR)/kernel
	- jupyter kernelspec install --user --name=biocham $(KERNEL_DIR)

install_gui: $(WORKFLOWS_DIR)/workflows.js
	- cd $(JUPYTER_DIR)/guinbextension && npm install && npm run build && npm run docs:build
	- jupyter nbextension install --user $(JUPYTER_DIR)/guinbextension/gui-build/
	- jupyter nbextension enable --user gui-build/nbextension

install_gui_dev: $(WORKFLOWS_DIR)/workflows.js
	- cd $(JUPYTER_DIR)/guinbextension && npm install && npm run dev && npm run docs:build
	- jupyter nbextension install --user $(JUPYTER_DIR)/guinbextension/gui-build/
	- jupyter nbextension enable --user gui-build/nbextension

$(KERNEL_DIR)/commands.py: biocham
	rm -f $@
	echo '# Auto generated file, please do not edit manually' > $@
	echo 'commands = [' >> $@
	./biocham --list-commands | tail -n +4 | sort -u | sed -e 's/^\(.*\)$$/    "\1",/' >> $@
	echo ']' >> $@
	cp $@ $(WORKFLOWS_DIR)

$(KERNEL_DIR)/commands.js: doc/commands.js
	cp -f $< $@

# only static .bc files in that dir
$(WORKFLOWS_DIR)/workflows.js:
	# for file in $(WORKFLOWS_DIR)/nb/*.ipynb ; do jupyter nbconvert --to script $$file ; done
	python3 $(WORKFLOWS_DIR)/make_workflows.py -i $(WORKFLOWS_DIR)/nb/ -o $@

clean:
	- for dir in $(SUBDIRS) ; do $(MAKE) -C $$dir clean ; done
	- rm biocham
	- rm biocham_debug
	- rm biocham_tests
	- rm biocham_full_tests
	- rm swipl-biocham
	- rm swipl-biocham.o
	- rm library/gsl_solver.o
	- rm library/cmaes.o
	- rm library/csv_reader.o

$(CMAES_LIB): modules/c-cmaes/src
	cp $</$(notdir $@) $@

modules/c-cmaes/src:
	if [ -d .git ] ; then \
		git submodule init && \
		git submodule update ; \
	else \
		git clone https://github.com/CMA-ES/c-cmaes.git modules/c-cmaes ; \
	fi

# size issue, so we exclude the video
web/biocham.zip:
	git archive --prefix=biocham/ -o $@ HEAD
	-zip $@ -d biocham/web/biocham4.mov biocham/web/\*.zip

web: doc/index.html web/biocham.zip web/index.html web/logo.png
	-rsync -avz $^ doc lifeware:/nfs/web-epi/biocham4/
	ssh -t lifeware 'sudo chmod -R g+w /nfs/web-epi/biocham4/ && sudo chown -R www-data.lifeware /nfs/web-epi/biocham4/'

cabernet: web
	-rsync -avz tmpnb/* lifeware@cabernet:/usr/local/share/tmpbc/
	ssh -t lifeware@cabernet 'cd /usr/local/share/tmpbc/lifeware_biocham && sudo docker build --no-cache -t lifeware/biocham:$(TAG) --build-arg tag=$(TAG) .'

cabernet_restart: cabernet
	ssh -t lifeware@cabernet 'cd /usr/local/share/tmpbc/ && sudo ./cleanup.sh && sudo ./tmpnb_biocham.sh'

gadagne: web
	-rsync -avz tmpnb/* gadagne:/opt/tmpnb/
	ssh -t gadagne 'cd /opt/tmpnb/lifeware_biocham && sudo docker build --no-cache -t lifeware/biocham:$(TAG) --build-arg tag=$(TAG) .'

gadagne_restart: gadagne
	ssh -t gadagne 'cd /opt/tmpnb/ && sudo ./cleanup.sh && sudo ./tmpnb_biocham.sh'

release:
	@echo "current version: " && grep '^version' about.pl
	@read -p "Version number (e.g. 4.1.0): " version && \
	git flow release start "$$version" && \
	sed -i"" -e "s/^version('.*')/version('$$version')/" about.pl && \
	sed -i"" -e "s/\"version\": \"\(.*\)\"/\"version\": \"$$version\"/" biocham_jupyter/guinbextension/package.json && \
	sed -i"" -e "s/__version__ = '\(.*\)'/__version__ = '$$version'/" biocham_jupyter/kernel/biocham_kernel/__init__.py && \
	sed -i"" -e "s+^<h1>version.*</h1>+<h1>version $$version $(shell LC_ALL=C date '+%B %Y')</h1>+" web/index.html && \
	sed -i"" -e "s/biocham:.*$$/biocham:v$$version/" binder/Dockerfile && \
	git commit about.pl biocham_jupyter/guinbextension/package.json biocham_jupyter/kernel/biocham_kernel/__init__.py web/index.html binder/Dockerfile -m "Version $$version" && \
	git flow release finish "$$version" && \
	git push --tags

# if necessary convert a notebook to a biocham script
%.bc: %.ipynb
	jupyter nbconvert --to script $<

refs: $(NOTEBOOKS:.ipynb=.bc) biocham
	@mkdir -p $(REFDIR)
	@for file in $(NOTEBOOKS) ; do \
		out=$(REFDIR)/$$(basename $${file%%ipynb}out) && \
		result=0 && \
		printf "$$out " && \
		echo "load($${file%%ipynb}bc). quit." | ./biocham --jupyter 2>&1 | \
			grep -v '[0-9]\.[0-9]\|Copyright\|Time:' > $$out && \
		rm -f $$(dirname $$file)/graph?.png && \
		rm -f $$(dirname $$file)/ode.tex && \
		rm -f $$(dirname $$file)/plot-*.csv{,X} && \
		if [ -f $$out.ref ] ; then \
			diff $$out $$out.ref && printf "\e[1;32m✔\e[0m\n" || \
				{ printf "\e[1;31m✘\e[0m\n" && result=1 && break ; } \
		else \
			echo "Saving $$out to $$out.ref" && \
			cp $$out $$out.ref ; \
		fi ; \
	done ; \
	[ "$$result" -eq 0 ]
